var userLocale = document.documentElement.lang;
var formatter = new Intl.NumberFormat(userLocale);

// Display the result in the element with id="countdown"
const countdown = document.getElementById("countdown");

// Set the date we're counting down to
const countDownDate = new Date("February 28, 2025 15:00:00").getTime();

// Update the count down every 1 second
const x = setInterval(() => {
  // Get today's date and time
  const now = new Date().getTime();

  // Find the distance between now and the count down date
  const distance = countDownDate - now;

  // Time calculations for days, hours, minutes and seconds
  const days = formatter.format(Math.floor(distance / (1000 * 60 * 60 * 24)));
  const hours = formatter.format(
    Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)),
  );
  const minutes = formatter.format(
    Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60)),
  );
  const seconds = formatter.format(Math.floor((distance % (1000 * 60)) / 1000));

  countdown.innerHTML = countdown.dataset.text
    .replace(/%1/, days)
    .replace(/%2/, hours)
    .replace(/%3/, minutes)
    .replace(/%4/, seconds);

  // If the count down is finished, write some text
  if (distance < 0) {
    clearInterval(x);
    countdown.innerHTML = countdown.dataset.expired;
  }
}, 1000);

/////////////////////////////////
// PROGRESS BAR STUFF

var amount_direct = 7500+3780+200+50+100+50+50+50+100+59+50+50+51+50; //donated directly via PayPal or bank transfer
var amount_donorbox = 33579;
var amount = amount_direct+amount_donorbox;
var goal1 = 30000;
var goal2 = 20000;
var goal3 = 20000;
var goal = goal1 + goal2;// + goal3;

var percent = (amount / goal) * 100;
var percentFormatted = new Intl.NumberFormat(userLocale, {
  style: "percent",
}).format(percent / 100);

var goal1_w = (goal1 / goal) * 100;
var goal2_w = (goal2 / goal) * 100;
//var goal3_w = (goal3 / goal) * 100;

if (amount > goal) {
  goal1_w = (goal1 / amount) * 100;
  goal2_w = (goal2 / amount) * 100;
  goal3_w = (goal3 / amount) * 100;
  percent = 100;
}

// Goal bar

document.getElementById("bar_goal1").style.width = goal1_w + "%";
document.getElementById("bar_goal2").style.width = goal2_w + "%";
//document.getElementById("bar_goal3").style.width = goal3_w + "%";

// Progress bar proper

document.getElementById("bar_fill").style.width = percent + "%";
document.getElementById("bar_text").innerHTML = percentFormatted;

var bar_amount = document.getElementById("bar_amount");
bar_amount.innerHTML = bar_amount.dataset.progress.replace(/%1/, amount);
bar_amount.style.width = percent + "%";

//var bar_amount_rest = document.getElementById("bar_amount_rest");
//bar_amount_rest.style.width = (100-percent) + "%";

//////////////////////
// SHOW CONCLUSIONS

function show_conclusion(story_name) {
  document.getElementById(story_name).className = 'd-block'
}

function blk4blu(reason) {
  var dblack = document.getElementById(reason + "_blk")
  var dblue = document.getElementById(reason + "_blu")

  if (dblack.style.display === "none") {
    dblack.style.display = "block";
    dblue.style.display = "none"
  } else {
    dblack.style.display = "none";
    dblue.style.display = "block"
  }
}
