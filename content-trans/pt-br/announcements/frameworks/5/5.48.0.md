---
aliases:
- ../../kde-frameworks-5.48.0
date: 2018-07-14
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Migração dos usos restantes do qDebug() para qcDebug(ATTICA)
- Correcção da verificação de URL's de fornecedores inválidos
- Correcção de URL inválido na especificação da API

### Baloo

- Remoção do item não usado X-KDE-DBus-ModuleName dos meta-dados do 'plugin' do 'kded'
- [tags_kio] A pesquisa do URL deverá ser um par chave-valor
- O sinal do estado da energia só deve ser emitido quando o estado da energia muda de facto
- baloodb: Alterações na descrição do argumento da linha de comandos após mudar o nome de 'prune' -&gt; 'clean'
- Mostrar de forma clara nomes de ficheiros duplicados nas pastas de marcas

### BluezQt

- Actualização dos ficheiros XML do D-Bus para usar "Out*" como anotações do Qt para o tipo de sinal
- Adição de sinal para notificar a mudança de endereço do dispositivo

### Ícones Breeze

- Uso do ícone da vassoura também para o 'edit-clear-all'
- Uso de um ícone de vassoura para o 'edit-clear-history'
- modificação do ícone 'view-media-artist' em 24px

### Módulos extra do CMake

- Android: Possibilidade de substituição do destino da pasta de um APK
- Remoção do QT_USE_FAST_OPERATOR_PLUS desactualizado
- Adição do '-Wlogical-op -Wzero-as-null-pointer-constant' aos avisos do KF5
- [ECMGenerateHeaders] Adição de opção para outra extensão de ficheiros de inclusão que não o '.h'
- Não incluir um 64 ao compilar as arquitecturas a 64 bits no Flatpak

### KActivitiesStats

- Correcção de um erro por uma unidade no Cache::clear (erro 396175)
- Correcção do movimento do item do ResultModel (erro 396102)

### KCompletion

- Remoção da inclusão desnecessária do 'moc'
- Certificação de que o KLineEdit::clearButtonClicked é emitido

### KCoreAddons

- Remoção das definições do QT duplicadas do KDEFrameworkCompilerSettings
- Certificação de que compila com opções de compilação restritas
- Remoção da chave não usada X-KDE-DBus-ModuleName dos meta-dados do tipo de serviço de testes

### KCrash

- Redução do QT_DISABLE_DEPRECATED_BEFORE à dependência mínima do Qt
- Remoção das definições do QT duplicadas do KDEFrameworkCompilerSettings
- Certificação de que compila totalmente com opções de compilação restritas

### KDeclarative

- validação se o nó tem de facto uma textura válida (erro 395554)

### KDED

- definição do tipo de serviço do KDEDModule: remoção da chave não usada X-KDE-DBus-ModuleName

### KDELibs 4 Support

- Definição própria do QT_USE_FAST_OPERATOR_PLUS
- Não exportar o 'kf5-config' para o ficheiro de configuração do CMake
- Remoção do item não usado X-KDE-DBus-ModuleName dos meta-dados do 'plugin' do 'kded'

### WebKit do KDE

- Migração do KRun::runUrl() &amp; KRun::run() para uma API não desactualizada
- Migração do KIO::Job::ui() -&gt; KIO::Job::uiDelegate()

### KFileMetaData

- Eliminação de avisos do compilador para os ficheiros de inclusão da Taglib
- PopplerExtractor: uso directo de argumentos QByteArray() em vez de ponteiros nulos
- taglibextractor: Reposição das proporções áudio da extracção sem marcas existentes
- OdfExtractor: tratamento de nomes de prefixos não comuns
- Não adicionar o '-ltag' à interface de edição de ligações pública
- implementação da marca 'lyrics' (letras musicais) no 'taglibextractor'
- testes automáticos: não incorporar o EmbeddedImageData já na biblioteca
- adição da capacidade de ler ficheiros de capas incorporados
- implementação da leitura da marca 'rating' (classificação)
- validação da versão necessária das bibliotecas libavcode, libavformat e libavutil

### KGlobalAccel

- Actualização do ficheiro XML do D-Bus para usar o "Out*" para as anotações do Qt para os tipos de sinais

### KHolidays

- holidays/plan2/holiday_jp_* - adição dos meta-dados em falta
- actualização dos feriados Japoneses (em Japonês e Inglês) (erro 365241)
- holidays/plan2/holiday_th_en-gb - actualização da Tailândia (Inglês da GB) para 2018 (erro 393770)
- adição dos feriados da Venezuela (Espanhol) (erro 334305)

### KI18n

- No ficheiro de macros do CMake, uso por consequência do CMAKE_CURRENT_LIST_DIR em vez de um uso misto com o KF5I18n_DIR
- KF5I18NMacros: Não instalar uma pasta vazia se não existirem ficheiros PO

### KIconThemes

- Suporte da selecção de ficheiros .ico no selector de ficheiros de ícones personalizados (erro 233201)
- Suporte da Escala do Ícone a partir da especificações de nomes dos ícones na versão 0.13 (erro 365941)

### KIO

- Uso do novo método 'fastInsert' em todo o lado onde fizer sentido
- Reposição da compatibilidade do UDSEntry::insert com a adição de um método 'fastInsert'
- Migração do KLineEdit::setClearButtonShown -&gt; QLineEdit::setClearButtonEnabled
- Actualização do ficheiro XML do D-Bus para usar o "Out*" para as anotações do Qt para os tipos de sinais
- Remoção da definição duplicada do QT do KDEFrameworkCompilerSettings
- Uso de um ícone de emblema correcto para os ficheiros e pastas apenas para leitura (erro 360980)
- Possibilidade de subir até à raiz de novo no elemento gráfico do ficheiro
- [Janela de propriedades] Melhoria de alguns textos relacionados com permissões (erro 96714)
- [KIO] Adição do suporte para o XDG_TEMPLATES_DIR no KNewFileMenu (erro 191632)
- Actualização do Docbook do lixo para o 5.48
- [Janela de propriedades] Tornar todos os valores de campos na página Geral seleccionáveis (erro 105692)
- Remoção do item não usado X-KDE-DBus-ModuleName dos meta-dados dos 'plugins' do kded
- Possibilidade de comparação dos KFileItems pelo URL
- [KFileItem] Validação do URL mais local se é partilhado ou não
- Correcção de regressão ao colar dados binários a partir da área de transferência

### Kirigami

- cor mais consistente do rato à passagem
- não abrir o submenu nas acções sem filhos
- Remodelação do conceito da Barra de Ferramentas Global (erro 395455)
- Tratamento da propriedade 'enabled' (activo) nos modelos simples
- Introdução do ActionToolbar
- correcção do puxão para actualizar
- Remoção da documentação do Doxygen nas classes internas
- Não compilar com o Qt5::DBus quando o DISABLE_DBUS estiver definido
- não atribuir uma margem extra nas 'overlaysheets' no 'overlay'
- correcção do menu para o Qt 5.9
- Verificação da propriedade 'visible' também na acção
- melhor aparência/alinhamento no modo compacto
- não sondar os 'plugins' em cada criação do 'platformTheme'
- eliminação do conjunto "custom" (personalizado)
- adição de métodos de limpeza para todas as cores personalizadas
- migração da coloração dos botões de ferramentas para o conjunto de cores personalizado
- introdução do conjunto de cores Personalizado
- 'buddyFor' editável para as legendas de posição referentes aos subitens
- Ao usar uma cor de fundo diferente, usar o 'highlightedText' como cor do texto

### KNewStuff

- [KMoreTools] Possibilidade de instalação de ferramentas com um URL do 'appstream'
- Remoção do truque do ponteiro-d do KNS::Engine

### KService

- Remoção da chave não usada X-KDE-DBus-ModuleName dos meta-dados do serviço de testes

### KTextEditor

- guarda do 'updateConfig' para as barras de estado desactivadas
- adição de menu de contexto para a barra de estado para comutar a visibilidade do número de palavras/linhas totais
- Implementação da apresentação das linhas totais no Kate (erro 387362)
- Possibilidade de os botões da barra de ferramentas com menus mostrá-los com um 'click' normal em vez de um 'click' e manter pressionado (erro 353747)
- CVE-2018-10361: escalada de privilégios
- Correcção da largura do cursor (erro 391518)

### KWayland

- [servidor] Enviar um evento da pilha em vez de eliminar com um movimento relativo do cursor (erro 395815)
- Correcção do teste de janela instantânea do XDGV6
- Correcção de um erro estúpido ao copiar/colar no Cliente do XDGShellV6
- Não cancelar a selecção antiga da área de transferência, se for igual à nova (erro 395366)
- Respeito do BUILD_TESTING
- Correcção de alguns erros ortográficos sugeridos pela nova ferramenta de validação
- Adição do ficheiro 'arclint' no KWayland
- Correcção do @since para ignorar a API de mudança

### KWidgetsAddons

- [KMessageWidget] Actualização da folha de estilo com a mudança de paleta
- Actualização do kcharselect-data para o Unicode 11.0
- [KCharSelect] Migração do generate-datafile.py para o Python 3
- [KCharSelect] Preparar as traduções para a actualização para o Unicode 11.0

### ModemManagerQt

- Implementação do suporte para as interfaces Voice e Call
- Não definir regras de filtragem do domínio personalizadas

### Ícones do Oxygen

- Mostrar um ícone para os ficheiros escondidos no Dolphin (erro 395963)

### Plasma Framework

- FrameSvg: Actualização da moldura da máscara caso tenha mudado a localização da imagem
- FrameSvg: Não danificar molduras de máscaras partilhadas
- FrameSvg: Simplificação do 'updateSizes'
- Ícones para o Indicador do Teclado do T9050
- correcção da cor do ícone 'media'
- FrameSvg: Colocação em 'cache' da 'maskFrame' se o 'enabledBorders' tiver sido alterado (erro 391659)
- FrameSvg: Desenho dos cantos apenas se estiverem activos ambos os contornos em ambas as direcções
- Indicação ao ContainmentInterface::processMimeData como lidar com largadas do Gestor de Tarefas
- FrameSVG: Remoção de verificações redundantes
- FrameSVG: Correcção da inclusão do QObject
- Uso do QDateTime para interagir com o QML (erro 394423)

### Purpose

- Adição da acção Partilhar ao menu de contexto do Dolphin
- Inicialização adequada dos 'plugins'
- Filtragem dos 'plugins' duplicados

### QQC2StyleBridge

- valores sem pixel no 'checkindicator'
- uso do RadioIndicator para todos
- Não ultrapassar os limites das janelas instantâneas
- no Qt&lt;5.11, a paleta de controlo é completamente ignorada
- âncora no fundo do botão

### Solid

- Correcção da legenda do dispositivo com um tamanho desconhecido

### Realce de sintaxe

- Correcções para os comentários em Java
- Realce dos ficheiros do Gradle também com a sintaxe do Groovy
- CMake: Correcção do realce a seguir aos textos com um único símbolo <code>@</code>
- CoffeeScript: adição da extensão .cson
- Rust: Adição das palavras-chave &amp; bytes, correcção dos identificadores e outras melhorias/correcções
- Awk: correcção de expressão regular numa função e actualização da sintaxe para o gawk
- Pony: correcção do escape com plicas e de um potencial ciclo infinito com o #
- Actualização da sintaxe do CMake para a próxima versão 3.12

### Informações de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

Você pode discutir e compartilhar ideias sobre esta versão na seção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
