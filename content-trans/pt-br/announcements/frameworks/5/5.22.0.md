---
aliases:
- ../../kde-frameworks-5.22.0
date: 2016-05-15
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Verificação adequada para identificar se uma URL é um arquivo local

### Baloo

- Correções de compilação para Windows

### Ícones Breeze

- Diversos ícones novos para ações e aplicativos.
- Definição das extensões oferecidas como 'por alteração' nos kiconthemes

### Módulos extra do CMake

- Desenvolvimento Android: suporte para projetos sem itens na 'share' ou 'lib/qml' (erro <a href='https://bugs.kde.org/show_bug.cgi?id=362578'>362578</a>)
- Ativação do "KDE_INSTALL_USE_QT_SYS_PATHS" se tiver o prefixo "CMAKE_INSTALL_PREFIX" do Qt5
- ecm_qt_declare_logging_category: melhoria da mensagem de erro ao usar sem inclusão

### Integração do Framework

- Remoção do plugin <i>platformtheme</i>, pois já está incluído no <i>plasma-integration</i>

### KCoreAddons

- Possibilidade de desativar o uso do <i>inotify</i> no KDirWatch
- Correção do KAboutData::applicationData() para inicializar a partir dos metadados atuais do Q*Application
- Esclarecimento de que o KRandom não é recomendado para fins de criptografia

### KDBusAddons

- KDBusService: transformação de '-' em '_' nos caminhos dos objetos

### KDeclarative

- Não finalizar de forma inesperada em caso de ausência de contexto do OpenGL

### KDELibs 4 Support

- Definição de um MAXPATHLEN de contingência, caso não seja definido
- Correção do KDateTime::isValid() para valores ClockTime (erro <a href='https://bugs.kde.org/show_bug.cgi?id=336738'>336738</a>)

### KDocTools

- Adição de <i>entities</i> para os aplicativos

### KFileMetaData

- Mesclagem da ramificação <i>externalextractors</i>
- Correção dos plugins externos e de testes
- Adição do suporte para plugins de escrita externos
- Adição do suporte de plugins de escrita
- Adição do suporte de plugins de extração externos

### KHTML

- Implementação do <i>toString</i> para o 'Uint8ArrayConstructor' e objetos relacionados
- Mesclagem de diversas correcções relacionadas ao Coverity
- Uso correto do QCache::insert
- Correção de alguns vazamentos de memória
- Verificação de sanidade do processamento de fontes Web em CSS, para evitar possíveis vazamentos de memória
- dom: Adição das prioridades das etiquetas de 'comentário'

### KI18n

- libgettext: Correção de possível uso-após-liberação, usando outros compiladores que não seja o g++

### KIconThemes

- Uso de contêineres apropriados para as listas internas de ponteiros
- Adição de oportunidade para reduzir acessos ao disco desnecessários, introdução das Extensões do KDE
- Economia de alguns acessos ao disco

### KIO

- kurlnavigatortoolbutton.cpp - uso do <i>buttonWidth</i> no <i>paintEvent()</i>
- Novo menu de arquivo: filtragem de duplicados (p.ex. entre arquivos .qrc e de sistema) (erro <a href='https://bugs.kde.org/show_bug.cgi?id=355390'>355390</a>)
- Correcção de mensagem de erro no arranque do KCM de 'cookies'
- Remoção do 'kmailservice5', por ser prejudicial nesta altura (erro 354151)
- Correcção do KFileItem::refresh() nas ligações simbólicas. Estavam a ser definidos tamanhos, tipos e permissões errados
- Correcção de regressão no KFileItem: o refresh() iria perder o tipo do ficheiro, pelo que uma pasta se transformaria num ficheiro (erro 353195)
- Definição do texto no elemento QCheckbox em vez de usar uma legenda separada (erro 245580)
- Não activar o elemento das permissões da ACL se não formos os donos do ficheiro (erro 245580)
- Correcção de barra dupla nos resultados do KUriFilter quando for definido um filtro de nomes
- KUrlRequester: adição do sinal 'textEdited' (encaminhado do QLineEdit)

### KItemModels

- Correcção da sintaxe do modelo para a geração de casos de teste
- Correcção da compilação com o Qt 5.4 (#endif fora do sítio)

### KParts

- Correcção do formato da janela BrowserOpenOrSaveQuestion

### KPeople

- Adição de uma verificação da validade do PersonData

### KRunner

- Correcção do metainfo.yaml: o KRunner não é nem uma migração nem está descontinuado

### KService

- Remoção do tamanho máximo dos textos demasiado restrito na base de dados do KSycoca

### KTextEditor

- Uso da sintaxe adequada de caracteres '"' em vez de '\"'
- doxygen.xml: Uso da 'dsAnnotation' do estilo predefinido também para as "Marcas Personalizadas" (menos cores fixas)
- Adição de opção para mostrar a contagem das palavras
- Melhoria no contraste da cor princopal nos realces de pesquisa &amp; substituição
- Correcção de estoiro ao fechar o Kate através de DBus, enquanto a janela de impressão está aberta (erro #356813)
- Cursor::isValid(): adição de nota sobre o isValidTextPosition()
- Adição da API {Cursor, Range}::{toString, static fromString}

### KUnitConversion

- Informação ao cliente caso não se conheça a taxa de conversão
- Adição da moeda ILS (Novo Shekel Israelita) (erro 336016)

### KWallet Framework

- desactivação da reposição de sessões para o kwalletd5

### KWidgetsAddons

- KNewPasswordWidget: Remoção da sugestão de tamanhos nos espaços, o que estava a originar sempre algum espaço vazio na disposição
- KNewPasswordWidget: correcção do QPalette quando o elemento gráfico está desactivado

### KWindowSystem

- Correcção da geração da localização do 'plugin' XCB

### Plasma Framework

- [QuickTheme] Correcção das propriedades
- highlight/highlightedText do grupo de cores adequado
- ConfigModel: Não tentar resolver a localização de origem do pacote em branco
- [calendário] Mostrar apenas a marca de eventos na grelha dos dias, não dos meses ou anos
- declarativeimports/core/windowthumbnail.h - correcção de aviso -Wreorder
- actualização do tema de ícones de forma adequada
- Escrita sempre do nome do tema no 'plasmarc', mesmo que esteja escolhido o tema predefinido
- [calendário] Adição de uma marca aos dias que contêm um dado evento
- adição de cores de texto Positivas, Neutras e Negativas
- ScrollArea: Correcção de aviso quando o 'contentItem' não é possível inverter
- Adição de propriedade e método para alinhar o menu face a um canto do seu item-pai visual
- Permissão da alteração da largura mínima no Menu
- Manutenção da ordem na lista de itens guardada
- Extensão da API para permitir o novo posicionamento dos itens de menu durante a inserção de um procedimento
- associação da cor 'highlightText' ao Plasma::Theme
- Correcção da limpeza de 'application/urls' associados no Plasma::Applets
- Não expor os símbolos da classe privada DataEngineManager
- adição de um eelemento "evento" no SVG do calendário
- SortFilterModel: Invalidação do filtro ao mudar de 'callback' de filtragem

### Sonnet

- Instalação da ferramenta 'parsetrigrams' na compilação cruzada
- hunspell: Carregamento/gravação de um dicionário pessoal
- Suporte para o hunspell 1.4
- configwidget: notificação sobre a mudança de configuração quando as palavras ignoradas forem actualizadas
- configuração: não gravar imediatamente a configuração ao actualizar a lista de palavras a ignorar
- configwidget: correcção de gravação quando as palavras a ignorar são actualizadas
- Correcção do problema que impossibilitava a gravação de palavras a ignorar (erro 355973)

Você pode discutir e compartilhar ideias sobre esta versão na seção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
