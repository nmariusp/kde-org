---
aliases:
- ../../kde-frameworks-5.34.0
date: 2017-05-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- balooctl, baloosearch, balooshow: corrixir a orde da creación de obxectos de QCoreApplication (fallo 378539)

### Iconas de Breeze

- Engadir iconas de punto de acceso (https://github.com/KDAB/hotspot)
- Mellores iconas de sistemas de control de versións (fallo 377380)
- Engadir unha icona para Plasmate (fallo 376780)
- Actualizar as iconas microphone-sensitivity (fallo 377012)
- Aumentar o predeterminado das iconas de «Panel» a 48

### Módulos adicionais de CMake

- Desinfectadores: Non usar marcas como as de GCC para, p. ex., MSVC
- KDEPackageAppTemplates: melloras de documentación
- KDECompilerSettings: Pasar -Wvla e -Wdate-time
- Permitir versións anteriores de qmlplugindump
- Introducir ecm_generate_qmltypes
- Permitir aos proxectos incluír o ficheiro dúas veces
- Corrixir a expresión regular que obtén nomes de proxectos a partir do seu URI de Git
- Introducir a orde de construción fetch-translations (obter as traducións)
- Usar -Wno-gnu-zero-variadic-macro-arguments máis

### KActivities

- Só usamos infraestruturas de nivel 1, movémonos ao nivel 2
- Retirouse KIO das dependencias

### KAuth

- Corrección de seguranza: verificar que quen nos chama é quen di ser

### KConfig

- Corrixir o cálculo de relativePath en KDesktopFile::locateLocal() (fallo 345100)

### KConfigWidgets

- Definir a icona para a acción de doar
- Reducir as restricións para procesar QGroupBoxes

### KDeclarative

- Non definir ItemHasContents en DropArea
- Non permitir eventos de cubrir en DragArea

### KDocTools

- Apaño para MSVC e a carga de catálogos
- Resolver un conflito de visibilidade de meinproc5 (fallo 379142)
- Poñer outras variábeis de rutas entre comiñas (evitar problemas con espazos)
- Poñer algunhas variábeis de rutas entre comiñas (evitar problemas con espazos)
- Desactivar temporalmente a documentación local en Windows
- FindDocBookXML4.cmake e FindDocBookXSL.cmake - buscar nas instalacións caseiras

### KFileMetaData

- fai que KArchive sexa opcional e non constrúe os extractores que o necesitan
- corrixir o erro de compilación sobre símbolos duplicados con mingw en Windows

### KGlobalAccel

- build: Retirar a dependencia de KService

### KI18n

- corrixir a xestión de nomes base de ficheiros PO (fallo 379116)
- Corrixir a preparación inicial de ki18n

### KIconThemes

- Non intentar crear iconas sen tamaño

### KIO

- KDirSortFilterProxyModel: recuperar a ordenación natural (fallo 343452)
- Encher UDS_CREATION_TIME co valor de st_birthtime en FreeBSD
- Escravo de HTTP: enviar unha páxina de erro tras un fallo de autorización (erro 373323)
- kioexec: delegar o envío a un módulo de kded (fallo 370532)
- Corrixir unha proba de interface gráfica de usuario de KDirlister que definía o esquema de URL dúas veces
- Eliminar os módulos de kiod ao saír
- Xerar un ficheiro moc_predefs.h para KIOCore (fallo 371721)
- kioexec: corrixir a compatibilidade con --suggestedfilename

### KNewStuff

- Permitir varias categorías co mesmo nome
- KNewStuff: Mostrar a información de tamaño de ficheiro no delegado de grade
- Se se coñece o tamaño dunha entrada, mostralo na vista de lista
- Rexistrar e declarar KNSCore::EntryInternal::List como un metatipo
- Non caer pola opción. Entradas duplas? Non, por favor
- sempre pechar o ficheiro descargado tras descargalo

### Infraestrutura KPackage

- Corrixir a ruta de inclusión en KF5PackageMacros.cmake
- Ignorar avisos durante a xeración de appdata (fallo 378529)

### KRunner

- Modelo: Cambiar a categoría de modelo raíz a «Plasma»

### KTextEditor

- Integración de KAuth coa garda de documentación - vol. 2
- Corrixir a aserción ao aplicar pregado de código que cambia a posición do cursor
- Usar o elemento raíz non obsoleto &lt;gui&gt; no ficheiro ui.rc
- Engadir scroll-bar-marks tamén á busca e substitución incluída
- Integración de KAuth coa garda de documentos

### KWayland

- Validar que a superficie é válida ao enviar o evento de saída de TextInput

### KWidgetsAddons

- KNewPasswordWidget: non agochar a acción de visibilidade no modo de texto simple (fallo 378276)
- KPasswordDialog: non ocultar a acción de visibilidade no modo de texto simple (fallo 378276)
- Corrixir KActionSelectorPrivate::insertionIndex()

### KXMLGUI

- kcm_useraccount morreu, longa vida a user_manager
- Construcións reproducíbeis: retirar a versión de XMLGUI_COMPILING_OS
- Corrección: o nome DOCTYPE debe coincidir co tipo do elemento raíz
- Corrixir un uso incorrecto de ANY en kpartgui.dtd
- Usar o elemento raíz non obsoleto &lt;gui&gt;
- Correccións da documentación da API: substituír 0 por nullptr ou retirar onde corresponda

### NetworkManagerQt

- Corrixir a quebra ao obter a lista de conexións activas (fallo 373993)
- Definir o valor predeterminado de negociación automática baseado na versión de NM en execución

### Iconas de Oxygen

- Engadir unha icona de punto de acceso (https://github.com/KDAB/hotspot)
- Aumentar o predeterminado das iconas de «Panel» a 48

### Infraestrutura de Plasma

- cargar de novo a icona cando usesPlasmaTheme cambie
- Instalar a versión 3 dos compoñentes de Plasma para poder usalos
- Introducir units.iconSizeHints.* para fornecer consellos de tamaño de icona que os usuarios poden configurar (fallo 378443)
- [TextFieldStyle] Corrixir o erro de que textField non está definido
- Actualizar o apaño de ungrabMouse para Qt 5.8
- Impedir que Applet non cargue AppletInterface (fallo 377050)
- Calendario: Usar o idioma correcto para os nomes de mes e día
- Xerar os ficheiros plugins.qmltypes dos complementos que instalamos
- se o usuario non definiu un tamaño implícito, mantelo

### Solid

- Engadir unha inclusión necesaria para msys2

### Realce da sintaxe

- Engadir a extensión de Arduino
- LaTeX: Fix Incorrect termination of iffalse comments (bug 378487)

### Información de seguranza

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
