---
aliases:
- ../4.11
custom_about: true
custom_contact: true
date: 2013-08-14
description: KDE Ships Plasma Workspaces, Applications and Platform 4.11.
title: KDE tarkvarakomplekt 4.11
---
{{<figure src="/announcements/4/4.11.0/screenshots/plasma-4.11.png" class="text-center" caption=`KDE Plasma töötsoonid 4.11` >}} <br />

August 14, 2013. The KDE Community is proud to announce the latest major updates to the Plasma Workspaces, Applications and Development Platform delivering new features and fixes while readying the platform for further evolution. The Plasma Workspaces 4.11 will receive long term support as the team focuses on the technical transition to Frameworks 5. This then presents the last combined release of the Workspaces, Applications and Platform under the same version number.<br />

See väljalase on pühendatud Indiast pärit suure vaba ja avatud lähtekoodiga tarkvara tulihingelise eestvõitleja <a href='http://en.wikipedia.org/wiki/Atul_Chitnis'>Atul "toolz" Chitnise</a> mälestusele. Atul juhtis 2001. aastast saadik konverentse Linux Bangalore ja FOSS.IN, mis mõlemad olid India vaba ja avatud lähtekoodiga tarkvara kogukonna suursündmused. KDE India sündiski esimese FOSS.IN-i käigus detsembris 2005. Neilt üritustelt on tõuke saanud paljud India päritolu KDE kaasautorid. Just Atuli taganttõukamisel kujunes FOSS.IN-i KDE projekti päev alati nii tohutult edukaks. Atul lahkus meie seast 3. juunil, vandudes alla võitluses vähiga. Puhaku ta hing rahus. Me oleme talle tänulikud panuse eest parema maailma loomisel.

Väljalasked on tõlgitud 54 keelde ja me ootame keelte lisandumist järgnevatel kuudel, kui ilmuvad KDE vigu parandavad väljalasked. Selle väljalaske jaoks uuendas dokumentatsioonimeeskond 91 rakenduse käsiraamatut.

## <a href="./plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.11" width="64" height="64" /> Plasma Workspaces 4.11 Continues to Refine User Experience</a>

Pikaajalise hoolduse huvides pakuvad Plasma töötsoonid edasisi põhifunktsioonide täiendusi, näiteks sujuvamalt tegutsevat tegumiriba, nutikamat akuvidinat ja täiustatud helimikserit. Uus KScreen juurutab töötsoonide nutika mitme monitori halduse. Kõigele sellele lisandub suurel hulgal jõudluse parandusi ja kui liita väiksemad kasutatavuse täiustused, siis ootab kasutajat ees kindlasti senisest parem kogemus.

## <a href="./applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="The KDE Applications 4.11"/> KDE Applications 4.11 Bring Huge Step Forward in Personal Information Management and Improvements All Over</a>

See väljalase toob kaasa hulganisi täiustusi KDE PIM-i rakendustes, tagades tunduvalt parema jõudluse ja mitmeid uusi võimalusi. Kate täiustused lubavad neil, kellele meeldib kasutada Pythonit ja JavaScripti, uute pluginate abil produktiivsust suurendada. Dolphin on kiirem ning õpirakendused pakuvad mimesuguseid uusi võimalusi.

## <a href="./platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="The KDE Development Platform 4.11"/> KDE Platform 4.11 Delivers Better Performance</a>

KDE arendusplatvormi versioon 4.11 keskendub endiselt stabiilsusele. Tulevase KDE Frameworks 5.0 puhuks oleme näinu vaeva ka uute omadustega, aga stabiilses väljalaskes on meie tähelepanu keskmes olnud eelkõige Nepomuki raamistiku optimeerimine.

<br />
When upgrading, please observe the <a href='http://community.kde.org/KDE_SC/4.11_Release_Notes'>release notes</a>.<br />

## Levita sõnumit ja vaata, mis juhtub: kasuta silti &quot;KDE&quot;

KDE innustab kõiki inimesi levitama sõnumit ühismeedias. Saada lugusid uudistesaitidele, kasuta selliseid kanaleid, nagu delicious, digg, reddit, twitter, identi.ca. Laadi ekraanipilte sellistesse teenustesse, nagu Facebook, Flickr, ipernity ja Picasa, ning postita neid sobivatesse gruppidesse. Tee ekraanivideoid ja laadi need YouTube'i, Blip.tv-sse ja Vimeosse. Palun lisa postistustele ja üles laaditud materjalile silt &quot;KDE&quot;. See lubab neid hõlpsasti üles leida ning võimaldab KDE reklaamimeeskonnal analüüsida KDE tarkvarakomplekti väljalaske 4.11 kajastamist.

## Väljalaskepeod

Nagu ikka, korraldavad KDE kogukonna liikmed väljalasete ilmumise puhul peoüritusi kogu maailmas. Mõnedki on juba paika pandud, aga neid tuleb aina juurde. Teadaolevate <a href='http://community.kde.org/Promo/Events/Release_Parties/4.11'>pidude nimekirja näeb siin</a>. Kõik on kutsutud liituma! Sellised peod pakuvad lisaks söögile-joogile huvitavat seltskonda ja indu andvaid vestlusi. See on üks paremaid võimalusi teada saada, mis KDE-s toimub, lasta end kaasa tõmmata või ka lihtsalt kasutajate ja kaasautoritega kohtuda.

Me innustamine inimesi korraldama ise pidusid. Neid on lahe korraldada ja need on kõigile avatud! Uuri <a href='http://community.kde.org/Promo/Events/Release_Parties'>nõuandeid, kuidas pidu korraldada</a>.

## Väljalasketeadete teave

Väljalasketeated on koostanud Jos Poortvliet, Sebastian Kügler, Markus Slopianka, Burkhard Lück, Valorie Zimmerman, Maarten De Meyer, Frank Reininghaus, Michael Pyne, Martin Gräßlin ning teised KDE reklaamimeeskonna ja laiema KDE kogukonna liikmed. Need tõstavad esile viimase poole aasta olulisemad muutused KDE tarkvaras.

#### KDE toetamine

<a href="http://jointhegame.kde.org/"> <img src="/announcements/4/4.9.0/images/join-the-game.png" class="img-fluid float-left mr-3" alt="Join the Game"/> </a>

KDE e.V.'s new <a href='http://jointhegame.kde.org/'>Supporting Member program</a> is now open. For &euro;25 a quarter you can ensure the international community of KDE continues to grow making world class Free Software.
