---
aliases:
- ../announce-applications-17.12.3
changelog: true
date: 2018-03-08
description: KDE toob välja KDE rakendused 17.12.3
layout: application
title: KDE toob välja KDE rakendused 17.12.3
version: 17.12.3
---
March 8, 2018. Today KDE released the third stability update for <a href='../17.12.0'>KDE Applications 17.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Umbes 25 teadaoleva veaparanduse hulka kuuluvad Kontacti, Dolphini, Gwenview, JuKi, KGeti, Okulari, Umbrello ja teiste rakenduste täiustused.

Täiustused sisaldavad muu hulgas:

- Akregator ei kustuta enam tõrke järel .opml-vormingus voogude loendit
- Täisekraanirežiimis Gwenview näitab nüüd pärast failinime muutmist korralikult uut nime
- Okularis leiti mitme krahhi põhjus ja need parandati ära
