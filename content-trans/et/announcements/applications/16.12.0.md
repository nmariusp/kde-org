---
aliases:
- ../announce-applications-16.12.0
changelog: true
date: 2016-12-15
description: KDE toob välja KDE rakendused 16.12.0
layout: application
title: KDE toob välja KDE rakendused 16.12.0
version: 16.12.0
---
15. detsember 2016. KDE laskis täna välja KDE rakendused 16.12 märkimisväärse hulga uuendustega, mis on nüüd veelgi hõlpsamini kasutatavad, veelgi rohkemate võimalustega ja aina vabamad kõigist neist seni nördimust tekitanud pisivigadest, sõnaga, teel aina lähemale just sinu seadmele ideaalselt sobivate rakendusteni.

<a href='https://okular.kde.org/'>Okular</a>, <a href='https://konqueror.org/'>Konqueror</a>, <a href='https://www.kde.org/applications/utilities/kgpg/'>KGpg</a>, <a href='https://www.kde.org/applications/education/ktouch/'>KTouch</a>, <a href='https://www.kde.org/applications/education/kalzium/'>Kalzium</a> and more (<a href='https://community.kde.org/Applications/16.12_Release_Notes#Tarballs_that_were_based_on_kdelibs4_and_are_now_KF5_based'>Release Notes</a>) have now been ported to KDE Frameworks 5. We look forward to your feedback and insight into the newest features introduced with this release.

In the continued effort to make applications easier to build standalone, we have split the kde-baseapps, kdepim and kdewebdev tarballs. You can find the newly created tarballs at <a href='https://community.kde.org/Applications/16.12_Release_Notes#Tarballs_that_we_have_split'>the Release Notes document</a>

Me loobusime järgmistest pakettidest: kdgantt2, gpgmepp ja kuser. See aitab meil paremini keskenduda ülejäänud koodile.

### Heliredaktor Kwave liitub KDE rakendustega!

{{<figure src="https://www.kde.org/images/screenshots/kwave.png" width="600px" >}}

<a href='http://kwave.sourceforge.net/'>Kwave</a> is a sound editor, it can record, play back, import and edit many sorts of audio files including multi channel files. Kwave includes some plugins to transform audio files in several ways and presents a graphical view with a complete zoom and scroll capability.

### Maailm taustapildina

{{<figure src="https://frinring.files.wordpress.com/2016/08/screenshot_20160804_171642.png" width="600px" >}}

Marble sisaldab nüüd nii Plasma taustapilti kui ka vidinat, mis Maa satelliidivaate peal reaalajas kellaaega. Need olid saadaval ka Plasma 4 peal, uuendamise järel töötavad nüüd samuti Plasma 5 peal.

You can find more information on <a href='https://frinring.wordpress.com/2016/08/04/wip-plasma-world-map-wallpaper-world-clock-applet-powered-by-marble/'>Friedrich W. H. Kossebau's blog</a>.

### Emotikoniuputus!

{{<figure src="/announcements/applications/16.12.0/kcharselect1612.png" width="600px" >}}

KCharSelect has gained the ability to show the Unicode Emoticons block (and other SMP symbol blocks).

Samuti sai rakendus järjehoidjate menüü, nii et nüüd saab kõik oma lemmiktähed järjehoidjatesse panna.

### Julia muudab matemaatika paremaks

{{<figure src="https://2.bp.blogspot.com/-BzJNpF5SXZQ/V7skrKcQttI/AAAAAAAAAA8/7KD8g356FfAd9-ipPcWYi6QX5_nCQJFKgCLcB/s640/promo.png" width="600px" >}}

Cantoril on uus Julia taustaprogramm, mis annab kasutajatele võimaluse ära kasutada teaduslike arvutuste uusimaid edusamme.

You can find more information on <a href='https://juliacantor.blogspot.com/2016/08/cantor-gets-support-of-julia-language.html'>Ivan Lakhtanov's blog</a>.

### Täiustatum arhiveerimine

{{<figure src="https://rthomsen6.files.wordpress.com/2016/11/blog-1612-comp-method.png" width="600px" >}}

Ark sai mitmeid uusi omadusi:

- Faile ja katalooge saab nüüd otse arhiivi sees ümber nimetada, kopeerida või liigutada
- Nüüd saab arhiive luues valida tihendamis- ja krüpteerimisalgoritme
- Ark can now open AR files (e.g. Linux \*.a static libraries)

You can find more information on <a href='https://rthomsen6.wordpress.com/2016/11/26/new-features-in-ark-16-12/'>Ragnar Thomsen's blog</a>.

### Ja veel palju rohkem!

Kopete sai X-OAUTH2 SASL autentimise toetuse jabberi protokollis  ja parandas mõned probleemid OTR krüpteerimispluginas.

Kdenlive has a new Rotoscoping effect, support for downloadable content and an updated Motion Tracker. It also provides <a href='https://kdenlive.org/download/'>Snap and AppImage</a> files for easier installation.

KMail ja Akregator võivad Google Safe Browsingi abil kontrollida, ega klõpsatav link vii mõnele kurivara pakkuvale saidile. Samuti tõid mõlemad rakendused tagasi trükkimise võimaluse (selleks on vajalik Qt 5.8).

### Agressiivne võitlus kõikvõimalike vigadega

Üle 130 vea parandati sellistes rakendustes, nagu Dolphin, Akonadi, KDE aadressiraamat, KNotes, Akregator, Cantor, Ark, Kdenlive jt!

### Täielik muudatuste logi
