---
aliases:
- ../announce-applications-18.12.0
changelog: true
date: 2018-12-13
description: KDE lanza las Aplicaciones 18.12.
layout: application
release: applications-18.12.0
title: KDE lanza las Aplicaciones de KDE 18.12.0
version: 18.12.0
---
{{% i18n_date %}}

Publicadas las Aplicaciones de KDE 18.12.

{{%youtube id="ALNRQiQnjpo"%}}

Trabajamos continuamente en la mejora del software incluido en nuestras series de Aplicaciones de KDE y esperamos que encuentre útiles todas las nuevas mejoras y correcciones de errores.

## Novedades de las Aplicaciones de KDE 18.12

Se han corregido más de 140 errores en aplicaciones, que incluyen la suite Kontact, Ark, Cantor, Dolphin, Gwenview, Kate, KmPlot, Konsole, Lokalize, Okular, Spectacle y Umbrello, entre otras.

### Gestión de archivos

{{<figure src="/announcements/applications/18.12.0/app1812_dolphin01.png" width="600px" >}}

<a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, el potente gestor de archivos de KDE:

- Nueva implementación de MTP que la hace completamente estable para la producción.
- Enormes mejoras de rendimiento para la lectura de archivos sobre el protocolo SFTP.
- En las vistas previas de miniaturas, los marcos y las sombras solo se dibujan en las imágenes que no tienen transparencia, lo que mejora la visualización de iconos.
- Nuevas vistas previas de miniaturas para documentos de LibreOffice y para aplicaciones AppImage.
- Los archivos de vídeo con un tamaño superior a 5 MB también se muestran ya en las miniaturas de los directorios cuando se activa su visualización.
- Al leer CD de audio, Dolphin puede ahora cambiar la tasa de bits CBR para el codificador MP3 y corregir las marcas temporales en FLAC.
- El menú «Control» de Dolphin muestra ahora elementos «Crear nuevo...» y dispone de una entrada «Mostrar los lugares ocultos».
- Dolphin se cierra ahora cuando solo existe una pestaña abierta y se pulsa el acceso rápido de teclado estándar para cerrar pestañas (Ctrl+w).
- Ahora es posible volver a montar un volumen en el panel de «Lugares» tras haberlo desmontado previamente.
- La vista de «Documentos recientes» (disponible escribiendo «recentdocuments:/» en Dolphin) ahora muestra solo los documentos reales y filtra automáticamente las URL de páginas web.
- Dolphin muestra ahora una advertencia antes de permitirle cambiar el nombre de un archivo o directorio de tal forma que se cause que se oculte inmediatamente.
- Ya no es posible intentar desmontar los discos de su sistema operativo activo ni la carpeta personal del panel de «Lugares».

<a href='https://www.kde.org/applications/utilities/kfind'>KFind</a>, la búsqueda de archivos tradicional de KDE, dispone ahora de un método de búsqueda basado en KFileMetaData.

### Oficina

<a href='https://www.kde.org/applications/internet/kmail/'>KMail</a>, el potente cliente de correo electrónico de KDE:

- KMail puede mostrar hora una bandeja de entrada unificada.
- Nuevo complemento: generar un correo HTML usando el lenguaje Markdown.
- Uso de «Purpose» para compartir texto (como mensaje de correo).
- Los mensajes en HTML son legibles ahora independientemente del esquema de color en uso.

{{<figure src="/announcements/applications/18.12.0/app1812_okular01.png" width="600px" >}}

<a href='https://www.kde.org/applications/graphics/okular/'>Okular</a>, el versátil visor de documentos de KDE:

- Nueva herramienta de anotación «Máquina de escribir» que se puede usar para escribir texto en cualquier lugar.
- La vista de la tabla de contenidos jerárquica dispone ahora de la posibilidad de expandir y contraer todo o solo una sección específica.
- Se ha mejorado el comportamiento del ajuste dinámico de palabras en las anotaciones en línea.
- Al situar el cursor del ratón sobre un enlace, ahora se muestra su URL siempre que se pueda pulsar sobre él, en lugar de solo cuando se estaba en el modo de navegación.
- Ahora se muestran correctamente los archivos ePub que contienen recursos con espacios en sus URL.

### Desarrollo

{{<figure src="/announcements/applications/18.12.0/app1812_kate01.png" width="600px" >}}

<a href='https://www.kde.org/applications/utilities/kate/'>Kate</a>, el editor de texto avanzado de KDE:

- Cuando se usa la terminal empotrada, ahora se sincroniza automáticamente el directorio actual con la ubicación en disco del documento activo.
- Ahora se puede activar y desactivar el foco de la terminal empotrada de Kate usando el acceso rápido de teclado F4.
- El selector de pestañas integrado en Kate muestra ahora las rutas completas de los archivos de nombres similares.
- Los números de línea están activados ahora por omisión.
- El increíblemente útil y potente complemento de filtrado de texto está ahora activado por omisión y resulta más visible.
- Al volver a abrir un documento ya abierto usando la función de apertura rápida, se cambia a dicho documento.
- La función de apertura rápida ya no muestra entradas duplicadas.
- Si está usando múltiples actividades, los archivos se abren ahora en la actividad correcta.
- Kate muestra ahora todos los iconos correctos cuando se ejecuta en GNOME usando el tema de iconos de GNOME.

{{<figure src="/announcements/applications/18.12.0/app1812_konsole.png" width="600px" >}}

<a href='https://www.kde.org/applications/system/konsole/'>Konsole</a>, el emulador de terminal de KDE:

- Konsole permite ahora el uso completo de caracteres emoji.
- Los iconos de las pestañas inactivas se resaltan ahora cuando reciben una señal de sonido.
- Los dos puntos finales ya no se consideran partes de una palabra cuando se hace doble clic en ella para seleccionarla, lo que facilita la selección de rutas y la salida de «grep».
- Cuando se conecta un ratón que dispone de los botones «Atrás» y «Adelante», Konsole ya puede usar esos botones para cambiar de pestaña.
- Konsole dispone ahora de una opción de menú para reiniciar el tamaño del tipo de letra al predeterminado del perfil, si se ha modificado su tamaño.
- Ahora resulta más difícil desprender pestañas de forma accidental y más rápido ordenarlas con precisión.
- Se ha mejorado el comportamiento de la selección con Mayúsculas+clic.
- Se ha corregido el doble clic sobre líneas de texto que excedan la anchura de la ventana.
- La barra de búsqueda vuelve a cerrarse cuando se pulsa la tecla «Escape».

<a href='https://www.kde.org/applications/development/lokalize/'>Lokalize</a>, la herramienta de traducción de KDE:

- Ocultar los archivos traducidos en la pestaña del proyecto.
- Se ha añadido un uso básico de «pology», el sistema de comprobación de la sintaxis y del glosario.
- Navegación simplificada con ordenación de pestañas y apertura de varias pestañas.
- Se han corregido cuelgues de la aplicación debidos al acceso concurrente a objetos de la base de datos.
- Se ha corregido el comportamiento inconsistente de arrastrar y soltar.
- Se ha corregido un error en los accesos rápidos que eran distintos entre editores.
- Se ha mejorado el comportamiento de la búsqueda (ahora se encuentran y se muestran las formas plurales).
- Se ha restaurado la compatibilidad con Windows gracias al sistema de compilación «craft».

### Utilidades

<a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a>, el visor de imágenes de KDE:

- La herramienta de «reducción de ojos rojos» ha recibido diversas mejoras de usabilidad.
- Gwenview muestra ahora una advertencia cuando oculta la barra de menú que le indica cómo recuperarla.

{{<figure src="/announcements/applications/18.12.0/app1812_spectacle01.png" width="600px" >}}

<a href='https://www.kde.org/applications/graphics/spectacle/'>Spectacle</a>, la utilidad de capturas de pantalla de KDE:

- Spectacle puede ahora numerar de forma secuencial los archivos de las capturas de pantalla, y usa este esquema si borra el campo de texto del nombre de archivo.
- Se ha corregido el problema existente al guardar imágenes en un formato distinto de PNG al usar «Guardar como...».
- Si usa Spectacle para abrir una captura de pantalla en una aplicación externa, ahora es posible modificar y guardar la imagen cuando haya terminado con ella.
- Spectacle abre ahora la carpeta correcta al hacer clic en «Herramientas > Abrir la carpeta de capturas de pantalla».
- Las marcas temporales de las capturas de pantalla muestran ahora cuándo se creó la imagen, no cuándo se guardó.
- Todas las opciones de guardar están situadas ahora en la página «Guardar».

<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, el gestor de archivos comprimidos de KDE:

- Se ha añadido la implementación del formato Zstandard (archivos comprimidos tar.zst).
- Se ha corregido la vista previa de algunos archivos en Ark (por ejemplo, Open Document) como archivos comprimidos en lugar de abrirlos en la aplicación apropiada.

### Matemáticas

<a href='https://www.kde.org/applications/utilities/kcalc/'>KCalc</a>, la sencilla calculadora de KDE, dispone ahora de un ajuste para repetir el último cálculo varias veces.

<a href='https://www.kde.org/applications/education/cantor/'>Cantor</a>, la interfaz matemática de KDE:

- Se ha añadido el tipo de entrada en Markdown.
- Resaltado animado de la entrada de orden actualmente calculada.
- Visualización de las entradas de órdenes pendientes (en cola, pero todavía sin calcular).
- Permitir formatear la entradas de órdenes (propiedades de color de fondo, color de primer plano y tipo de letra).
- Permitir insertar nuevas entradas de órdenes en cualquier lugar de la hoja de trabajo situando el cursor en la posición deseada y comenzando a escribir.
- En las expresiones que tienen varias órdenes, mostrar el resultado como objetos de resultado independientes en la hoja de trabajo.
- Ahora es posible abrir hojas de trabajo por rutas relativas desde la consola.
- Ahora es posible abrir varios archivos en un intérprete de Cantor.
- Cambiar el color y el tipo de letra cuando se solicita información adicional para poder distinguirla mejor de la entrada normal en la línea de órdenes.
- Se han añadido accesos rápidos de teclado para la navegación entre hojas de trabajo (Ctrl+AvPág, Ctrl+RePág).
- Se ha añadido una acción en el submenú «Ver» para reiniciar la ampliación.
- Se ha activado la descarga de proyectos de Cantor desde store.kde.org (por ahora, los envíos solo funcionan con el sitio web).
- Abrir la hoja de trabajo en modo de solo lectura si el motor no está disponible en el sistema.

Numerosos problemas corregidos en <a href='https://www.kde.org/applications/education/kmplot/'>KmPlot</a>, el trazador de funciones de KDE:

- Se han corregido los nombres incorrectos de los gráficos de derivadas e integrales en la notación convencional.
- La exportación a SVG de KmPlot funciona ahora correctamente.
- La funcionalidad de la primera derivada no tiene notación prima.
- Ahora se oculta el gráfico de las funciones desmarcadas que no tengan el foco.
- Se ha solucionado un cuelgue de KmPlot que se producía al abrir «Editar constantes» de forma recursiva desde el editor de funciones.
- Se ha solucionado un cuelgue de KmPlot que se producía tras borrar una función seguida por el puntero del ratón en el gráfico.
- Ahora puede exportar los datos dibujados como cualquier formato de imagen.
