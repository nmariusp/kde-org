---
aliases:
- ../announce-applications-17.12-rc
custom_spread_install: true
date: 2017-12-01
description: KDE lanza la versión candidata de las Aplicaciones 17.12.
layout: application
release: applications-17.11.90
title: KDE lanza la candidata a versión final para las Aplicaciones 17.12
---
Hoy, 1 de diciembre de 2017, KDE ha lanzado la candidata a versión final de las nuevas Aplicaciones. Una vez congeladas las dependencias y las funcionalidades, el equipo de KDE se ha centrado en corregir errores y en pulir aún más la versión.

Consulte las <a href='https://community.kde.org/Applications/17.12_Release_Notes'>notas de lanzamiento de la comunidad</a> para obtener información sobre nuevos paquetes que ahora se basan en KF5 y sobre los problemas conocidos. Se realizará un anuncio más completo para la versión final.

Los lanzamientos de las Aplicaciones de KDE 17.12 necesitan una prueba exhaustiva con el fin de mantener y mejorar la calidad y la experiencia del usuario. Los usuarios reales son críticos para mantener la alta calidad de KDE porque los desarrolladores no pueden probar todas las configuraciones posibles. Contamos con ustedes para ayudar a encontrar errores de manera temprana de forma que se puedan corregir antes de la versión final. Considere la idea de unirse al equipo instalando la versión candidata <a href='https://bugs.kde.org/'>e informando de cualquier error que encuentre</a>.

#### Instalación de paquetes binarios de la candidata a versión final de las Aplicaciones de KDE 17.12.

<em>Paquetes</em>. Algunos distribuidores del SO Linux/UNIX tienen la gentileza de proporcionar paquetes binarios de la candidata a versión final de las Aplicaciones de KDE 17.12 (internamente, 17.11.90) para algunas versiones de sus distribuciones; en otros casos han sido voluntarios de la comunidad los que lo han hecho posible. En las próximas semanas estarán disponibles paquetes binarios adicionales, así como actualizaciones de los paquetes disponibles en este momento.

<em>Paquetes</em>. Para obtener una lista actualizada de los paquetes binarios disponibles de los que el Equipo de Lanzamiento de KDE tiene conocimiento, visite la <a href='http://community.kde.org/Binary_Packages'>Wiki de la Comunidad</a>.

#### Compilación de la candidata a versión final de las Aplicaciones de KDE 17.12

La totalidad del código fuente de la candidata a versión final de las Aplicaciones de KDE 17.12 se puede <a href='http://download.kde.org/unstable/applications/17.11.90/src/'>descargar libremente</a>. Dispone de instrucciones sobre cómo compilar e instalar dicha versión en la <a href='/info/applications/applications-17.11.90'>página de información sobre la candidata a versión final de las Aplicaciones de KDE 17.12</a>.
