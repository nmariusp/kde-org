---
date: '2013-12-18'
hidden: true
title: Die KDE-Anwendungen 4.12 bringen erhebliche Verbesserungen in der Verwaltung
  persönlicher Daten und allgemeine Verbesserungen
---
Die KDE-Gemeinschaft freut sich, die neueste größere Aktualisierung der KDE-Anwendungen mit neuen Funktionen und Fehlerkorrekturen zu veröffentlichen. Diese Veröffentlichung enthält umfangreiche Verbesserungen der KDE-PIM-Programme mit besserer Leistungsfähigkeit und vielen neuen Funktionen. Für Kate wurde die Integration von Python-Modulen verbessert und die erste Unterstützung für Vim-Makros hinzugefügt. Bei den Spielen und Lernprogrammen gibt es eine Vielzahl von neuen Funktionen.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.12.0/screenshots/kate.png" width="600px">}}

Bei Kate, dem fortschrittlichsten graphischen Texteditor für Linux, wurde weiter an der Quelltextvervollständigung gearbeitet. Dieses Mal gibt es <a href='http://scummos.blogspot.com/2013/10/advanced-code-completion-filtering-in.html'>erweiterte Quelltextsuche, Abkürzungen und teilweise Übereinstimmung in Klassen</a> als Neuheit. So wird zum Beispiel ein eingegebenes ‚QualIdent‘ mit ‚QualifiedIdentifier‘ vervollständigt. Außerdem gibt es jetzt eine <a href='http://dot.kde.org/2013/09/09/kde-commit-digest-18th-august-2013'>erste Unterstützung für Vim-Makros</a>. Besonders toll ist, dass diese Verbesserungen auch zu KDevelop und anderen Anwendungen, die die Kate-Komponente nutzen, durchgereicht werden.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.12.0/screenshots/okular.png" width="600px">}}

Der Dokumentbetrachter Okular berücksichtigt jetzt die <a href='http://tsdgeos.blogspot.com/2013/10/changes-in-okular-printing-for-412.html'>druckbaren Bereiche von Druckern</a>, hat bessere Unterstützung für Audio und Video für EPUB, eine verbesserte Suche und kann jetzt mehr Transformationen einschließlich von Exif-Bildmetadaten verarbeiten. Im UML-Diagrammprogramm Umbrello können Assoziationen jetzt in unterschiedlichen <a href='http://dot.kde.org/2013/09/20/kde-commit-digest-1st-september-2013'>Layouts</a> gezeichnet werden. Außerdem gibt es jetzt in Umbrello eine <a href='http://dot.kde.org/2013/09/09/kde-commit-digest-25th-august-2013'>visuelle Anzeige, wenn ein Bedienelement dokumentiert ist</a>.

Das Verschlüsselungsprogramm KGpg zeigt mehr Informationen für den Benutzer an und der Passwortspeicher KWallet kann Ihre Passwörter jetzt auch im <a href='http://www.rusu.info/wp/?p=248'>GPG-Format</a> speichern. In Konsole gibt es eine neue Funktion: Klicken Sie mit gedrückter Strg-Taste, um Adressen (URLs) in der Konsolenausgabe direkt zu öffnen. Jetzt können ebenfalls <a href='http://martinsandsmark.wordpress.com/2013/11/02/mangonel-1-1-and-more/'>Prozesse bei der Warnung beim Beenden aufgelistet werden</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.12.0/screenshots/dolphin.png" width="600px">}}

In KWebKit wurde die Fähigkeit zum <a href='http://dot.kde.org/2013/08/09/kde-commit-digest-7th-july-2013'>automatischen Skalieren des Inhalts passend zur Auflösung der Arbeitsfläche</a> hinzugefügt. Beim Dateiverwaltungsprogramm Dolphin gibt es eine Anzahl von Leistungssteigerungen beim Sortieren und der Anzeige von Dateien. Dadurch wird die Speicherbelegung verringert und der Ablauf beschleunigt. KRDC kann jetzt automatisch VNC-Verbindungen wiederherstellen. In KDialog gibt es jetzt „detailedsorry“- und „detailederror“-Meldungen für informativere Dialoge für Konsolenskripte. Kopete hat das OTR-Modul aktualisiert und das Jabber-Protokoll hat jetzt Unterstützung für XEP-0264: Vorschaubilder für Dateiübertragungen. Zusätzlich zu diesen neuen Funktionen wurde der Quelltext aufgeräumt und Compiler-Warnungen beseitigt.

### Spiele und Lernsoftware

Die KDE-Spiele wurden in mehreren Bereichen überarbeitet. KReversi basiert jetzt auf <a href='http://tsdgeos.blogspot.ch/2013/10/kreversi-master-is-now-qt-quick-based.html'>QML und Qt Quick</a> und bietet daher ein schöneres und flüssigeres Spielerlebnis. KNetWalk wurde ebenfalls mit den gleichen Vorteilen <a href='http://tsdgeos.blogspot.ch/2013/08/knetwalk-portedx-to-qtquick.html'>portiert</a> und es kann jetzt ein Spielfeld mit  benutzerdefinierter Breite und Höhe eingestellt werden. Konquest hat jetzt einen anspruchsvollen KI-Spielmodus mit dem Namen „Becai“.

Es gab einige tiefgreifende Änderungen bei den Lernprogrammen. KTouch <a href='http://blog.sebasgo.net/blog/2013/11/12/what-is-new-for-ktouch-in-kde-sc-4-dot-12/'>bietet jetzt benutzerdefinierte Lektionen und mehrere neue Kurse </a>; KStars enthält ein neues und genaueres <a href='http://knro.blogspot.ch/2013/10/demo-of-ekos-alignment-module.html'>Ausrichtungsmodul für Teleskope</a>, <a href='http://www.youtube.com/watch?v=7Dcn5aFI-vA'>hier gibt es ein Youtube-Video</a> über die neuen Funktionen. Cantor, das eine leicht zu benutzende und mächtige Oberfläche für eine Reihe von Mathematik-Systemen bietet, unterstützt jetzt <a href='http://blog.filipesaraiva.info/?p=1171'>Python2 und Scilab</a>. Mehr über das umfangreiche Scilab-Backend gibt es <a href='http://blog.filipesaraiva.info/?p=1159'>hier</a>. Marble bietet jetzt eine Integration von ownCloud (kann im Einrichtungsdialog eingestellt werden) und die Darstellung von Einblendungen. KAlgebra kann jetzt 3D-Plots nach PDF exportieren, was eine tolle Art ist, wissenschaftliche Arbeiten miteinander zu teilen. Außerdem wurden viele Fehler in den KDE-Lernprogrammen beseitigt.

### E-Mail, Kalender und persönliche Informationen

KDE-PIM. die KDE-Anwendungen für den Umgang mit E-Mail, Kalender und persönliche Informationen, wurde in großem Maße weiterentwickelt.

Um mit dem E-Mail-Programm KMail zu beginnen, gibt es jetzt <a href='http://dot.kde.org/2013/10/11/kde-commit-digest-29th-september-2013'>Unterstützung für Werbefilter</a> (bei angeschalteter Anzeige von HTML-Nachrichten) und eine verbesserte Erkennung von betrügerischen E-Mails, indem gekürzte URLs ausgeschrieben werden. Ein neuer Akonadi-Agent namens FolderArchiveAgent ermöglicht es, gelesene E-Mails in festgelegten Ordnern zu archivieren. Außerdem wurde die GUI für die „Später senden“-Funktion aufgeräumt. KMail profitiert des weiteren von einer verbesserten Unterstützung für „Sieve“-Filter. Mit ihnen können E-Mails auf dem Server gefiltert werden und Sie können ab jetzt <a href='http://www.aegiap.eu/kdeblog/2013/08/new-in-kdepim-4-12-sieve-script-parsing-22/'>Filter auf dem Server erzeugen und anpassen</a> sowie <a href='http://www.aegiap.eu/kdeblog/2013/08/new-in-kdepim-4-12-sieve-12/'>bestehende KMail-Filter in Serverfilter umwandeln</a>. KMails MBox-Unterstützung <a href='http://www.aegiap.eu/kdeblog/2013/08/new-in-kdepim-4-12-mboximporter/'>wurde ebenso verbessert</a>.

In anderen Anwendungen machen mehrere Änderungen die Arbeit leichter und erfreulicher. Es gibt als neues Hilfsprogramm den <a href='http://www.aegiap.eu/kdeblog/2013/11/new-in-kdepim-4-12-kaddressbook/'>Editor für Kontaktdesigns</a>. Damit können Grantlee-Designs zur Anzeige von Kontakten im KAddressBook erzeugt werden. Im Adressbuch gibt es jetzt eine Druckvorschau. Für KNotes wurde viel Arbeit zur <a href='http://www.aegiap.eu/kdeblog/2013/11/what-news-in-kdepim-4-12-knotes/'>Korrektur von Fehlern</a> geleistet. Das Programm Blogilo zum Bloggen kann jetzt mit Übersetzungen umgehen und es gibt eine Vielzahl von Fehlerkorrekturen und Verbesserungen für alle KDE-PIM-Anwendungen.

Es wurde <a href='http://ltinkl.blogspot.ch/2013/11/this-month-october-in-red-hat-kde.html'>viel Arbeit zur Leistungssteigerung, Stabilität und Skalierbarkeit</a> in den KDE-PIM-Datenspeicher investiert, wovon viele Anwendungen profitieren. Dazu gehören Fehlerkorrekturen in der <a href='http://www.progdan.cz/2013/10/akonadi-1-10-3-with-postgresql-fix/'>Unterstützung für PostgreSQL mit dem neuesten Qt 4.8.5</a>. Es gibt ein neues Befehlszeilenprogramm „calendarjanitor“, das alle Kalenderdaten nach fehlerhaften Einträgen durchsucht sowie einen Dialog zur Fehlersuche für die Suche bringt. Besonderer Dank geht an Laurent Montel für seine Arbeit an den Funktionen von KDE-PIM.

#### KDE-Programme installieren

KDE-Software einschließlich all ihrer Bibliotheken und Anwendungen ist kostenlos unter Open-Source-Lizenzen erhältlich. KDE-Software läuft auf verschiedenen Betriebssystemen, Hardware-Konfigurationen und Prozessorarchitekturen wie z.B. ARM und x86, und es arbeitet mit jeder Art von Fensterverwaltung und Arbeitsumgebung zusammen. Neben Linux und anderen UNIX-basierten Betriebssystemen sind von den meisten KDE-Anwendungen auch Versionen für Microsoft Windows und Apple Mac OS X erhältlich. Sie sind auf den Seiten <a href='http://windows.kde.org'>KDE-Software für Windows</a> bzw. <a href='http://mac.kde.org/'>KDE-Software für den Mac</a> erhältlich. Experimentelle Versionen von KDE-Anwendungen für verschiedene Mobilplattformen wie MeeGo, MS Windows Mobile und Symbian sind im Internet erhältlich, werden zur Zeit aber nicht unterstützt. <a href='http://plasma-active.org'>Plasma Active</a> ist eine Benutzerumgebung für ein großes Spektrum an Geräten wie beispielsweise Tabletts und andere mobile Hardware.

Die KDE-Software kann als Quelltext und in verschiedenen binären Formaten von <a href='http://download.kde.org/stable/4.12.0'>download.kde.org</a> heruntergeladen und auch auf <a href='/download'>CD-ROM</a> und von vielen großen <a href='http://download.kde.org/stable/%1'>GNU/Linux- und UNIX-Systemen</a> bezogen werden.

##### Pakete

Einige Anbieter von Linux-/UNIX-Betriebssystemen haben dankenswerterweise Binärpakete von 4.12.0für einige Versionen Ihrer Distributionen bereitgestellt, ebenso wie freiwillige Mitglieder der Gemeinschaft,

##### Paketquellen

Eine aktuelle Liste aller Binärpakete, von denen das Release-Team in Kenntnis gesetzt wurde, finden Sie im <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_SC_4.12.0'>Community-Wiki</a>.

Der vollständige Quelltext für 4.12.0 kann <a href='/info/4/4.12.0'>hier</a> heruntergeladen werden. Anweisungen zum Kompilieren und Installieren von %1 finden Sie auf der <a href='/info/4/4.12.0#binary'>Infoseite zu 4.12.0</a>.

#### Systemanforderungen

Um den größtmöglichen Nutzen aus den Veröffentlichungen zu ziehen, empfehlen wir, eine aktuelle Version von Qt zu verwenden, etwa 4.8.4. Dies ist nötig, um eine stabile und performante Erfahrung zu bieten, weil einige KDE-Verbesserungen in Wahrheit am darunterliegenden Qt-Framework ansetzen.

Um den vollen Umfang an Fähigkeiten der KDE-Software zu nutzen, empfehlen wir außerdem, den neuesten Grafiktreiber für Ihr System zu verwenden, weil dies den Nutzungseindruck maßgeblich verbessert, sowohl bei wählbaren Funktionen als auch bei der allgemeinen Leistung und Stabilität.
