---
aliases:
- ../4.11
custom_about: true
custom_contact: true
date: 2013-08-14
description: KDE veröffentlicht die Plasma-Arbeitsbereiche, Anwendungen und Plattform
  4.11
title: KDE-Software-Sammlung 4.11
---
{{<figure src="/announcements/4/4.11.0/screenshots/plasma-4.11.png" class="text-center" caption=`Der KDE-Plasma-Arbeitsbereich 4.11` >}} <br />

14. August 2013. Die KDE-Gemeinschaft freut sich, die neueste größere Aktualisierung der Plasma-Arbeitsbereiche, der Anwendungen und Entwicklerplattform mit neuen Funktionen und Fehlerkorrekturen zu veröffentlichen und durch die Plattform für die weitere Entwicklung vorzubereiten. Der Plasma-Arbeitsbereich 4.11 wird langfristige Unterstützung erhalten, da sich das Team auf den technischen Übergang zu Frameworks 5 konzentrieren wird. Dies ist die letzte gemeinsame Veröffentlichung der Arbeitsbereiche, Anwendungen und Plattform mit der gleichen Versionsnummer.<br />

Diese Veröffentlichung ist dem Andenken an <a href='http://en.wikipedia.org/wiki/Atul_Chitnis'>Atul 'toolz' Chitnis</a> gewidmet, einem großartigen Verfechter von freier und quelloffener Software aus Indien. Atul leitete seit 2001 die Linux-Konferenz in Bangalore sowie FOSS.in, die beide wegweisende Veranstaltungen in der indischen FOSS-Szene sind. KDE Indien wurde auf der ersten FOSS.in im Dezember 2005 gegründet. Viele indische KDE-Mitwirkende begannen auf diesen Veranstaltungen. Nur durch Atuls Förderung wurde der KDE-Projekttag auf der FOSS.IN stets ein großer Erfolg. Atul ist am 3. Juni nach einem Kampf mit dem Krebs von uns gegangen. Möge seine Seele in Frieden ruhen. Wir sind für seinen Beitrag für eine bessere Welt sehr dankbar.

Diese Veröffentlichungen sind alle in 54 Sprachen übersetzt. Es ist zu erwarten, dass weitere Sprachen mit den folgenden monatlichen Veröffentlichen mit Fehlerkorrekturen durch KDE hinzukommen. Das Dokumentationsteam hat 91 Handbücher für Anwendungen für diese Veröffentlichung aktualisiert.

## <a href="./plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.11" width="64" height="64" />Plasma-Arbeitsbereich 4.11 verbessert weiter die Benutzererfahrung </a>

Mit dem Ziel der langfristigen Unterstützung im Auge wurde der Plasma-Arbeitsbereich in grundlegenden Funktionen weiter verbessert. So arbeitet die Fensterleiste flüssiger, die Batterieverwaltung wurde intelligenter und der Mixer erhielt Verbesserungen. Durch die Einführung von KScreen erhielt die Arbeitsfläche eine intelligente Verwaltung für Mehrbildschirmbetrieb. Insgesamt wurde die Nutzererfahrung sowohl durch große Leistungsverbesserungen als auch durch kleine Änderungen an der Nutzbarkeit verbessert,

## <a href="./applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="The KDE Applications 4.11"/> Die KDE-Anwendungen 4.11 bringen erhebliche Verbesserungen in der Verwaltung persönlicher Daten und allgemeine Verbesserungen</a>

Diese Veröffentlichung bringt enorme Verbesserungen für die KDE-PIM-Anwendungen mit verbesserter Leistung und vielen neuen Funktionen. Für Kate gibt es verbesserte Produktivität für Python- und JavaScript-Entwicklern mit neuen Erweiterungen. Die Geschwindigkeit von Dolphin wurde gesteigert und für die Lernprogramme gibt es verschiedene neue Funktionen.

## <a href="./platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="The KDE Development Platform 4.11"/> Die KDE-Plattform 4.11 liefert eine bessere Leistung</a>

Diese Veröffentlichung der KDE-Plattform 4.11 legt weiterhin großen Wert auf Stabilität. Neue Funktionen werden für unsere zukünftigen Veröffentlichung der KDE-Frameworks 5.0 implementiert, aber für diese stabile Version wurde das Nepomuk-Framework optimiert.

<br />
Beachten Sie bei der Aktualisierung bitte die <a href='http://community.kde.org/KDE_SC/4.11_Release_Notes'>Hinweise zur Veröffentlichung</a>.<br />

## Verbreiten Sie diese Neuigkeit und sehen Sie was passiert: Geben Sie als Stichwort &quot;KDE&quot; an

KDE ermuntert alle Personen, diese Neuigkeit im sozialen Web zu verbreiten. Schicken Sie Artikel an News-Seiten. Benutzen Sie Kanäle wie delicious, digg, reddit, twitter, identi.ca. Laden Sie Bildschirmfotos auf Dienste wie Facebook, Flickr, ipernity und Picasa hoch und posten sie in passenden Gruppen. Erstellen Sie Videos und laden Sie sie auf YouTube, Blip.tv und  Vimeo hoch. Bitte geben Sie Posts und hochgeladenem Material das Stichwort &quot;KDE&quot;. Dann sind sie einfach zu finden und es gibt dem KDE-Promo-Team die Möglichkeit, die Verbreitung der Version 4.11 der KDE-Software zu ermitteln.

## Partys zur Veröffentlichung

Wie immer organisieren Mitglieder der KDE-Gemeinschaft Veröffentlichungs-Parties überall in der Welt. Mehrere Parties sind bereits geplant und weitere werden noch hinzukommen. Eine Liste dieser Parties finden Sie <a href='http://community.kde.org/Promo/Events/Release_Parties/4.11'>hier</a>. Jeder kann teilnehmen. Es gibt sowohl interessierte Firmen und anregende Gespräche wie auch Essen und Getränke. Das ist eine großartige Gelegenheit, mehr über die Entwicklung von KDE zu lernen, beteiligen Sie sich oder treffen Sie einfach nur andere Benutzer und Mitwirkende.

Wir ermuntern alle Personen. Ihre eigenen Parties zu organisieren. Es macht Spaß, diese Verarbeitungen zu organisieren und sie können von allen Personen besucht werden. Lesen Sie hier <a href='http://community.kde.org/Promo/Events/Release_Parties'>Hinweise zur Organisation einer Party</a>.

## Über diese Ankündigungen zu den Veröffentlichungen

Diese Ankündigungen zur Veröffentlichung wurde von Jos Poortvliet, Sebastian Kügler, Markus Slopianka, Burkhard Lück, Valorie Zimmerman, Maarten De Meyer, Frank Reininghaus, Michael Pyne, Martin Gräßlin und anderen Mitgliedern des KDE-Promotionsteam wie auch der weiteren KDE-Gemeinschaft vorbereitet. Diese Ankündigungen enthalten die wichtigsten der vielen Änderungen an der KDE-Software in den vergangenen sechs Monaten.

#### KDE unterstützen

<a href="http://jointhegame.kde.org/"> <img src="/announcements/4/4.9.0/images/join-the-game.png" class="img-fluid float-left mr-3" alt="Join the Game"/> </a>

Das neue Programm <a href='http://jointhegame.kde.org/'>Unterstützung durch Mitglieder</a> des KDE e.V. ist jetzt gestartet. Für 25 &euro; im Vierteljahr können Sie sicherstellen, dass die internationale KDE-Gemeinschaft weiterhin wächst, um erstklassige freie Software zu erstellen.
