---
aliases:
- ../../kde-frameworks-5.54.0
date: 2019-01-12
layout: framework
libCount: 70
---
###  Attica

- Notify if a default provider failed to download

### Baloo

- Move typesForMimeType helper from BasicIndexingJob to anonymous namespace
- Add "image/svg" as Type::Image to the BasicIndexingJob
- Use Compact json formatting for storing document metadata

### Breeze Icons

- Change preferences-system-network to symlink
- Use a Kolf icon with nice background and shadows
- Copy some changed icons from Breeze to Breeze Dark
- Add YaST and new preference icons
- Add a proper python-bytecode icon, use consistent color in python icons (bug 381051)
- Delete python bytecode symlinks in preparation for them becoming their own icons
- Make the python mimetype icon the base, and the python bytecode icon a link to it
- Add device icons for RJ11 and RJ45 ports
- Add missing directory separator (bug 401836)
- Use correct icon for Python 3 scripts (bug 402367)
- Change network/web color icons to consistent style
- Add new name for sqlite files, so the icons actually show up (bug 402095)
- Add drive-* icons for YaST Partitioner
- Add view-private icon (bug 401646)
- Add flashlight action icons
- Improve symbolism for off and muted status icon (bug 398975)

### Extra CMake-Module

- Add find module for Google's libphonenumber

### KActivities

- Fix the version in the pkgconfig file

### KDE Doxygen Tools

- Fix doxygen markdown rendering

### KConfig

- Escape bytes that are larger than or equal to 127 in config files

### KCoreAddons

- cmake macros: Port away from deprecated ECM var in kcoreaddons_add_plugin (bug 401888)
- make units and prefixes of formatValue translatable

### KDeclarative

- don't show separators on mobile
- root.contentItem instead of just contentItem
- Add the missing api for multilevel KCMs to control the columns

### KFileMetaData

- Fail writing test if mime type is not supported by the extractor
- Fix ape disc number extraction
- Implement cover extraction for asf files
- Extend list of supported mimetypes for embedded image extractor
- Refactor embedded image extractor for greater extensibility
- Add missing wav mimetype
- Extract more tags from exif metadata (bug 341162)
- fix extraction of GPS altitude for exif data

### KGlobalAccel

- Fix KGlobalAccel build with Qt 5.13 prerelease

### KHolidays

- README.md - add basic instructions for testing holiday files
- various calendars - fix syntax errors

### KIconThemes

- ksvg2icns : use Qt 5.9+ QTemporaryDir API

### KIdleTime

- [xscreensaverpoller] Flush after reset screensaver

### KInit

- Use soft rlimit for number of open handles. This fixes very slow Plasma startup with latest systemd.

### KIO

- Revert "Hide file preview when icon is too small"
- Display error instead of silently failing when asked to create folder that already exists (bug 400423)
- Change the path for every item of the subdirectories in a directory rename (bug 401552)
- Extend getExtensionFromPatternList reg exp filtering
- [KRun] when asked to open link in external browser, fall back to mimeapps.list if nothing is set in kdeglobals (bug 100016)
- Make the open url in tab feature a bit more discoverable (bug 402073)
- [kfilewidget] Return editable URL navigator to breadcrumb mode if it has focus and everything is selected and when Ctrl+L is pressed
- [KFileItem] Fix isLocal check in checkDesktopFile (bug 401947)
- SlaveInterface: Stop speed_timer after a job is killed
- Warn user before copy/move job if the file size exceeds the maximum possible file size in FAT32 filesystem(4 GB) (bug 198772)
- Avoid constantly increasing Qt event queue in KIO slaves
- Support for TLS 1.3 (part of Qt 5.12)
- [KUrlNavigator] Fix firstChildUrl when going back from archive

### Kirigami

- Make sure we don't override QIcon::themeName when we should not
- Introduce a DelegateRecycler attached object
- fix gridview margins considering scrollbars
- Make AbstractCard.background react to AbstractCard.highlighted
- Simplify code in MnemonicAttached
- SwipeListItem: always show the icons if !supportsMouseEvents
- Consider whether needToUpdateWidth according to widthFromItem, not height
- Take the scrollbar into account for the ScrollablePage margin (bug 401972)
- Don't try to reposition the ScrollView when we get a faulty height (bug 401960)

### KNewStuff

- Change default sort order in the download dialog to "Most downloads" (bug 399163)
- Notify about the provider not being loaded

### KNotification

- [Android] Fail more gracefully when building with API &lt; 23
- Add Android notification backend
- Build without Phonon and D-Bus on Android

### KService

- applications.menu: remove unused category X-KDE-Edu-Teaching
- applications.menu: remove &lt;KDELegacyDirs/&gt;

### KTextEditor

- Fix scripting for Qt 5.12
- Fix emmet script by using HEX instead of OCT numbers in strings (bug 386151)
- Fix broken Emmet (bug 386151)
- ViewConfig: Add 'Dynamic Wrap At Static Marker' option
- fix folding region end, add ending token to the range
- avoid ugly overpainting with alpha
- Don't re-mark words added/ignored to the dictionary as misspelled (bug 387729)
- KTextEditor: Add action for static word wrap (bug 141946)
- Don't hide 'Clear Dictionary Ranges' action
- Don't ask for confirmation when reloading (bug 401376)
- class Message: Use inclass member initialization
- Expose KTextEditor::ViewPrivate:setInputMode(InputMode) to KTextEditor::View
- Improve performance of small editing actions, e.g. fixing large replace all actions (bug 333517)
- Only call updateView() in visibleRange() when endPos() is invalid

### KWayland

- Add clarifying about using both KDE's ServerDecoration and XdgDecoration
- Xdg Decoration Support
- Fix XDGForeign Client header installs
- [server] Touch drag support
- [server] Allow multiple touch interfaces per client

### KWidgetsAddons

- [KMessageBox] Fix minimum dialog size when details are requested (bug 401466)

### NetworkManagerQt

- Added DCB, macsrc, match, tc, ovs-patch and ovs-port settings

### Plasma Framework

- [Calendar] Expose firstDayOfWeek in MonthView (bug 390330)
- Add preferences-system-bluetooth-battery to preferences.svgz

### Purpose

- Add plugin type for sharing URLs

### QQC2StyleBridge

- Fix menu item width when the delegate is overridden (bug 401792)
- Rotate busy indicator clockwise
- Force checkboxes/radios to be square

### Solid

- [UDisks2] Use MediaRemovable to determine if media can be ejected
- Support Bluetooth batteries

### Sonnet

- Add method to BackgroundChecker to add word to session
- DictionaryComboBox: Keep user preferred dictionaries on top (bug 302689)

### Syntax Highlighting

- Update php syntax support
- WML: fix embedded Lua code &amp; use new default styles
- Highlight CUDA .cu and .cuh files as C++
- TypeScript &amp; TS/JS React: improve types detection, fix float-points &amp; other improvements/fixes
- Haskell: Highlight empty comments after 'import'
- WML: fix infinite loop in contexts switch &amp; only highlight tags with valid names (bug 402720)
- BrightScript: Add workaround for QtCreator 'endsub' highlighting, add function/sub folding
- support more variants of C number literals (bug 402002)

### Sicherheitsinformation

Der veröffentlichte Quelltext wurde mit GPG mit folgendem Schlüssel signiert: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärer Fingerprint des Schlüssels: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
