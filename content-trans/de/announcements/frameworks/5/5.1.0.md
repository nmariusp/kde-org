---
aliases:
- ../../kde-frameworks-5.1
- ./5.1
customIntro: true
date: '2014-08-08'
description: KDE veröffentlicht die zweite Version von Frameworks 5.
layout: framework
qtversion: 5.2
title: Zweite Veröffentlichung der KDE Frameworks 5
---
7. August 2014. KDE kündigt heute die zweite Veröffentlichung von KDE Frameworks 5 an. Im Einklang mit den geplanten Veröffentlichungsregeln für KDE Frameworks kommt diese Veröffentlichung einen Monat nach der ersten Version und enthält sowohl Fehlerbehebungen als auch neue Funktionen.

{{% i18n "annc-frameworks-intro" "60" "/announcements/frameworks/5/5.0" %}}

## {{< i18n "annc-frameworks-new" >}}

This release, versioned 5.1, comes with a number of bugfixes and new features including:

- KTextEditor: Major refactorings and improvements of the vi-mode
- KAuth: Now based on PolkitQt5-1
- New migration agent for KWallet
- Windows compilation fixes
- Translation fixes
- New install dir for KXmlGui files and for AppStream metainfo
- <a href='http://www.proli.net/2014/08/04/taking-advantage-of-opengl-from-plasma/'>Plasma Taking advantage of OpenGL</a>
