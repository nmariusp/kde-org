---
aliases:
- ../announce-applications-16.12.3
changelog: true
date: 2017-03-09
description: KDE wydało Aplikacje KDE 16.12.3
layout: application
title: KDE wydało Aplikacje KDE 16.12.3
version: 16.12.3
---
9 marca 2017. Dzisiaj KDE wydało trzecie uaktualnienie stabilizujące <a href='../16.12.0'>Aplikacji KDE 16.12</a>. To wydanie zawiera tylko poprawki błędów i uaktualnienia do tłumaczeń; jest to bezpieczne i przyjemne uaktualnienie dla każdego.

Więcej niż 20 zarejestrowanych poprawek błędów uwzględnia ulepszenia do depim, ark, filelight, gwenview, kate, kdenlive, okular oraz innych.

To wydanie zawiera także długoterminowo wspieraną wersję Platformy Programistycznej KDE 4.14.30.
