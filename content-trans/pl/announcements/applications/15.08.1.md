---
aliases:
- ../announce-applications-15.08.1
changelog: true
date: 2015-09-15
description: KDE wydało Aplikacje KDE 15.08.1
layout: application
title: KDE wydało Aplikacje KDE 15.08.1
version: 15.08.1
---
15 wrzesień 2015. Dzisiaj KDE wydało pierwsze uaktualnienie stabilizujące <a href='../15.08.0'>Aplikacji KDE 15.08</a>. To wydanie zawiera tylko poprawki błędów i uaktualnienia do tłumaczeń; jest to bezpieczne i przyjemne uaktualnienie dla każdego.

Więcej niż 40 zarejestrowanych poprawek błędów uwzględnia ulepszenia do kdelibs, kdepim, kdenlive, dolphin, marble, kompare, konsole oraz umbrello.

To wydanie zawiera także długoterminowo wspieraną wersję Platformy Programistycznej KDE 4.14.12.
