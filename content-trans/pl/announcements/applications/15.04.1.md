---
aliases:
- ../announce-applications-15.04.1
changelog: true
date: '2015-05-12'
description: KDE wydało Aplikacje 15.04.1.
layout: application
title: KDE wydało Aplikacje KDE 15.04.1
version: 15.04.1
---
12 maja 2015. Dzisiaj KDE wydało pierwsze uaktualnienie stabilizujące <a href='../15.04.0'>Aplikacji KDE 15.04</a>. To wydanie zawiera tylko poprawki błędów i uaktualnienia do tłumaczeń; jest to bezpieczne i przyjemne uaktualnienie dla każdego.

Więcej niż 50 zarejestrowanych poprawek błędów uwzględnia ulepszenia do kdelibs, kdepim, kdenlive, okular, marble oraz umbrello.

To wydanie zawiera także wersje o długoterminowym wsparciu: Przestrzeni Roboczych 4.11.19, Platformę Programistyczną KDE 4.14.8 oraz Pakiet Kontact 4.14.8 .
