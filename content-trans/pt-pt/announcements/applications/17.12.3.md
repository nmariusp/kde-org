---
aliases:
- ../announce-applications-17.12.3
changelog: true
date: 2018-03-08
description: O KDE Lança as Aplicações do KDE 17.12.3
layout: application
title: O KDE Lança as Aplicações do KDE 17.12.3
version: 17.12.3
---
8 de Março de 2018. Hoje o KDE lançou a terceira actualização de estabilidade para as <a href='../17.12.0'>Aplicações do KDE 17.12</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

As mais de 25 correcções de erros registadas incluem as melhorias no Kontact, no Dolphin, no Gwenview, no JuK, no KGet, no Okular, no Umbrello, entre outros.

As melhorias incluem:

- O Akregator já não apaga mais a lista de fontes 'feeds.opml' a seguir a um erro
- O modo de ecrã completo do Gwenview agora funciona com o nome de ficheiro correcto após a mudança de nome
- Diversos estoiros raros do Okular foram identificados e corrigidos
