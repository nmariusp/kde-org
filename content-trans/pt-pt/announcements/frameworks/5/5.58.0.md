---
aliases:
- ../../kde-frameworks-5.58.0
date: 2019-05-13
layout: framework
libCount: 70
---
### Baloo

- [baloo_file] Espera pelo início do processo de extracção
- [balooctl] Adição de comando para mostrar os ficheiros que não conseguiram ser indexados (erro 406116)
- Adição de tipos de QML para o código-fonte
- [balooctl] Captura da constante 'totalsize' no lambda
- [balooctl] Mudança do resultado multi-linhas para um novo utilitário
- [balooctl] Uso de novo utilitário no resultado em JSON
- [balooctl] Usar um novo utilitário para resultados num formato simples
- [balooctl] Factorização da recolha do estado de indexação de ficheiros a partir do resultado
- Manter a documentação de meta-dados vazios em JSON na BD do DocumentData
- [balooshow] Possibilidade de referência dos ficheiros por URL numa ligação real
- [balooshow] Eliminação de aviso quando o URL se refere a um ficheiro não indexado
- [MTimeDB] Possibilidade de datas/horas mais recentes que o documento mais recente na correspondência de intervalos
- [MTimeDB] Uso da correspondência exacta quando a mesma for pedida
- [balooctl] Limpeza do tratamento dos diferentes argumentos de posição
- [balooctl] Extensão do texto de ajuda das opções, melhoria na verificação de erros
- [balooctl] Uso de nomes mais compreensíveis para o tamanho no resultado do estado
- [balooctl] comando de limpeza: Remoção de verificação inútil do documentData, limpeza
- [kio_search] Correcção de aviso e adição de UDSEntry para o "." no 'listDir'
- Uso da notação hexadecimal para o enumerado DocumentOperation
- Cálculo correcto do tamanho total da BD
- Adiamento do processamento de termos até serem necessários, não definir logo o termo e o texto da pesquisa
- Não adicionar filtros de datas com valores predefinidos no JSON
- Uso da formatação JSON compacta ao converter os URL's da pesquisa
- [balooshow] Não imprimir um aviso inútil para um ficheiro não indexado

### Ícones do Brisa

- Adição de versões em 16px não simbólicas para o 'find-location' e o 'mark-location'
- Ligação simbólica do 'preferences-system-windows-effect-flipswitch' para o 'preferences-system-tabbox'
- Adição de ligação simbólica do ícone "edit-delete-remove" e adição de versão em 22px do "paint-none" e "edit-none"
- Uso de um ícone de utilizador consistente no Kickoff
- Adição de ícone para o KCM de Thunderbolt
- Aumento de nitidez dos Z's nos ícones 'system-suspend*'
- Melhoria do ícone "widget-alternatives"
- Adição dos ícones 'go-up/down/next/previous-skip'
- Actualização do logótipo do KDE para ser mais próximo do original
- Adição de ícones de alternativas

### Módulos Extra do CMake

- Correcção de erro: pesquisa do STL de C*+ com uma expressão regular
- Activação incondicional do -DQT_STRICT_ITERATORS e não apenas no modo de depuração

### KArchive

- KTar: Protecção contra tamanhos negativos de ligações longas
- Correcção de gravação inválida na memória no caso de ficheiros TAR com problemas
- Correcção de fuga de memória ao ler alguns ficheiros TAR
- Correcção de memória não inicializada ao ler ficheiros TAR com problemas
- Correcção de esgotamento da memória com ficheiros com problemas
- Correcção de remoção de referência nula em ficheiros TAR com problemas
- Instalação do ficheiro de inclusão krcc.h
- Correcção de remoção dupla de ficheiros com problemas
- Proibição da cópia do KArchiveDirectoryPrivate e do KArchivePrivate
- Correcção do esgotamento da pilha no KArchive::findOrCreate com localizações MUITO GRANDES
- Introdução e uso do KArchiveDirectory::addEntryV2
- O 'removeEntry' pode falhar, por isso é uma boa ideia saber se isso aconteceu
- KZip: Correcção de uso-após-libertação em ficheiros com problemas

### KAuth

- Obrigação dos utilitários do KAuth em ter suporte para UTF-8 (erro 384294)

### KBookmarks

- Adição do suporte para o KBookmarkOwner para comunicar se tem páginas abertas

### KCMUtils

- Adição de sugestões de tamanhos do ApplicationItem propriamente dito
- Correcção do gradiente de fundo do Oxygen para os módulos em QML

### KConfig

- Adição da capacidade Notify ao KConfigXT

### KCoreAddons

- Correcção de avisos "Unable to find service type" (Não foi possível encontrar o tipo de serviço) inválidos
- Nova classe KOSRelease - um processador de ficheiros 'os-release'

### KDeclarative

- [KeySequenceItem] Fazer com que o botão de limpeza tenha a mesma altura que o botão do atalho
- Plotter: Âmbito do Programa em GL para o tempo de vida do nó 'scenegraph' (erro 403453)
- KeySequenceHelperPrivate::updateShortcutDisplay: Não mostrar texto em inglês ao utilizador
- [ConfigModule] Passage das propriedades iniciais no push()
- Activação do suporte para o glGetGraphicsResetStatus por omissão no Qt &gt;= 5.13 (erro 364766)

### KDED

- Instalação do ficheiro .desktop para o kded5 (erro 387556)

### KFileMetaData

- [TagLibExtractor] Correcção de estoiro nos ficheiros Speex inválidos (erro 403902)
- Correcção de estoiro do 'exivextractor' em ficheiros com problemas (erro 405210)
- Declaração de propriedades como meta-tipos
- Mudança dos atributos das propriedades por consistência
- Tratamento da lista de variantes nas funções de formatação
- Correcção do tipo LARGE_INTEGER do Windows
- Correcção de erros (de compilação) na implementação do UserMetaData no Windows
- Adição de tipo MIME em falta no 'taglibwriter'
- [UserMetaData] Tratamento das alterações no tamanho dos dados dos atributos de forma correcta
- [UserMetaData] Reorganização e limpeza de código do Windows, Linux/BSD/Mac e código acessório

### KGlobalAccel

- Cópia do contentor no Component::cleanUp antes da iteração
- Não usar o qAsConst sobre uma variável temporária (erro 406426)

### KHolidays

- holidays/plan2/holiday_zm_en-gb - adição dos feriados da Zâmbia
- holidays/plan2/holiday_lv_lv - correcção do Dia de Midsummer
- holiday_mu_en - Feriados de 2019 nas Maurícias
- holiday_th_en-gb - actualização para 2019 (erro 402277)
- Actualização dos feriados Japoneses
- Adição dos feriados públicos na Baixa Saxónia (Alemanha)

### KImageFormats

- tga: não tentar ler mais do que o 'max_palette_size' para dentro da paleta
- tga: 'memset' do 'dist' se a leitura for mal-sucedida
- tga: 'memset' de toda a lista da paleta, não só com o tamanho 'palette_size'
- Inicialização dos dados não lidos do '_starttab'
- xcf: Correcção de memória não inicializada com documentos com problemas
- ras: Não ler exageradamente os dados de ficheiros com problemas
- xcf: a 'layer' é constante no 'copy' e 'merge' - marcá-la como tal

### KIO

- [FileWidget] Substituição do "Filtro:" por "Tipo de ficheiro:" ao grava com uma lista limitada de tipos MIME (erro 79903)
- Os ficheiros recém-criados de 'Atalho para Aplicação' têm um ícone genérico
- [Janela de propriedades] Uso do texto "Espaço livre" em vez de "Utilização do disco" (erro 406630)
- Preenchimento do UDSEntry::UDS_CREATION_TIME no Linux quando a glibc &gt;= 2.28
- [KUrlNavigator] Correcção da navegação do URL quando sair do pacote com o 'krarc' e o Dolphin (erro 386448)
- [KDynamicJobTracker] Quando o 'kuiserver' não estiver disponível, também usar em alternativa a janela do elemento (erro 379887)

### Kirigami

- [aboutpage] esconder o cabeçalho de Autores se estes não existirem
- Actualização do qrc.in para corresponder ao .qrc (ActionMenuItem em falta)
- Certificação que o ActionButton não fica esticado (erro 406678)
- Pages: exportação dos tamanhos correctos do 'contentHeight/implicit'
- [ColumnView] Verificar o índice também no filtro de filhos..
- [ColumnView] Não deixar que o botão para recuar do rato vá para lá da primeira página
- o cabeçalho fica imediatamente com o tamanho correcto

### KJobWidgets

- [KUiServerJobTracker] Acompanhamento do tempo de vida do serviço 'kuiserver' e voltar a registar as tarefas se necessário

### KNewStuff

- Remoção de contorno pixelizado (erro 391108)

### KNotification

- [Notificação pelo Portal] Suporte das sugestões de prioridades e acções predefinidas
- [KNotification] Adição do HighUrgency
- [KNotifications] Actualizar quando as opções, os URL's ou a urgência mudam
- Permitir a definição da urgência das notificações

### Plataforma KPackage

- Adição das propriedades em falta no 'kpackage-generic.desktop'
- kpackagetool: leitura do 'kpackage-generic.desktop' a partir do 'qrc'
- Geração do AppStream: certificação de que se procura na estrutura dos pacotes que tenham também o 'metadata.desktop/json'

### KTextEditor

- Revisão das páginas de configuração do Kate para tornar mais amigável a sua manutenção
- Possibilidade de mudar o Modo depois de mudar o Realce
- ViewConfig: Uso da nova interface de configuração genérica
- Correcção da pintura da imagem do favorito na barra de ícones
- Garantia de que o contorno esquerdo não perde a alteração do número de algarismos do número de linha
- Correcção para mostrar a antevisão da dobragem quando mover o rato de baixo para cima
- Revisão do IconBorder
- Adição de métodos de entrada para o botão de método de entrada na barra de estado
- Pintar o marcador de dobragem de código com uma cor adequada e torná-lo mais visível
- remoção do atalho predefinido F6 para mostrar o contorno de ícones
- Adição de acção para comutar a dobragem dos intervalos-filhos (erro 344414)
- Mudança do título do botão "Fechar" para "Fechar o ficheiro" quando um dado ficheiro tiver sido removido no disco (erro 406305)
- subida do 'copy-right' - talvez devesse também ser uma definição
- evitar atalhos em conflito para a mudança de páginas
- KateIconBorder: Correcção da largura e altura da área de dobragem
- evitar um salto na janela para o fundo quando mudar a dobragem
- DocumentPrivate: Respeito do modo de indentação com a selecção em bloco (erro 395430)
- ViewInternal: Correcção do makeVisible(..) (erro 306745)
- DocumentPrivate: Tornar mais inteligente o tratamento de parêntesis (erro 368580)
- ViewInternal: Revisão do evento 'drop'
- Possibilidade de fechar um documento cujo ficheiro foi apagado no disco
- KateIconBorder: Uso de carácter UTF-8 em vez de uma imagem especial como indicador de mudança de linha dinâmico
- KateIconBorder: Garantia de que os Marcadores de Mudança de Linha Dinâmica ficam visíveis
- KateIconBorder: Cosmética do código
- DocumentPrivate: Suporte para parêntesis automáticos na selecção em bloco (erro 382213)

### KUnitConversion

- Correcção da conversão de l/100 km para MPG (erro 378967)

### Plataforma da KWallet

- Configuração do 'kwalletd_bin_path' correcto
- Exportação da localização do binário 'kwalletd' para o 'kwallet_pam'

### KWayland

- Adição do tipo de janela CriticalNotification para o protocolo PlasmaShellSurface
- Implementação da Interface de Servidor do wl_eglstream_controller

### KWidgetsAddons

- Actualização do kcharselect-data para o Unicode 12.1
- Modelo interno do KCharSelect: garantia de que o rowCount() é 0 para índices válidos

### KWindowSystem

- Introdução do CriticalNotificationType
- Suporte para o NET_WM_STATE_FOCUSED
- Documentação de que o 'modToStringUser' e o 'stringUserToMod' só lidam com textos em inglês

### KXMLGUI

- KKeySequenceWidget: Não mostrar textos em Inglês para o utilizador

### NetworkManagerQt

- WireGuard: Não forçar a 'private-key' a ser não-nula para o 'private-key-flags'
- WireGuard: truque sobre o tipo de opção errado da senha
- WireGuard: os valores de 'private-key' e 'preshared-keys' podem ser pedidos em conjunto

### Plataforma do Plasma

- PlatformComponentsPlugin: correcção do IID do 'plugin' para o QQmlExtensionInterface
- IconItem: remoção do dados da propriedade em falta &amp; não usada 'smooth'
- [Dialog] Adição do tipo CriticalNotification
- Correcções dos nomes de grupos errados para  22, 32 px no audio.svg
- fazer com que a barra de ferramentas do texto móvel só apareça ao carregar
- uso do novo Kirigami.WheelHandler
- Adição de mais tamanhos de ícones para o 'audio', 'configure', 'distribute'
- [FrameSvgItem] Actualização da filtragem em caso de mudança do 'smooth'
- Tema Air/Oxygen: correcção da altura da barra de progresso com o "hint-bar-size"
- Correcção do suporte para folhas de estilos no 'audio-volume-medium'
- Actualização dos ícones 'audio', 'drive', 'edit', 'go', 'list', 'media', 'plasmavault' para corresponder aos ícones do Brisa
- Alinhamento dos Z's à grelha de pixels no 'system.svg'
- uso do 'mobiletextcursor' do espaço de nomes correcto
- [FrameSvgItem] Leitura da propriedade 'smooth'
- Tema do Oxygen: adição de preenchimento aos ponteiros, face ao contorno tremido na rotação
- SvgItem, IconItem: eliminação da substituição da propriedade "smooth", actualização do nó em caso de mudança
- Suporte do 'gzip' no 'svgz' também no Windows, usando o 7z
- Tema Air/Oxygen: correcção das posições das mãos com o 'hint-*-rotation-center-offset'
- Adição da API pública para invocação e emissão do evento 'contextualActionsAboutToShow'
- Relógio do tema Brisa: suporte da sugestão do deslocamento da sombra dos ponteiros do Plasma 5.16
- Manter os ficheiros SVG do tema do ecrã descomprimidos no repositório e instalação do svgz
- separação da selecção do texto móvel para evitar importações recursivas
- Uso do ícone e texto mais apropriados para "Alternativas"
- FrameSvgItem: Adição da propriedade "mask"

### Prisão

- Aztec: Correcção do preenchimento se a última palavra parcial for um conjunto de bits todos a 1

### QQC2StyleBridge

- Impedimento de Controls encadeados no TextField (erro 406851)
- fazer com que a barra de ferramentas do texto móvel só apareça ao carregar
- [TabBar] Actualização da altura quando os TabButtons forem adicionados de forma dinâmica
- uso do novo Kirigami.WheelHandler
- Suporte de tamanhos de ícones personalizados para o ToolButton
- Compila sem problemas sem o 'foreach'

### Solid

- [Fstab] Adição do suporte para sistemas de ficheiros sem rede
- [FsTab] Adição de 'cache' para o tipo de sistema de ficheiros dos dispositivos
- [Fstab] Trabalho preparatório para activar sistemas de ficheiros para além do NFS/SMB
- Correcção do erro 'nenhum membro chamado 'setTime_t' no 'QDateTime'' na compilação (erro 405554)

### Realce de Sintaxe

- Adição do realce de sintaxe para a consola Fish
- AppArmor: não realçar as atribuições de variáveis e regras alternativas dentro de perfis

### Informação de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
