---
aliases:
- ../../kde-frameworks-5.21.0
date: 2016-04-09
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
Nova plataforma: KActivitiesStats, uma biblioteca para aceder aos dados de estatísticas de utilização recolhidos pelo gestor de actividades do KDE.

### Todas as plataformas

O Qt &gt;= 5.4 é agora obrigatório, i.e., o Qt 5.3 já não é mais suportado.

### Attica

- Adição da variante 'const' ao método de 'get'

### Baloo

- Centralização do tamanho do lote na configuração
- Remoção da indexação bloqueante do código dos ficheiros 'text/plain' sem extensão .txt (erro 358098)
- Verificação tanto do nome como do conteúdo do ficheiro para determinar o tipo MIME (erro 353512)

### BluezQt

- ObexManager: Divisão das mensagens de erro para os objectos em falta

### Ícones do Brisa

- adição de ícones Brisa para o Lokalize
- sincronização dos ícones das aplicações entre o Brisa e o Brisa Escuro
- actualizações dos ícones dos temas e remoção dos grupos do Kicker para correcção dos ícones das aplicações no sistema
- adição do suporte do '.xpi' para as extensões do Firefox (erro 359913)
- actualização do ícone do Okular com o correcto
- adição do suporte de ícones das aplicações 'ktnef'
- adição de ícones para o KMenueditor, KMouse e KNotes
- mudança do ícone de volume silencioso para usar o '-' para silencioso em vez de apenas estar a vermelho (erro 360953)
- adição do suporte para os tipos MIME do 'djvu' (erro 360136)
- adição de ligação em vez de um item duplo
- adição de um ícone de 'ms-shortcut' para o Gnucash (erro 360776)
- mudança do fundo do papel de parede para um genérico
- actualização dos ícones para usar um papel de parede genérico
- adição do ícone para o Konqueror (erro 360304)
- adição de um ícone de processo-em-curso para a animação do progresso no KDE (erro 360304)
- adição de ícone de instalação de aplicações e actualização do ícone de actualizações com a cor correcta
- adição de ícones para adição e remoção de emblemas para a selecção, adição e montagem no Dolphin
- Remoção da folha de estilo dos ícones das 'applets' do relógio analógico e do 'kickerdash'
- sincronização do Brisa e Brisa Escuro (erro 360294)

### Módulos Extra do CMake

- Correcção do _ecm_update_iconcache para apenas actualizar o local de instalação
- Anulação do "ECMQtDeclareLoggingCategory: Inclusão do &lt;QDebug&gt; com o ficheiro gerado"

### Integração da Plataforma

- Contingência para a implementação do QCommonStyle do 'standardIcon'
- Definição de um tempo-limite predefinido de fecho do menu

### KActivities

- Remoção das validações do compilador, agora que todas as plataformas necessitam do C++11
- Remoção do ResourceModel do QML, dado ser substituída pelo KAStats::ResultModel
- A inserção num QFlatSet vazio devolveu um iterador inválido

### KCodecs

- Simplificação do código ('qCount -&gt; std::count', melhorias com 'isprint -&gt; QChar::isPrint')
- detecção da codificação: correcção de estoiros com má utilização do 'isprint' (erro 357341)
- Correcção de estoiros por variável não inicializada (erro 357341)

### KCompletion

- KCompletionBox: forçar uma janela sem moldura e não atribuir o foco
- O KCompletionBox *não* deverá ser uma dica

### KConfig

- Adição do suporte para a obtenção de localizações do QStandardPaths dentro dos ficheiros .desktop

### KCoreAddons

- Correcção do 'kcoreaddons_desktop_to_json()' no Windows
- src/lib/CMakeLists.txt - correcção da compilação com uma biblioteca do Threads
- Adição de rotinas substitutas para permitir a compilação no Android

### KDBusAddons

- Evitar a introspecção de uma interface de DBus quando não é usada

### KDeclarative

- uniformização do uso do 'std::numeric_limits'
- [DeclarativeDragArea] Não substituir o "texto" dos dados MIME

### Suporte para a KDELibs 4

- Correcção de ligação obsoleta no Docbook do 'kdebugdialog5'
- Não deixar o Qt5::Network como biblioteca obrigatória no resto das ConfigureChecks

### KDESU

- Definição de macros de funcionalidades para permitir a compilação na 'libc' do 'musl'

### KEmoticons

- KEmoticons: correcção de estoiro quando o 'loadProvider' falha por alguma razão

### KGlobalAccel

- Possibilidade de matar de forma ordeira o 'kglobalaccel5', corrigindo um encerramento extremamente lento

### KI18n

- O uso das línguas da localização regional do sistema Qt como contingência em sistemas não-UNIX

### KInit

- Limpeza e remodelação da versão para 'xcb' do 'klauncher'

### KIO

- FavIconsCache: sincronização após a escrita, para que as outras aplicações a possam ver, e para evitar estoiros em caso de destruição
- Correcção de muitas questões relacionadas com múltiplas tarefas no KUrlCompletion
- Correcção de estoiro na janela de mudança de nome (erro 360488)
- KOpenWithDialog: melhoria do texto do título e da descrição da janela (erro 359233)
- Possibilidade de melhor instalação multi-plataforma dos 'ioslaves', através do envio da informação do protocolo nos meta-dados do 'plugin'

### KItemModels

- KSelectionProxyModel: Simplificação do tratamento da remoção de linhas e da lógica de remoção da selecção
- KSelectionProxyModel: Recriação do mapeamento na remoção somente se necessário (erro 352369)
- KSelectionProxyModel: Só limpar as associações 'firstChild' no nível de topo
- KSelectionProxyModel: Garantir o envio de sinais adequados ao remover a última selecção
- Possibilidade de pesquisar no DynamicTreeModel por papel de visualização

### KNewStuff

- Não estoirar se os ficheiros .desktop não existirem ou estiverem danificados

### KNotification

- Tratamento do botão esquerdo do rato nos ícones antigos da bandeja do sistema (erro 358589)
- Uso do X11BypassWindowManagerHint apenas na plataforma X11

### Plataforma de Pacotes

- Depois de instalar um pacote, carregá-lo
- se o pacote existir e estiver actualizado, não falhar
- Adição do Package::cryptographicHash(QCryptographicHash::Algorithm)

### KPeople

- Definição do URI do contacto como URI da pessoa no PersonData, quando não existir nenhuma pessoa
- Definição de um nome para a ligação à base de dados

### KRunner

- Importação do modelo de módulos de execução no KAppTemplate

### KService

- Correcção do novo aviso do 'kbuildsycoca', quando um tipo MIME herda de um nome alternativo
- Correcção do tratamento do x-scheme-handler/* no 'mimeapps.list'
- Correcção do tratamento do 'x-scheme-handler/*' no processamento do 'mimeapps.list' (erro 358159)

### KTextEditor

- Anulação do "Página de configuração do Abrir/Gravar: Usar o termo "Pasta" em vez de "Directoria""
- uso obrigatório do UTF-8
- Página de configuração do Abrir/Gravar: Uso do termo "Pasta" em vez de "Directoria"
- kateschemaconfig.cpp: uso dos filtros correctos nas janelas de abertura/gravação (erro 343327)
- c.xml: uso de estilo predefinido para as palavras-chave de controlo de fluxo
- isocpp.xml: uso de estilo predefinido "dsControlFlow" para as palavras-chave de controlo de fluxo
- c/isocpp: adição de mais tipos-padrão em C
- O KateRenderer::lineHeight() devolve um inteiro
- impressão: uso o tamanho do texto do esquema de impressão seleccionado (erro 356110)
- aceleração do cmake.xml: Uso do WordDetect em vez do RegExpr
- Mudança do tamanho das tabulações de 8 para 4
- Correcção da mudança da cor do número da linha actual
- Correcção da selecção do item de completação com o rato (erro 307052)
- Adição do realce de sintaxe para o 'gcode'
- Correcção da pintura do fundo da selecção do MiniMap
- Correcção da codificação do 'gap.xml' (uso do UTF-8)
- Correcção dos blocos de comentários encadeados (erro 358692)

### KWidgetsAddons

- Ter as margens do conteúdo em conta ao calcular as sugestões de tamanho

### KXMLGUI

- Correcção da edição das barras de ferramentas que perdiam as acções associadas

### NetworkManagerQt

- ConnectionSettings: Inicialização do tempo-limite do contacto da 'gateway'
- Novo TunSetting e novo tipo de ligação Tun
- Criação de dispositivos para todos os tipos conhecidos

### Ícones do Oxygen

- Instalação do 'index.theme' na mesma pasta onde sempre esteve
- Instalação no 'oxygen/base/', para que a mudança de local dos ícones das aplicações não entre em conflito com a versão instalada por essas aplicações
- Replicação das ligações simbólicas dos ícones do Brisa
- Adição de novos ícones para adicionar e remover emblemas na sincronização com o Brisa

### Plataforma do Plasma

- [calendário] Correcção da 'applet' do calendário que não limpava a selecção quando se escondia (erro 360683)
- actualização do ícone do áudio para usar as folhas de estilo
- actualização do ícone de silenciamento do áudio (erro 360953)
- Correcção da criação forçada das 'applets' quando o Plasma está inalterável
- [Nó a Desvanescer] Não misturar a opacidade em separado (erro 355894)
- [Svg] Não voltar a processar a configuração em resposta ao Theme::applicationPaletteChanged
- Dialog: Mudança dos estados do SkipTaskbar/Pager antes de mostrar a janela (erro 332024)
- Reintrodução da propriedade 'busy' (ocupado) na Applet
- Certificação de que o ficheiro de exportação do PlasmaQuick é encontrado de forma adequada
- Não importar uma disposição inexistente
- Possibilidade de uma 'applet' oferece um objecto de testes
- Substituição do QMenu::exec por QMenu::popup
- FrameSvg: Correcção de ponteiros órfãos no 'sharedFrames' com as mudanças de tema
- IconItem: Agendar uma actualização da imagem quando a janela muda
- IconItem: Animação das mudanças para activo mesmo com as animações desligadas
- DaysModel: Transformação do 'update' num 'slot'
- [Item de Ícone] Não animar a partir da imagem anterior quando a mesma estava invisível
- [Item de Ícone] Não invocar o 'loadPixmap' no 'setColorGroup'
- [Applet] Não substituir a opção "Persistente" na notificação 'Undo'
- Possibilidade de substituição da definição de mutabilidade do Plasma na criação de um contentor
- Adição do 'icon/titleChanged'
- Remoção de dependência do QtScript
- O cabeçalho do plasmaquick_export.h está na pasta 'plasmaquick'
- Instalação de alguns ficheiros de inclusão do 'plasmaquick'

Poderá discutir e partilhar ideias sobre esta versão na secção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-'>artigo do Dot</a>.
