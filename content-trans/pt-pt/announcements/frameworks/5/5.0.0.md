---
aliases:
- ../../kde-frameworks-5.0
- ./5.0
customIntro: true
date: '2014-07-07'
description: O KDE Lança a Primeira Versão das Plataformas 5.
layout: framework
qtversion: 5.2
title: Primeira Versão das Plataformas 5
---
7 de Julho de 2014.  A Comunidade do KDE orgulha-se em anunciar as Plataformas do KDE 5.0. As Plataformas 5 são a próxima geração das bibliotecas do KDE, modulares e optimizadas para uma integração simples nas aplicações do Qt. As Plataformas oferecem uma grande variedade de funcionalidades comuns em bibliotecas maduras, revistas e bem testadas com licenças amigáveis. Existem cerca de 50 plataformas diferentes nesta versão, oferecendo soluções que incluem a integração com o 'hardware', o suporte para determinados formatos de ficheiros, itens gráficos adicionais, funções de gráficos, verificação ortográfica, entre outras. Muitas das plataformas são multi-plataforma e têm poucas ou nenhumas dependências extra, tornando-as simples de compilar e adicionar a qualquer aplicação em Qt.

As Plataformas do KDE representam um esforço para remodelar as poderosas bibliotecas da Plataforma do KDE 4 num conjunto de módulos independentes e multi-plataforma que poderão ficar disponíveis desde logo para todos os programadores de Qt, por forma a acelerar e reduzir o custo do desenvolvimento em Qt. As plataformas individuais são multi-plataforma, bem documentadas e testadas e a sua utilização será familiar para os programadores em Qt, seguindo o estilo e as normas impostas pelo Projecto Qt. As plataformas são desenvolvidas segundo o modelo de governação do KDE, com um calendário de versões previsível, um processo de contribuições claro e independente de fornecedores e com uma licença flexível (LGPL).

As Plataformas têm uma estrutura de dependências clara, dividida em Categorias e Níveis. As Categorias dizem respeito às dependências durante a execução:

- Os elementos <strong>funcionais</strong> não têm dependências durante a execução.
- A <strong>Integração</strong> define o código que poderá necessitar de dependências durante a execução para a integração, dependendo do que o SO ou a plataforma oferece.
- As <strong>Soluções</strong> têm dependências durante a execução obrigatórias.

Os <strong>Níveis</strong> referem-se às dependências durante a compilação de outras Plataformas. As Plataformas de Nível 1 não têm dependências dentro das Plataformas e só necessitam do Qt ou de outras bibliotecas relevantes. As Plataformas de Nível 2 só podem depender do Nível 1. As Plataformas de Nível 3 podem depender de outras Plataformas de Nível 3, assim como de Nível 2 e Nível 1.

A transição da Plataforma paras as Plataformas está em curso há cerca de 3 anos, guiada por contribuintes técnicos de topo do KDE. Aprenda mais sobre as Plataformas 5 <a href='%1'>neste artigo do ano passado</a>.

## Destaques

Existem mais de 50 Plataformas disponíveis de momento. Consulte a lista completa na <a href='%1'>documentação 'online' da API</a>. Em baixo fica uma impressão de alguma da funcionalidade que as Plataformas oferecem aos programadores de aplicações em Qt.

O <strong>KArchive</strong> oferece o suporte para muitas das codificações de compressão conhecidas numa biblioteca de arquivo e extracção de ficheiros poderosa, simples de usar e independente. Basta passar-lhe ficheiros; não há necessidade de reinventar uma função de arquivo para a sua aplicação baseada no Qt!

O <strong>ThreadWeaver</strong> oferece uma API de alto-nível para gerir tarefas de processamento com base em interfaces baseadas em tarefas e filas de execução. Permite o escalonamento simples da execução de tarefas, indicando as dependências entre as tarefas e executando-as à medida que essas dependências vão sendo satisfeitas, simplificando em grande medida o uso de várias tarefas de processamento.

O <strong>KConfig</strong> é uma plataforma para lidar com o armazenamento e consulta de definições de configuração. Oferece uma API orientada por grupos. Funciona com os ficheiros INI e com as pastas encadeadas compatíveis com o XDG. Ele gera código com base em ficheiros XML.

O <strong>Solid</strong> oferece a detecção de 'hardware' e poderá informar uma aplicação sobre os dispositivos e volumes de armazenamento, o processador, o estado da bateria, a gestão de energia, o estado e as interfaces da rede e o Bluetooth. Para as partições encriptadas, a gestão de energia e a rede, são necessários alguns serviços em execução.

O <strong>KI18n</strong> adiciona o suporte do Gettext às aplicações, tornando-se mais fácil integrar o fluxo de trabalho de traduções do Qt na infra-estrutura geral de traduções dos diversos projectos.
