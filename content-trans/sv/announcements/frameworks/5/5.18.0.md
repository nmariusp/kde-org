---
aliases:
- ../../kde-frameworks-5.18.0
date: 2016-01-09
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Rätta fler problem med sökningar relaterade till ändringstid
- PostingDB Iter: Gör inte assert för MDB_NOTFOUND
- Balooctl status: Undvik visa 'Indexering av innehåll' för kataloger
- StatusCommand: Visa riktig status för kataloger
- SearchStore: Hantera tomma begreppsvärden snyggt (fel 356176)

### Breeze-ikoner

- ikonuppdateringar och tillägg
- 22 bildpunkter stora statusikoner också för 32 bildpunkter eftersom de behövs i systembrickan
- Ändrade fast till skalbart värde för 32 bildpunkters kataloger i Breeze mörk

### Extra CMake-moduler

- Gör CMake-modulen i KAppTemplate global
- Ta bort CMP0063-varningar med KDECompilerSettings
- ECMQtDeclareLoggingCategory: Inkludera &lt;QDebug&gt; med den genererade filen
- Rätta CMP0054-varningar

### KActivities

- Strömlinjeformade QML-inläsning för KCM (fel 356832)
- Provisorisk lösning för Qt SQL-fel som inte rensar bort anslutningar på ett riktigt sätt (fel 348194)
- Infogade ett insticksprogram som kör program vid ändring av aktivitetstillstånd
- Konverterade från KService till KPluginLoader
- Konvertera insticksprogram att använda kcoreaddons_desktop_to_json()

### KBookmarks

- Initiera DynMenuInfo helt och hållet i returvärden

### KCMUtils

- KPluginSelector::addPlugins: Rätta assert om parametern 'config' har sitt förvalda värde (fel 352471)

### KCodecs

- Undvik att överfylla en full buffert med avsikt

### KConfig

- Säkerställ att gruppundantag tas bort på ett riktigt sätt i kconf_update

### KCoreAddons

- Lägg till KAboutData::fromPluginMetaData(const KPluginMetaData &amp;plugin)
- Lägg till KPluginMetaData::copyrightText(), extraInformation() och otherContributors()
- Lägg till KPluginMetaData::translators() och KAboutPerson::fromJson()
- Rätta användning efter free i skrivbordsfiltolken
- Gör det möjligt att konstruera KPluginMetaData från en JSON-sökväg
- desktoptojson: Gör saknad tjänsttypsfil till ett fel för binärprogrammet
- Gör anrop till kcoreaddons_add_plugin utan SOURCES till ett fel

### KDBusAddons

- Anpassa till Qt 5.6:s dbus-in-secondary-thread

### KDeclarative

- [DragArea] Lägg till egenskapen dragActive
- [KQuickControlsAddons MimeDatabase] Exponera QMimeType-kommentar

### KDED

- kded: Anpassa till att Qt 5.6:s trådade dbus: messageFilter måste utlösa laddning av modulen i huvudtråden

### Stöd för KDELibs 4

- kdelibs4support kräver kded (för kdedmodule.desktop)
- Rätta CMP0064-varning genom att ställa in policy CMP0054 till NEW
- Exportera inte symboler som också finns i KWidgetsAddons

### KDESU

- Läck inte fildeskriptor när uttag skapas

### KHTML

- Windows: ta bort beroende på kdewin

### KI18n

- Dokumentera den första argumentregeln för plural i QML
- Reducera oönskade typändringar
- Gör det möjligt att använda double som index för anrop till i18np*() i QML

### KIO

- Rätta att kiod för Qt 5.6:s trådade dbus: messageFilter måste vänta till modulen är laddad innan det returnerar.
- Ändra felkoden vid inklistring eller flyttning in i en underkatalog
- Rätta problem med att emptyTrash blockeras
- Rätta fel knapp i KUrlNavigator för fjärrwebbadresser
- KUrlComboBox: Rätta att en absolut sökväg returneras från urls()
- kiod: Inaktivera sessionshantering
- Lägg till automatisk komplettering för '.' indata, som erbjuder alla dolda filer* och kataloger* (fel 354981)
- ktelnetservice: Rätta plus ett fel i kontroll av argc, programfix av Steven Bromley

### KNotification

- [Notify By Popup] Skicka med händelse-id
- Ställ in icke-tom standardorsak för inhibering av skärmsläckare (fel 334525)
- Lägg till ett tips för att hoppa över gruppering av underrättelser (fel 356653)

### KNotifyConfig

- [KNotifyConfigWidget] Tillåt val av en specifik händelse

### Paketet Framework

- Gör det möjligt att tillhandahålla metadata i JSON

### KPeople

- Rätta möjlig dubbel borttagning i DeclarativePersonData

### KTextEditor

- Syntaxfärgläggning för pli: Inbyggda funktioner tillagda, expanderbara områden tillagda

### Ramverket KWallet

- kwalletd: Rätta läcka av FILE*

### KWindowSystem

- Lägg till xcb-variant för statiska KStartupInfo::send metoder

### NetworkManagerQt

- Få det att fungera med äldre versioner av NM

### Plasma ramverk

- [ToolButtonStyle] Indikera alltid aktivt fokus
- Använd flaggan SkipGrouping för underrättelsen om "grafisk komponent borttagen" (fel 356653)
- Hantera symboliska länkar i sökvägar till paket på ett riktigt sätt
- Lägg till HiddenStatus för plasmoid som döljer sig själv
- Sluta dirigera om fönster när objekt är inaktiverat eller dolt (fel 356938).
- Skicka inte statusChanged om det inte har ändrats
- Rätta element-identifierare för östlig orientering
- Containment: Skicka inte appletCreated med null miniprogram (fel 356428)
- [Containment Interface] Rätta oberäknelig högprecisionsrullning
- Läs egenskapen X-Plasma-ComponentTypes i KPluginMetada som en stränglista
- [Window Thumbnails] Krascha inte om sammansättning är inaktiverad
- Låt omgivningar överskrida CompactApplet.qml

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
