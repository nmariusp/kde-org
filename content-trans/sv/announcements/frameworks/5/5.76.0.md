---
aliases:
- ../../kde-frameworks-5.76.0
date: 2020-11-07
layout: framework
libCount: 70
qtversion: 5.12
---
### Baloo

* Dela upp CJK-termer vid skiljetecken, optimera kod
* Omstrukturera koden som förberedelse för integrering av ICU

### Breeze-ikoner

* Lägg 48 bildpunkters ikonen dialog-warning
* Ändra stil på media-repeat-single för att använda siffran 1
* Lägg till flera filer ignorerade av git
* kontrollera om filen finns innan borttagning
* Ta alltid bort målfilen först när symboliska länkar genereras
* Lägg till några färglägesikoner för Okular
* Lägg till ikoner task-complete (fel 397996)
* Lägg till ikonen network-limited
* Kopiera 32 bildpunkters kup symbolisk länk till apps/48 för att rätta fel i skalbar test
* Lägg till ikonen meeting-organizer (fel 397996)
* Lägg till fingeravtrycksikon
* Lägg till ikonerna task-recurring och appointment-recurring (fel 392533)
* Tillfälligt inaktivera generering av ikoner på Windows
* Skapa symbolisk länk från kup.svg till preferences-system-backup.svg

### Extra CMake-moduler

* Gör så att androiddeployqt hittar bibliotek och QML-insticksprogram utan installation
* find-modules/FindReuseTool.cmake - Rätta hitta återanvändningsverktyg
* förbättra förvalda formateringsalternativ
* inkludera alternativ för att använda LLVM för användare med Qt < 5.14
* lägg till saknad minsta version för parametern RENAME
* Dokumentera när FindGradle har lagts till
* Lägg till FindGradle från KNotification

### KAuth

* Konvertera gränssnittsnamnet till stora bokstäver tidigare
* Lägg till hjälpfunktion för att erhålla uid för anropande

### KCalendarCore

* Höj tvetydighet i ICalFormat::toString() i tester
* Lägg till serialisering av egenskapen COLOR från RFC7986
* Gör så att MemoryCalendar::rawEvents(QDate, QDate) fungerar för öppna gränser

### KCMUtils

* Konvertera från QStandardPaths::DataLocation till QStandardPaths::AppDataLocation
* Lägg till stöd för namnrymd i KCModuleData CMake-makro
* Avråd från KSettings::PluginPage
* ta bort referens till odefinierad deklarationsfil
* Flytta KCMUtilsGenerateModuleData till korrekt plats
* Lägg till alla förskapade delsidor av en inställningsmodul
* Lägg till CMake-funktion för att generera grundläggande moduldata
* Förbättra läsbarhet för QML på plats i kcmoduleqml.cpp
* riktig huvudhöjd med en platsmarkör
* [kcmoduleqml] Rätta övre marginal för QML inställningsmoduler

### KConfig

* Hitta saknat beroende av Qt5DBus
* kconf_update: Tillåt upprepade tester i --testmode genom att ignorera kconf_updaterc

### KConfigWidgets

* Ändra http: till https:

### KContacts

* Rätta fel 428276 - KContacts kan inte användas i qmake-projekt (fel 428276)

### KCoreAddons

* KJob: lägg till setProgressUnit(), för att välja hur procentvärden beräknas
* Rätta potentiell minnesläcka i KAboutData::registerPluginData
* Dela suggestName(); delningsmetoden kontrollerar inte om filen finns
* KAboutData: avråd från pluginData() och registerPluginData()
* Avsluta inte händelsesnurran i KJobTest::slotResult()
* Använd functor-baserad singleShot() överlagring i TestJob::start()

### KDeclarative

* [abstractkcm] Ange explicit vaddering
* [simplekcm] Ta bort egen vadderingshantering
* [kcmcontrols] Ta bort duplicerad kod
* Lägg till källa i KDeclarativeMouseEvent
* Ändra överliggande objekt för overlaysheets till rot
* Gör så att GridViewKCM och ScrollVieKCM ärver från AbstractKCM
* Lägg till hämtningsmetod för delsidor

### KDocTools

* Rätta XML-formatering i contributor.entities
* Koreansk uppdatering: formatera HTML-filer för GPL, FDL och lägg till LGPL

### KFileMetaData

* [ExtractionResult] Återställ binärkompatibilitet
* [TaglibWriter|Extractor] Ta bort obehandlad speex Mime-typ
* [TaglibWriter] Öppna också läs-skriv på Windows
* [Extractor|WriterCollection] Filtrera bort andra än biblioteksfiler
* [EmbeddedImageData] Försök med provisorisk lösning för dumhet i MSVC
* [ExtractionResult] Avråd från ExtractEverything, rättning sedan
* [EmbeddedImageData] Läs bara sann testbild en gång
* [EmbeddedImageData] Ta bort privat skrivimplementering av omslag
* [EmbeddedImageData] Flytta skrivimplementering till taglib skrivinsticksprogram
* [EmbeddedImageData] Ta bort privat implementering av omslagsextrahering
* [EmbeddedImageData] Flytta implementering av insticksprogram för taglib-extrahering

### KGlobalAccel

* systemd dbus aktivering

### KIconThemes

* Behåll proportion vid uppskalning

### KIdleTime

* Avråd från signal KIdleTime::timeoutReached(int identifier) med ett enda argument

### KImageFormats

* Lägg still stöd för RLE-komprimerad, 16-bitar per kanal PSD-filer
* Retur stöds inte vid läsning av 16-bitars RLE-komprimerade PSD-filer
* feat: lägg till psd färgdjup == 16 formatstöd

### KIO

* villkorlig jämförelse med tom QUrl istället för / på Windows för mkpathjob
* KDirModel: två rättningar av QAbstractItemModelTester
* CopyJob: Inkludera överhoppade filer i förloppsberäkningar vid namnbyte
* CopyJob: räkna inte överhoppade filer i underrättelsen (fel 417034)
* I fildialogrutor, markera en befintlig katalog vid försök att skapa den
* CopyJob: rätta totalt antal filer/kataloger i förloppsdialogruta (vid förflyttning)
* Gör så att FileJob::write() beter sig konsekvent
* Stöd för xattrs för kio kopiera/flytta
* CopyJob: räkna inte in katalogstorlek i den totala storleken
* KNewFileMenu: Rätta krasch genom att använda m_text istället för m_lineEdit->text()
* FileWidget: Visa förhandsgranskning av markerad fil när muspekaren lämnar den (fel 418655)
* exponera sammanhangsberoende fält för användarhjälp i kpasswdserver
* KNewFileMenu: använd NameFinderJob för att få ett nytt namn för "Ny katalog"
* Introducera NameFinderJob som föreslår nya namn för "Ny katalog"
* Definiera inte explicit Exec-rader för inställningsmoduler (fel 398803)
* KNewFileMenu: Dela koden för att skapa dialogruta till en separat metod
* KNewFileMenu: kontrollera att fil inte redan finns med fördröjning för förbättrad användbarhet
* [PreviewJob] Tilldela tillräckligt med minne för SHM segment (fel 427865)
* Använd versionsmekanism för att lägga till nya platser för befintliga användare
* Lägg till bokmärken för bilder, musik och videor (fel 427876)
* kfilewidget: behåll texten i namnrutan vid navigering (fel 418711)
* Hantera inställningsmoduler i OpenUrlJob med KService programmeringsgränssnitt
* Kanonisera filsökväg när miniatyrbilder hämtas och skapas
* KFilePlacesItem: dölj monteringar av kdeconnect sshfs
* OpenFileManagerWindowJob: välj fönster från huvudjobb korrekt
* Undvik meningslös sökning efter för icke existerande miniatyrbilder
* [FEL] Rätta regression när filer som innehåller `#` markeras
* KFileWidget: gör att zoomknapparna för ikoner gå till närmaste standardstorlek
* placera verkligen minimumkeepsize i inställningsmodulen för nätverk (fel 419987)
* KDirOperator: förenkla ikoners zoomreglagelogik
* UDSEntry: dokumentera förväntad tidsformat för tidsnycklar
* kurlnavigatortest: ta bort desktop:, kräver desktop.protocol för att fungera
* KFilePlacesViewTest: visa inte ett fönster, behövs inte
* OpenFileManagerWindowJob: Rätta krasch vid återgång till KRun-strategi (fel 426282)
* Internet-nyckelord: rätta krasch och misslyckades tester om avgränsare är mellanslag
* Föredra DuckDuckGo tecken istället för andra avgränsare
* KFilePlacesModel: ignorera dolda platser vid beräkning av närmaste objekt (fel 426690)
* SlaveBase: dokumentera beteende hos ERR_FILE_ALREADY_EXIST med copy()
* kio_trash: rätta logiken när ingen storleksgräns är inställd (fel 426704)
* I fildialogrutor, ska skapa en katalog som redan finns markera den
* KFileItemActions: Lägg till egenskap för min/max antal webbadresser

### Kirigami

* [avatar]: Gör tal ogiltiga namn
* [avatar]: Exponera bildens cache-egenskap
* Ställ också in maximumWidth för ikoner i global låda (fel 428658)
* Gör avslutningsgenvägen en åtgärd och exponera den som en skrivskyddad egenskap
* Använd handmarkörer för ListItemDragHandle (fel 421544)
* [controls/avatar]: Stöd CJK-namn för initialer
* Förbättra utseendet av FormLayout på mobil
* Rätta menyer i contextualActions
* Ändra inte objekt i kod anropat från objektets destruktor (fel 428481)
* ändra inte den andra layouten reversetwins
* Ge eller ta bort fokus för överlagrat blad vid öppna/stäng
* Dra bara fönster med den globala verktygsraden när klickad och dragen
* Stäng OverlaySheet när tangenten Esc används
* Page: Gör så att egenskaperna padding, horizontalPadding och verticalPadding fungerar
* ApplicationItem: Använd bakgrundsegenskap
* AbstractApplicationItem: lägg till saknade egenskaper och beteende från QQC2 ApplicationWindow
* begränsa objektbredd till layoutbredd
* Rätta att bakåtknappen inte visas på lagrade sidhuvuden för mobil
* Tysta varning om "checkable" bindningssnurra i ActionToolBar
* byt ordningen på kolumner för höger-till-vänster layouter
* provisorisk lösning för att säkerställa att ungrabmouse anropas varje gång
* kontrollera att startSystemMove finns
* rätta avskiljare för speglade layouter
* dra fönster genom att klicka på tomma områden
* Rulla inte genom att dra med musen
* Rätta fall när svaret är null
* Rätta felaktig omstrukturering av Forward/BackButton.qml
* Säkerställ tom ikon är redo och inte ritas som föregående ikon
* Begränsa höjd av från bakåt- och framåtknappar i PageRowGlobalToolBarUI
* Tysta terminalskräp från ContextDrawer
* Tysta terminalskräp från ApplicationHeader
* Tysta terminalskräp från bakåt- och framåtknappar
* Förhindra att dragning av musen drar ett OverlaySheet
* Använd inte lib prefix vid byggen för Windows
* rätta twinformlayouts justeringshantering
* Förbättra läsbarhet av inbäddad QML i C++ kod

### KItemModels

* KRearrangeColumnsProxyModel: Rätta krasch med ingen källmodell
* KRearrangeColumnsProxyModel: bar kolumn 0 har underliggande objekt

### KNewStuff

* Rätta felaktig logik introducerad i e1917b6a
* Rätta dubbelborttagningskrasch i kpackagejob (fel 427910)
* Avråd från Button::setButtonText() och rätta dokumentationen av programmeringsgränssnittet, ingenting läggs till först
* Skjut upp alla cacheskrivningar på disk till vi har haft en lugn sekund
* Rätta krasch när listan av installerade filer är tom

### KNotification

* Markera att KNotification::activated() avråds från
* Utför några rimlighetskontroller av åtgärdstangenter (fel 427717)
* Använd FindGradle från ECM
* Rätta villkor för att använda dbus
* Rättning: enable legacy tray on platforms without dbus
* skriv om notifybysnore för att ge tillförlitligare stöd för Windows
* Lägg till kommentarer för att beskriva fältet DesktopEntry i filen notifyrc

### Ramverket KPackage

* Gör varningen "no metadata" något som bara gäller avlusning

### KPty

* Riv ut stöd för AIX, Tru64, Solaris, Irix

### Kör program

* Avråd från föråldrade RunnerSyntax-metoder
* Avråd från ignoreTypes och RunnerContext::Type
* Ställ inte in typen till fil/katalog om den inte finns (fel 342876)
* Uppdatera underhållsansvarig som diskuterats i e-postlistan
* Avråd från oanvänd konstruktor av RunnerManager
* Avråd från kategorifunktion
* Ta bort onödig kontroll om körprogram är vilande
* Avråd från metoderna defaultSyntax och setDefaultSyntax
* Städa användning av RunnerSyntax som har upphört

### KService

* Tillåt att NotShowIn=KDE-program, listade i mimeapps.list, används (fel 427469)
* Skriv in reservvärde för inställningsmodulers körningsrader med lämplig körbart program (fel 398803)

### KTextEditor

* [EmulatedCommandBar::switchToMode] Gör ingenting när de gamla och nya lägen är samma (fel 368130 som följer:)
* KateModeMenuList: ta bort speciella marginaler för Windows
* Rätta en minnesläcka i KateMessageLayout
* försök att undvika radera custom-styles för färgläggning vi inte alls rörde (fel 427654)

### Kwayland

* Tillhandahåll bekvämlighetsmetoder omkring wl_data_offet_accept()
* Markera uppräkningsvärden i en Q_OBJECT, Q_ENUM

### KWidgetsAddons

* ny setUsernameContextHelp i KPasswordDialog
* KFontRequester: ta bort den nu redundanta hjälpfunktionen nearestExistingFont

### KWindowSystem

* xcb: Rätta detektering av skärmstorlekar för hög upplösning

### NetworkManagerQt

* Lägg till enum och deklarationer för att tillåta att skicka capabilities i registreringsprocessen till NetworkManager

### Plasma ramverk

* BasicPlasmoidHeading komponenter
* Visa alltid ExpandableListitem knappar, inte bara när muspekaren hålls över (fel 428624)
* [PlasmoidHeading]: Ställ in implicit storleksändring riktigt
* Lås huvudfärger för Breeze mörk och Breeze ljus (fel 427864)
* Förena proportion för 32 bildpunkters och 22 bildpunkters batteriikoner
* Lägg till marginaltips i toolbar.svg och omstrukturera verktygsraden PC3
* Lägg till AbstractButton och Pane i PC3
* stöd exklusiva åtgärdsgrupper i sammanhangsberoende åtgärder
* Rätta att BusyIndicator roterar även när den är osynlig, igen
* Rätta att färger inte verkställs för mobil aktivitetsbytesikon
* Lägg till Plasma mobil aktivitetsbytare och ikoner för stäng applikation (för taskpanel)
* Bättre meny i PlasmaComponents3
* Ta bort onödiga förankringar i ComboBox.contentItem
* Runda skjutreglagets grepposition
* [ExpandableListItem] Läs in expanderad vy på begäran
* Lägg till saknad `PlasmaCore.ColorScope.inherit: false`
* Ställ in PlasmoidHeading colorGroup i rotelement
* [ExpandableListItem] Gör färglagd text 100 % ogenomskinlig (fel 427171)
* BusyIndicator: Rotera inte om osynlig (fel 426746)
* ComboBox3.contentItem måste vara QQuickTextInput för att rätta automatisk komplettering (fel 424076)
* FrameSvg: Nollställ inte cachen vid storleksändring
* Växla Plasmoider när genväg aktiveras (fel 400278)
* TextField 3: Lägg till saknad import
* Rätta identifierare i ikonen plasmavault_error
* PC3: rätta färg på TabButton beteckning
* Använd ett tips istället för en Boolean
* Tillåt Plasmoider att ignorera marginalerna

### Syfte

* Lägg till beskrivning av kaccounts youtube-leverantör

### QQC2StyleBridge

* Rätta bindningssnurra för ToolBar contentWidth
* Referera genvägsbeteckning direkt enligt id istället för underförstått
* ComboBox.contentItem måste vara QQuickTextInput för att rätta automatisk komplettering (fel 425865)
* Förenkla villkorsuttryck i Connections
* Rätta varning för Connections i ComboBox
* Lägg till stöd för qrc-ikoner i StyleItem (fel 427449)
* Indikera fokustillstånd för ToolButton riktigt
* Lägg till TextFieldContextMenu för sammanhangsberoende menyer med högerklick i TextField och TextArea
* Ställ in bakgrundsfärg för ScrollView i ComboBox

### Solid

* Lägg till stöd för sshfs i gränssnittet för fstab
* CMake: Använd pkg_search_module vid sökning efter plist
* Rätta imobiledevice-gränssnitt: Kontrollera version av programmeringsgränssnitt för DEVICE_PAIRED
* Rätta bygge av imobiledevice-gränssnitt
* Lägg till Solid-gränssnitt med användning av libimobiledevice för att hitta iOS-enheter
* Använd QHash för att avbilda var ordning inte behövs

### Sonnet

* Använd modern syntax för signal-slot anslutning

### Syntaxfärgläggning

* Kärnfunktionen "compact" saknas
* kommentera bort kontrollen, lägg till kommentar om varför den inte längre fungerar här
* Värdet position:sticky saknas
* rätta generering av php/* för ny kommentarfärgläggning
* Funktion: Ersätt Alert med Special-Comments syntax och ta bort Modelines
* Funktion: Lägg till `comments.xml` som övergripande syntax för diverse typer av kommentarer
* Rättning: CMake-syntax nu markerar `1` och `0` som särskilda Booleska värden
* Förbättring: Inkludera Modelines regler i filer där Alerts har lagts till
* Förbättring: Lägg till några fler Booleska värden i `cmake.xml`
* Solariserade teman: förbättra avskiljare
* Förbättring: Uppdatering för CMake 3.19
* Lägg till stöd för systemd enhetsfiler
* debchangelog: lägg till Hirsute Hippo
* Funktion: Tillåt flera `-s` väljare för verktyget `kateschema2theme`
* Förbättring: Lägg till diverse tester i konverteraren
* flytta fler verktygsskript till bättre platser
* flytta skriptet update-kate-editor-org.pl till en bättre plats
* kateschema2theme: Lägg till ett Python-verktyg för att konvertera gamla schemafiler
* Minska ogenomskinlighet i avskiljare för teman Breeze och Dracula
* Uppdatera README med avsnittet "Color themes files"
* Rättning: Använd `KDE_INSTALL_DATADIR` när syntaxfiler installeras
* rätta återgivning av --syntax-trace=region med mer än en grafik på samma position
* rätta några problem med skalet fish
* ersätt StringDetect med DetectChar / Detect2Chars
* ersätt några RegExpr med StringDetect
* ersätt RegExpr="." + framåtreferens med fallthroughContext
* ersätt \s* with DetectSpaces

### Säkerhetsinformation

Den utgivna koden har signerats med GPG genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org> Fingeravtryck för primär nyckel: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
