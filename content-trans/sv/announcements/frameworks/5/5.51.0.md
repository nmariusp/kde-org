---
aliases:
- ../../kde-frameworks-5.51.0
date: 2018-10-15
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Lägg till anrop till KIO::UDSEntry::reserve i I/O-slavarna timeline/tags
- [balooctl] Mata ut buffrad rad "Indexerar &lt;fil&gt;" när indexering startar
- [FileContentIndexer] Anslut signalen finished från processen extractor
- [PositionCodec] Undvik krasch vid felaktig data (fel 367480)
- Rätta ogiltig teckenkonstant
- [Balooctl] Ta bort kontroll av överliggande katalog (fel 396535)
- Tillåt att icke-existerande kataloger tas bort från inkluderings- och exkluderingslistor (fel 375370)
- Använd String för att lagra UDS_USER och UDS_GROUP med typen String (fel 398867)
- [tags_kio] Rätta parenteser. På något vis tog det sig förbi min kodkontroll
- Undanta genomfiler från indexering

### BluezQt

- Implementera programmeringsgränssnitten Media och MediaEndpoint

### Breeze-ikoner

- Rätta "stackanvändning efter räckvidd" detekterad ab ASAN i CI
- Rätta enfärgade ikoner som saknar stilmallar
- Ändra drive-harddisk till en mer anpassningsbar stil
- Lägg till ikonerna firewall-config och firewall-applet
- Gör låset på ikonen plasmavault synlig för Breeze mörk
- Lägg till plussymbol i document-new.svg (fel 398850)
- Tillhandahåll ikoner för 2x skalning

### Extra CMake-moduler

- Kompilera Python-bindningar med samma SIP-flaggor som används av PyQt
- Android: Tillåt att en relativ sökväg anges som APK-katalog
- Android: Erbjud en reservmetod på ett riktigt sätt för program som inte har ett manifest
- Android: Säkerställ att Qm-översättningar läses in
- Rätta Android-byggen som använder cmake 3.12.1
- l10n: Rätta matchande siffror i arkivnamn
- Lägg till QT_NO_NARROWING_CONVERSIONS_IN_CONNECT som standardkompileringsflaggor
- Bindningar: Korrekt hantering aqv källor som innehåller UTF-8
- Iterera verkligen över CF_GENERATED, istället för att kontrollera objekt 0 hela tiden

### KActivities

- Rätta felaktig referens med "auto" som blir "QStringBuilder"

### KCMUtils

- Hantera returhändelser
- Ändra manuellt storlek på KCMUtilDialog till sizeHint() (fel 389585)

### KConfig

- Rätta fel när sökvägslistor läses
- kcfg_compiler dokumenterar nu giltig indata för dess typ 'Color'

### KFileMetaData

- Ta bort egen implementering av QString till TString konvertering för taglibwriter
- Öka testtäckningsgrad för taglibwriter
- Implementera mer grundläggande taggar för taglibwriter
- Ta bort användning av egna TString till QString konverteringsfunktioner
- Öka version av taglib som krävs till 1.11.1
- Implementera läsning av replaygain-taggar

### KHolidays

- Lägg till helger för Elfenbenskusten (franska) (fel 398161)
- holiday*hk** - Rätta datum för Tuen Ng-festivalen under 2019 (fel 398670)

### KI18n

- Ge ändring av CMAKE_REQUIRED_LIBRARIES riktig räckvidd
- Android: Säkerställ att vi letar efter .mo-filer med rätt sökväg

### KIconThemes

- Börja rita emblem i nedre högra hörnet

### KImageFormats

- kimg_rgb: Optimera bort QRegExp och QString::fromLocal8Bit
- [EPS] Rätta krasch när program stängs av (vid försök att låta klippbordsbild bevaras) (fel 397040)

### KInit

- Minska loggskräp genom att inte kontrollera om en fil med tomt namn finns (fel 388611)

### KIO

- Tillåt icke-lokal omdirigering med file:// till en Windows WebDAV webbadress
- [KFilePlacesView] Byt ikon för det sammanhangsberoende menyalternativet 'Redigera' i platspanelen
- [Platspanel] Använd lämpligare nätverksikon
- [KPropertiesDialog] Visa monteringsinformation för kataloger i / (rot)
- Rätta borttagning av filer från DAV (fel 355441)
- Undvik QByteArray::remove i AccessManagerReply::readData (fel 375765)
- Försök inte återställa ogiltiga användarplatser
- Gör det möjligt att byta katalog också när webbadressen har avslutande snedstreck
- Krascher av I/O-slavar hanteras nu av KCrash istället för egen kod av undermålig kvalitet
- Rättade att en fil skapad från inklistrat klippbordsinnehåll dyker upp bara efter en fördröjning
- [PreviewJob] Skicka aktiverade miniatyrbildsinsticksprogram till miniatyrbildsslaven (fel 388303)
- Förbättra felmeddelande "Otillräckligt diskutrymme"
- IKWS: Använd "X-KDE-ServiceTypes" som inte avråds från när skrivbordsfiler skapas
- Rätta WebDAV-målrubrik vid COPY- och MOVE-åtgärder
- Varna användare innan kopierings- eller förflyttningsåtgärd om tillgängligt utrymme inte räcker (fel 243160)
- Flytta SMB-inställningsmodul till kategorin nätverksinställningar
- Papperskorg: Rätta tolkning av katalogstorlekscache
- kioexecd: Bevaka om de tillfälliga filerna skapas eller ändras (fel 397742)
- Rita inte ramar och skuggor omkring bilder med genomskinlighet (fel 258514)
- Rättade att filtypikonen i filegenskapsdialogrutan återgavs suddig på skärmar med hög upplösning

### Kirigami

- Öppna lådan på ett riktigt sätt när den dras med greppet
- Extra marginal när sidoradens globala verktygsraden är en verktygsrad
- Stöd också föredragen bredd av layout för bladstorlek
- Bli av med sista kvarlevorna av controls1
- Tillåt att åtgärder skapas för avskiljare
- Gå med på ett godtyckligt antal kolumner i CardsGridview
- Förstör inte menyalternativ aktivt (fel 397863)
- Ikoner i åtgärdsknappar är enfärgade
- Gör inte ikoner enfärgade när de inte ska vara det
- Återställ godtycklig *1,5 storlek av ikoner för mobil
- Delegatåteranvändning: Begär inte sammanhangsobjektet två gånger
- Använd den interna implementeringen av materialkrusning
- Kontrollera rubrikbredd enligt källstorlek om horisontell
- Exponera alla egenskaper från BannerImage i Cards
- Använd också DesktopIcon med Plasma
- Ladda sökvägar med file:// riktigt
- Återställ "Börja titta efter sammanhanget från själva delegaten"
- Lägg till testfall som visar räckviddsproblem i DelegateRecycler
- Ange explicit en höjd för overlayDrawers (fel 398163)
- Börja titta efter sammanhanget från själva delegaten

### KItemModels

- Använd referens i for-snurra för typ med icketrivial kopieringskonstruktor

### KNewStuff

- Lägg till stöd för Attica-etikettstöd (fel 398412)
- [KMoreTools] Ge menyalternativet "Anpassa..." en lämplig ikon (fel 398390)
- [KMoreTools] Reducera menyhierarki
- Rätta 'Omöjligt att använda knsrc-fil för uppladdning från icke-standardplats' (fel 397958)
- Gör så att testverktyg länkas på Windows
- Laga bygge med Qt 5.9
- Lägg till stöd för Attica-etikettstöd

### KNotification

- Rätta en krasch som orsakades av felaktig livstidshantering av canberra-baserad ljudunderrättelse (fel 398695)

### KNotifyConfig

- Rätta UI-filtips: KUrlRequester har nu QWidget som basklass

### Ramverket KPackage

- Använd referens i for-snurra för typ med icketrivial kopieringskonstruktor
- Flytta Qt5::DBus till 'PRIVATE' länkmål
- Skicka signaler när ett paket installeras eller avinstalleras

### KPeople

- Rätta att signaler inte skickas när två personer sammanfogas
- Krascha inte om person tas bort
- Definiera PersonActionsPrivate som klass, enligt tidigare deklaration
- Gör programmeringsgränssnittet PersonPluginManager öppet

### Kross

- Kärna: Hantera bättre kommentarer för åtgärder

### KTextEditor

- Rita bara kodvikningsmarkör för kodvikningsområden med flera rader
- Initiera m_lastPosition
- Skript: isCode() returnerar false för dsAlert text (fel 398393)
- Använd R-skript hl för utprovning av R-indentering
- Uppdatera R-indenteringsskript
- Rätta färgscheman för Solariserad ljus och mörk (fel 382075)
- Kräv inte Qt5::XmlPatterns

### KTextWidgets

- ktextedit: Ladda objektet QTextToSpeech vid behov

### Ramverket KWallet

- Logga misslyckade försök att öppna plånbok

### Kwayland

- Gör inte tyst fel om skada skickas före buffert (fel 397834)
- [server] Returnera inte tidigt vid misslyckande i touchDown reservkod
- [server] Rätta hantering av fjärråtkomstbuffert när utdata inte är bunden
- [server] Försök inte skapa dataerbjudanden utan källa
- [server] Avbryt dragstart vid riktiga villkor och utan att ange ett fel

### KWidgetsAddons

- [KCollapsibleGroupBox] Respektera stilens animeringslängd för grafisk komponent (fel 397103)
- Ta bort föråldrad versionskontroll av Qt
- Kompilera

### KWindowSystem

- Använd _NET_WM_WINDOW_TYPE_COMBO istället för _NET_WM_WINDOW_TYPE_COMBOBOX

### KXMLGUI

- Rätta webbadress för leverantör av OCS i dialogrutan Om

### NetworkManagerQt

- Använd matchande uppräkningsvärde AuthEapMethodUnknown för att jämföra en AuthEapMethod

### Plasma ramverk

- Öka versionssträngar för teman eftersom det finns nya ikoner i 5.51
- Höj också inställningsfönstret när det återanvänds
- Lägg till saknad komponent: RoundButton
- Kombinera filer för skärmvisningsikoner och flytta till Plasma ikontema (fel 395714)
- [Plasma Components 3 Slider] Rätta implicit greppstorlek
- [Plasma Components 3 ComboBox] Byt poster med mushjul
- Stöd knappikoner när de är tillgängliga
- Rätta att veckonamn inte visas riktigt i kalendern när veckan börjar med en annan dag än måndag eller söndag (fel 390330)
- [DialogShadows] Använd position 0 för inaktiverade kanter med Wayland

### Prison

- Rätta återgivning av Aztec-koder med proportion skild från 1
- Ta bort antaganden om streckkodens proportion från QML-integreringen
- Rätta glitchar i återgivningen orsakade av avrundningsfel i Code 128
- Lägg till stöd för Code 128-streckkoder

### Syfte

- Gör cmake 3.0 till den minimala cmake-versionen

### QQC2StyleBridge

- Liten standardvaddering när det finns en bakgrund

### Solid

- Visa inte ett emblem för monterade diskar, bara omonterade diskar
- [Fstab] Ta bort stöd för AIX
- [Fstab] Ta bort stöd för Tru64 (**osf**)
- [Fstab] Visa icke-tomt delningsnamn i fallet då rotfilsystemet exporteras (fel 395562)
- Föredra tillhandahållen enhetsetikett också för loop-enheter

### Sonnet

- Rätta söndrig språkgissning
- Förhindra att färgläggningen raderar markerad text (fel 398661)

### Syntaxfärgläggning

- i18n: Rätta extrahering av temanamn
- Fortran: Färglägg larm i kommentarer (fel 349014)
- Undvik att huvudsammanhang kan bli #poped
- Vakt för oändlig tillståndsövergång
- YAML: Lägg till bokstavliga och vikta blockstilar (fel 398314)
- Logcat &amp; SELinux: Förbättringar för nya solariserade teman
- AppArmor: Rätta krascher i öppningsregler (i KF5.50) och förbättringar för nya solariserade teman
- Sammanfoga git://anongit.kde.org/syntax-highlighting
- Uppdatera git-ignorerade saker
- Använd referens i for-snurra för typ med icketrivial kopieringskonstruktor
- Rättning: E-postfärgläggning för ostängd parentes i rubrik (fel 398717)
- Perl: Rätta klammerparenteser, variabler, strängreferenser med mera (fel 391577)
- Bash: Rätta parametrar och expansion av klammerparenteser (bug 387915)
- Lägg till Solariserad ljust och mörkt tema

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
