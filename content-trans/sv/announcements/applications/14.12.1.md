---
aliases:
- ../announce-applications-14.12.1
changelog: true
date: '2015-01-08'
description: KDE levererar KDE-program 14.12.1.
layout: application
title: KDE levererar KDE-program 14.12.1
version: 14.12.1
---
13:e januari, 2015. Idag ger KDE ut den första stabilitetsuppdateringen av <a href='../14.12.0'>KDE-program 14.12</a>. Utgåvan innehåller bara felrättningar och översättningsuppdateringar, och är därmed en säker och behaglig uppdatering för alla.

Mer än 50 registrerade felrättningar omfattar förbättringar av arkiveringsverktyget Ark, UML-modelleringsverktyget Umbrello, dokumentvisaren Okular, uttalsinlärningsprogrammet Artikulate och fjärrskrivbordsklienten KRDC.

Utgåvan inkluderar också versioner för långtidsunderhåll av Plasma arbetsrymder 4.11.15, KDE:s utvecklingsplattform 4.14.4 och Kontakt-sviten 4.14.4.
