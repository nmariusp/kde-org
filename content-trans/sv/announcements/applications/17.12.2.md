---
aliases:
- ../announce-applications-17.12.2
changelog: true
date: 2018-02-08
description: KDE levererar KDE-program 17.12.2
layout: application
title: KDE levererar KDE-program 17.12.2
version: 17.12.2
---
8:e februari, 2018. Idag ger KDE ut den andra stabilitetsuppdateringen av <a href='../17.12.0'>KDE-program 17.12</a>. Utgåvan innehåller bara felrättningar och översättningsuppdateringar, och är därmed en säker och behaglig uppdatering för alla.

Omkring 20 registrerade felrättningar omfattar förbättringar av bland annat Kontact, Dolphin, Gwenview, KGet och Okular.
