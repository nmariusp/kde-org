---
aliases:
- ../announce-applications-18.08.3
changelog: true
date: 2018-11-08
description: KDE에서 KDE 프로그램 18.08.3 출시
layout: application
title: KDE에서 KDE 프로그램 18.08.3 출시
version: 18.08.3
---
November 8, 2018. Today KDE released the third stability update for <a href='../18.08.0'>KDE Applications 18.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Kontact, Ark, Dolphin, KDE 게임, Kate, Okular, Umbrello 등에 20개 이상의 버그 수정 및 기능 개선이 있었습니다.

개선 사항:

- KMail의 HTML 보기 모드를 기억하고 허용되는 경우 외부 이미지를 다시 불러옴
- Kate는 편집 세션간 메타 정보(책갈피 포함)를 기억함
- 새로운 QtWebEngine 버전 사용 시 Telepathy 텍스트 UI의 자동 스크롤 수정
