---
aliases:
- ../announce-applications-16.04.2
changelog: true
date: 2016-06-14
description: KDE publie les applications de KDE en version 16.04.2
layout: application
title: KDE publie les applications de KDE en version 16.04.2
version: 16.04.2
---
14 juin 2016. Aujourd'hui, KDE a publié la seconde mise à jour de consolidation pour les <a href='../16.04.0'>applications 16.04 de KDE</a>. Cette version ne contient que des corrections de bogues et des mises à jour de traduction, permettant une mise à jour sûre et appréciable pour tout le monde

Plus de 25 corrections de bogues apportent des améliorations à akonadi, ark, artikulate, dolphin. kdenlive, kdepim et bien d'autres.

Cette mise à jour inclut aussi une version de prise en charge à long terme pour la plate-forme de développement 4.14.21 de KDE.
