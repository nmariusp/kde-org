---
aliases:
- ../announce-applications-18.08.2
changelog: true
date: 2018-10-11
description: KDE stelt KDE Applicaties 18.08.2 beschikbaar
layout: application
title: KDE stelt KDE Applicaties 18.08.2 beschikbaar
version: 18.08.2
---
11 oktober 2018. Vandaag heeft KDE de tweede update voor stabiliteit vrijgegeven voor <a href='../18.08.0'>KDE Applicaties 18.08</a> Deze uitgave bevat alleen bugreparaties en updates van vertalingen, die een veilige en plezierige update voor iedereen levert.

Meer dan een dozijn aangegeven reparaties van bugs, die verbeteringen aan Kontact, Dolphin, Gwenview, KCalc, Umbrello, naast andere.

Verbeteringen bevatten:

- Slepen van een bestand in Dolphin kan niet langer per ongeluk inline hernoemen starten
- KCalc staat beide 'punt' en 'komma' toetsen toe bij invoeren van decimalen
- Een visuele glimp in het kaartenpak Paris voor kaartspellen van KDE is gerepareerd
