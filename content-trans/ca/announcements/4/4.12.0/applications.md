---
date: '2013-12-18'
hidden: true
title: Les aplicacions del KDE 4.12 fan un pas endavant en la gestió d'informació
  personal i moltes millores globals
---
La comunitat del KDE anuncia amb satisfacció la darrera actualització major de les aplicacions del KDE que aporta noves funcionalitats i esmenes. Aquest llançament es distingeix per moltes millores en la pila del KDE PIM, que aporten un rendiment millor i moltes funcionalitats noves. El Kate ha integrat amb eficiència els connectors de Python i ha afegit una implementació inicial de les macros del Vim. Els jocs i les aplicacions educatives tenen diverses funcionalitats noves.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.12.0/screenshots/kate.png" width="600px">}}

L'editor gràfic de text més avançat de Linux Kate ha rebut més millores en la compleció de codi, aquesta vegada s'ha incorporat una <a href='http://scummos.blogspot.com/2013/10/advanced-code-completion-filtering-in.html'>coincidència de codi avançada, gestionant abreviatures i coincidència parcial en les classes</a>. Per exemple, el codi nou faria coincidir un «QualIdent» teclejat amb «QualifiedIdentifier». El Kate també incorpora una <a href='http://dot.kde.org/2013/09/09/kde-commit-digest-18th-august-2013'>implementació inicial per les macros del Vim</a>. I el millor de tot, aquestes millores també es traspassen al KDevelop i a altres aplicacions que utilitzen la tecnologia del Kate.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.12.0/screenshots/okular.png" width="600px">}}

El visualitzador de documents Okular <a href='http://tsdgeos.blogspot.com/2013/10/changes-in-okular-printing-for-412.html'>ara té en compte els marges del maquinari d'impressora</a>, té suport d'àudio i vídeo per l'epub, una cerca millor i ara pot gestionar més transformacions incloses les de les metadades EXIF de les imatges. En l'eina de diagrames UML Umbrello, les associacions ara es poden <a href='http://dot.kde.org/2013/09/20/kde-commit-digest-1st-september-2013'>dibuixar amb capes diferents</a> i l'Umbrello <a href='http://dot.kde.org/2013/09/09/kde-commit-digest-25th-august-2013'>afegeix retroalimentació visual si s'han documentat els ginys</a>.

El guarda de privadesa KGpg mostra més informació als usuaris; i el KWalletManager, l'eina per desar les vostres contrasenyes, ara pot <a href=' http://www.rusu.info/wp/?p=248'>emmagatzemar-les en format GPG</a>. El Konsole introdueix una funcionalitat nova: Ctrl-clic per llançar directament URL des de la sortida de la consola. Ara també pot <a href='http://martinsandsmark.wordpress.com/2013/11/02/mangonel-1-1-and-more/'>llistar processos en avisar de la sortida</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.12.0/screenshots/dolphin.png" width="600px">}}

El KWebKit ha afegit la capacitat d'<a href='http://dot.kde.org/2013/08/09/kde-commit-digest-7th-july-2013'>escalar automàticament el contingut per a coincidir amb la resolució de l'escriptori</a>. El gestor de fitxers Dolphin ha introduït diverses millores de rendiment en ordenar i mostrar fitxers, reduint l'ús de la memòria i augmentant la velocitat. El KRDC ha introduït la reconnexió automàtica en VNC i ara el KDialog proporciona un accés als quadres de missatge «detailedsorry» i «detailederror» per a scripts de consola més informatius. El Kopete ha actualitzat el seu connector OTR i el protocol del Jabber té la implementació per al XEP-0264: Miniatures de transferències de fitxers. A banda d'aquestes funcionalitats el focus principal ha estat netejar el codi i esmenar els avisos de compilació.

### Jocs i programari educatiu

Els jocs del KDE s'han treballat en diverses àrees. El KReversi ara està <a href='http://tsdgeos.blogspot.ch/2013/10/kreversi-master-is-now-qt-quick-based.html'>basat en QML i Qt Quick</a>, generant una experiència de joc millor i més fluida. El KNetWalk també s'ha <a href='http://tsdgeos.blogspot.ch/2013/08/knetwalk-portedx-to-qtquick.html'>adaptat</a> amb el mateix avantatge i també la capacitat de definir una graella d'amplada i alçada personalitzades. El Konquest ara té un jugador d'IA estimulant anomenat «Becai».

A les aplicacions educatives s'han efectuat alguns canvis notables. El KTouch <a href='http://blog.sebasgo.net/blog/2013/11/12/what-is-new-for-ktouch-in-kde-sc-4-dot-12/'>introdueix la possibilitat de lliçons personalitzades i diversos cursos nous</a>; el KStars té un <a href='http://knro.blogspot.ch/2013/10/demo-of-ekos-alignment-module.html'>mòdul d'alineació per a telescopis</a> nou i més precís, cerqueu <a href='http://www.youtube.com/watch?v=7Dcn5aFI-vA'>aquí un vídeo de youtube</a> de les noves funcionalitats. El Cantor, que proporciona una IU senzilla i potent per a diversos dorsals matemàtics, ara té dorsals <a href='http://blog.filipesaraiva.info/?p=1171'>per Python2 i Scilab</a>. Llegiu més quant al potent dorsal Scilab <a href='http://blog.filipesaraiva.info/?p=1159'>aquí</a>. El Marble ha afegit integració amb l'ownCloud (l'arranjament està disponible a Preferències) i implementa la representació per superposició de capes. El KAlgebra fa possible exportar plànols 3D a PDF, facilitant la manera de compartir la vostra feina. Per acabar, s'han esmenat molts errors en diverses aplicacions educatives del KDE.

### Correu, calendari i informació personal

El KDE PIM, un conjunt d'aplicacions del KDE per a gestionar correu, calendari i altra informació personal, ha rebut molta feina.

Començant amb el client de correu KMail, ara té la <a href='http://dot.kde.org/2013/10/11/kde-commit-digest-29th-september-2013'>implementació d'AdBlock</a> (quan l'HTML està activat) i s'ha millorat la implementació de detecció de frau expandint els URL retallats. El nou agent de l'Akonadi anomenat «FolderArchiveAgent» permet als usuaris arxivar els correus llegits en carpetes específiques i la IGU de la funcionalitat «envia més tard» s'ha netejat. El KMail també es beneficia d'una implementació de filtres Sieve millorada. El Sieve permet el filtratge de correus des de la banda del servidor i ara podeu <a href='http://www.aegiap.eu/kdeblog/2013/08/new-in-kdepim-4-12-sieve-script-parsing-22/'>crear i modificar els filtres en els servidors</a> i <a href='http://www.aegiap.eu/kdeblog/2013/08/new-in-kdepim-4-12-sieve-12/'>convertir els filtres existents del KMail a filtres del servidor</a>. La implementació «mbox» del KMail també <a href='http://www.aegiap.eu/kdeblog/2013/08/new-in-kdepim-4-12-mboximporter/'>s'ha millorat</a>.

En altres aplicacions, diversos canvis faciliten la feina i la fan més agradable. S'introdueix una eina nova, <a href='http://www.aegiap.eu/kdeblog/2013/11/new-in-kdepim-4-12-kaddressbook/'>el «ContactThemeEditor»</a>, que permet crear temes del Grantlee del KAddressBook per mostrar contactes. La llibreta d'adreces ara pot mostrar una vista prèvia abans d'imprimir les dades. El KNotes ha rebut <a href='http://www.aegiap.eu/kdeblog/2013/11/what-news-in-kdepim-4-12-knotes/'>feina de resolució d'errors</a>. L'eina de blogs Blogilo ara pot tractar traduccions i hi ha una gran varietat d'esmenes i millores en totes les aplicacions del KDE PIM.

En benefici de totes les aplicacions, la memòria cau de dades subjacent del KDE PIM s'ha <a href='http://ltinkl.blogspot.ch/2013/11/this-month-october-in-red-hat-kde.html'>treballat per millorar el rendiment, l'estabilitat i l'escalabilitat</a>, solucionant la <a href='http://www.progdan.cz/2013/10/akonadi-1-10-3-with-postgresql-fix/'>implementació per al PostgreSQL amb les darreres Qt 4.8.5</a>. I hi ha una eina nova de línia d'ordres, el «calendarjanitor» que pot explorar totes les dades de calendari cercant incidències errònies i afegeix un diàleg de depuració per cerques. Un agraïment molt especial al Laurent Montel per la feina que està fent en les funcionalitats del KDE PIM!

#### Instal·lació d'aplicacions del KDE

El programari KDE, incloses totes les seves biblioteques i les seves aplicacions, és disponible de franc d'acord amb les llicències de codi font obert (Open Source). El programari KDE s'executa en diverses configuracions de maquinari i arquitectures de CPU com ARM i x86, sistemes operatius i treballa amb qualsevol classe de gestor de finestres o entorn d'escriptori. A banda del Linux i altres sistemes operatius basats en UNIX, podeu trobar versions per al Windows de Microsoft de la majoria d'aplicacions del KDE al lloc web <a href='http://windows.kde.org'>programari de Windows</a> i versions per al Mac OS X d'Apple en el <a href='http://mac.kde.org/'>lloc web del programari KDE per al Mac</a>. Les compilacions experimentals d'aplicacions del KDE per a diverses plataformes mòbils com MeeGo, MS Windows Mobile i Symbian es poden trobar en la web, però actualment no estan suportades. El <a href='http://plasma-active.org'>Plasma Active</a> és una experiència d'usuari per a un espectre ampli de dispositius, com tauletes i altre maquinari mòbil.

El programari KDE es pot obtenir en codi font i en diversos formats executables des de <a href='https://download.kde.org/stable/4.12.0'>download.kde.org</a> i també es pot obtenir en <a href='/download'>CD-ROM</a> o amb qualsevol dels <a href='/distributions'>principals sistemes GNU/Linux i UNIX</a> que es distribueixen avui en dia.

##### Paquets

Alguns venedors de Linux/UNIX OS han proporcionat gentilment paquets executables de 4.12.0 per a algunes versions de la seva distribució, i en altres casos ho han fet els voluntaris de la comunitat.

##### Ubicació dels paquets

Per a una llista actual de paquets executables disponibles dels quals ha informat l'equip de llançament del KDE, si us plau, visiteu el <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_SC_4.12.0'>wiki de la comunitat</a>.

El codi font complet per a 4.12.0 es pot <a href='/info/4/4.12.0'>descarregar de franc</a>. Les instruccions per a compilar i instal·lar el programari KDE, versió 4.12.0, estan disponibles a la <a href='/info/4/4.12.0#binary'>pàgina d'informació de la versió 4.12.0</a>.

#### Requisits del sistema

Per tal d'aprofitar al màxim aquestes versions, es recomana utilitzar una versió recent de les Qt, com la 4.8.4. Això és necessari per a assegurar una experiència estable i funcional, ja que algunes millores efectuades en el programari KDE s'ha efectuat realment en la infraestructura subjacent de les Qt.

Per tal de fer un ús complet de les capacitats del programari KDE, també es recomana l'ús dels darrers controladors gràfics per al vostre sistema, ja que aquest pot millorar substancialment l'experiència d'usuari, tant per a les funcionalitats opcionals, com el rendiment i l'estabilitat en general.
