---
aliases:
- ../../kde-frameworks-5.66.0
date: 2020-01-11
layout: framework
libCount: 70
---
### Tots els frameworks

- Adaptació des de QRegExp a QRegularExpression
- Adaptació des de «qrand» a QRandomGenerator
- Corregeix la compilació amb les Qt 5.15 (p. ex. «endl» ara és Qt::endl, QHash «insertMulti» ara requereix usar QMultiHash...)

### Attica

- No usa un «nullptr» verificat com a origen de les dades
- Permet múltiples elements fills als elements de comentari
- Estableix una cadena d'agent adequada per a les sol·licituds de l'Attica

### Baloo

- Informa correctament si el «baloo_file» no és disponible
- Comprova el valor de retorn de «cursor_open»
- Inicialitza els valors del monitor QML
- Mou els mètodes d'anàlisi de l'URL des del «kioslave» a l'objecte de la consulta

### BluezQt

- Afegeix una interfície de Battery1

### Icones Brisa

- Canvia la icona XHTML perquè sigui una icona HTML porpra
- Fusiona els auriculars i el zigzag al centre
- Afegeix la icona d'«application/x-audacity-project» (error 415722)
- Afegeix la icona «preferences-system» de 32px
- Afegeix la icona d'«application/vnd.apple.pkpass» (error 397987)
- Icona per al KTimeTracker usant el PNG que hi ha en el repositori de l'aplicació, que se substituirà pel SVG del Brisa real
- Afegeix la icona del Kipi, cal refer-lo com a s SVG de tema Brisa [o simplement eliminar el Kipi]

### Mòduls extres del CMake

- [android] Corregeix l'objectiu «apk» d'instal·lació
- Permet el PyQt5 compilat amb el SIP 5

### Integració del marc de treball

- Elimina ColorSchemeFilter del KStyle

### Eines de Doxygen del KDE

- Mostra el nom de la classe/espai de noms completament qualificat com a capçalera de pàgina (error 406588)

### KCalendarCore

- Millora el README.md per tenir una secció «Introduction»
- Fa que les coordenades geogràfiques de la incidència també sigui accessible com una propietat
- Corregeix la generació de RRULE per a les zones horàries

### KCMUtils

- Fa obsolet «KCModuleContainer»

### KCodecs

- Corregeix un «cast» no vàlid a una enumeració canviant el tipus a «int» en lloc d'«enum»

### KCompletion

- Fa obsolet KPixmapProvider
- [KHistoryComboBox] Afegeix un mètode per definir un proveïdor d'icones

### KConfig

- Neteja del protocol de transport del «kconfig» de l'EBN
- Exposa el «getter» a la configuració del KConfigWatcher
- Corregeix «writeFlags» amb KConfigCompilerSignallingItem
- Afegeix un comentari apuntant a l'historial de Retallar i Suprimir compartint una drecera

### KConfigWidgets

- Reanomena «Configura les dreceres« a «Configura les dreceres de teclat» (error 39488)

### KContacts

- Alinea la configuració dels ECM i les Qt amb les convencions dels Frameworks
- Especifica la versió de dependència dels ECM com a qualsevol altre Framework

### KCoreAddons

- Afegeix KPluginMetaData::supportsMimeType
- [KAutoSaveFile] Usa «QUrl::path()» en lloc de «toLocalFile()»
- Soluciona la construcció amb PROCSTAT: afegeix la implementació que manca de KProcessList::processInfo
- [KProcessList] Optimitza KProcessList::processInfo (error 410945)
- [KAutoSaveFile] Millora el comentari a «tempFileName()»
- Corregeix «KAutoSaveFile» que estava trencat pels camins llargs (error 412519)

### KDeclarative

- [KeySequenceHelper] Captura la finestra real quan està incrustada
- Afegeix un subtítol opcional al delegat de la quadrícula
- [QImageItem/QPixmapItem] No perd la precisió durant el càlcul

### KFileMetaData

- Correcció parcial per als caràcters accentuats en el nom de fitxer al Windows
- Elimina les declaracions privades no requerides del «taglibextractor»
- Solució parcial per a acceptar caràcters accentuats al Windows
- xattr: corregeix una fallada amb els enllaços simbòlics penjats (error 414227)

### KIconThemes

- Estableix el Brisa com a tema predeterminat en llegir des del fitxer de configuració
- Fa obsoleta la funció «IconSize()» de nivell superior
- Corregeix el centrat de les icones escalades als mapes de píxels amb ppp alts

### KImageFormats

- pic: corregeix un comportament no definit d'«Invalid-enum-value»

### KIO

- [KFilePlacesModel] Corregeix la comprovació dels esquemes acceptats per als dispositius
- Incrusta les dades de protocol per a la versió Windows de l'«ioslave» «trash»
- Afegeix el suport per muntar els URL del KIOFuse per a les aplicacions que no usen el KIO (error 398446)
- Afegeix la implementació del truncament a FileJob
- Fa obsolet KUrlPixmapProvider
- Fa obsolet KFileWidget::toolBar
- [KUrlNavigator] Afegeix el suport per a RPM al «krarc:» (error 408082)
- KFilePlaceEditDialog: corregeix una fallada en editar el lloc Paperera (error 415676)
- Afegeix un botó per obrir la carpeta al Filelight per a veure més detalls
- Mostra més detalls al diàleg d'avís que es mostra abans d'iniciar una operació privilegiada
- KDirOperator: usa una alçada fixa de línia per a la velocitat del desplaçament
- Ara es mostren camps addicionals com l'hora de supressió i el camí original al diàleg de propietats de fitxer
- KFilePlacesModel: emparenta adequadament «tagsLister» per a evitar fuites de memòria. Introduït amb la D7700
- HTTP ioslave: crida la classe base correcta a «virtual_hook()». La base de l'«ioslave» HTTP és TCPSlaveBase, no SlaveBase
- FTP ioslave: corregeix 4 caràcters de l'hora interpretats com a any
- Torna a afegir KDirOperator::keyPressEvent per mantenir BC
- Usa QStyle per a determinar les mides de les icones

### Kirigami

- ActionToolBar: només mostra el botó de desbordament si hi ha elements visibles al menú (error 415412)
- No construeix ni instal·la les plantilles de les aplicacions a l'Android
- No posa el marge al codi font de CardsListView
- Afegeix la implementació per als components de visualització personalitzats a Action
- Permet que els altres components creixin si hi ha altres coses a la capçalera
- Elimina la creació dinàmica d'elements a DefaultListItemBackground
- Torna a presentar el botó per reduir (error 415074)
- Mostra la icona de la finestra de l'aplicació a l'AboutPage

### KItemModels

- Afegeix KColumnHeadersModel

### KJS

- S'han afegit proves per a «Math.exp()»
- S'han afegit proves per a diversos operadors d'assignació
- Casos especials de prova d'operadors de multiplicació (*, / i %)

### KNewStuff

- Assegura que el títol del diàleg és correcte amb un motor no inicialitzat
- No mostra la icona d'informació al delegat de la vista prèvia gran (error 413436)
- Admet instal·lació d'arxius amb ordres d'adopció (error 407687)
- Envia el nom de la configuració junt amb les sol·licituds

### KPeople

- Exposa l'«enum» al compilador de metaobjectes

### KQuickCharts

- També corregeix els fitxers de capçalera dels «shaders»
- Corregeix les llicències de les capçaleres dels «shaders»

### KService

- Fa obsolet KServiceTypeProfile

### KTextEditor

- Afegeix la propietat «line-count» a la ConfigInterface
- Evita el desplaçament horitzontal no desitjat (error 415096)

### KWayland

- [plasmashell] Actualitza la documentació de «panelTakesFocus» per fer-la més genèrica
- [plasmashell] Afegeix un senyal per canviar el «panelTakesFocus»

### KXMLGUI

- KActionCollection: proporciona un senyal «changed()» com a substitució de «removed()»
- Ajusta el títol de la finestra de configuració de les dreceres de teclat

### NetworkManagerQt

- Manager: afegeix el suport per AddAndActivateConnection2
- CMake: considera les capçaleres del NM com a inclusions del sistema
- Sincronitza Utils::securityIsValid amb el NetworkManager (error 415670)

### Frameworks del Plasma

- [ToolTip] Arrodoneix la posició
- Activa els esdeveniments de la roda a «Slider {}»
- Sincronitza l'indicador WindowDoesNotAcceptFocus del QWindow a la interfície «plasmashell» del Wayland (error 401172)
- [calendar] Comprova els límits d'accés a la matriu a la cerca del QLocale
- [Plasma Dialog] Usa QXcbWindowFunctions per a definir els tipus de finestra que els WindowFlags de les Qt no coneixen
- [PC3] Completa l'animació de la barra de progrés del Plasma
- [PC3] Només mostra l'indicador de la barra de progrés quan els finals no se superposen
- [RFC] Corregeix els marges de la icona de configuració de la pantalla (error 400087)
- [ColorScope] Torna a funcionar amb els QObjects normals
- [Breeze Desktop Theme] Afegeix la icona monocroma «user-desktop»
- Elimina l'amplada predeterminada del PlasmaComponents3.Button
- [PC3 ToolButton] Té en compte l'etiqueta per als esquemes de color complementari (error 414929)
- S'han afegit colors de fons per a la vista activa i inactiva d'icona (error 370465)

### Purpose

- Usa els ECMQMLModules estàndard

### QQC2StyleBridge

- [ToolTip] Arrodoneix la posició
- Actualitza la correcció de la mida als canvis del tipus de lletra

### Solid

- Mostra primer / a la descripció d'accés de l'emmagatzematge muntat
- Assegura que els sistemes de fitxers muntats concorden amb la seva contrapartida declarada al «fstab» (error 390691)

### Sonnet

- El senyal fet és obsolet a favor de «spellCheckDone», que ara s'emet correctament

### Ressaltat de la sintaxi

- LaTeX: corregeix els parèntesis a diverses ordres (error 415384)
- TypeScript: afegeix el tipus de primitiva «bigint»
- Python: millora les paraules clau dels números, afegeix els octals, binaris i «breakpoint» (error 414996)
- SELinux: afegeix la paraula clau «glblub» i actualitza la llista de permisos
- Diverses millores a la definició de sintaxi del Gitolite

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
