---
aliases:
- ../announce-applications-17.04.0
changelog: true
date: 2017-04-20
description: KDE distribueix les aplicacions 17.04.0 del KDE
layout: application
title: KDE distribueix les aplicacions 17.04.0 del KDE
version: 17.04.0
---
20 d'abril de 2017. Les Aplicacions del KDE 17.04 ja són aquí. Globalment hem treballat per fer tant les aplicacions com les biblioteques subjacents més estables i més fàcils d'usar. Hem llimat les arestes i escoltat els vostres comentaris, hem fet que el conjunt de les Aplicacions del KDE sigui menys propens a errors i molt més fàcil d'emprar.

Gaudeix de les noves aplicacions!

#### <a href="https://edu.kde.org/kalgebra/">KAlgebra</a>

{{<figure src="/announcements/applications/17.04.0/kalgebra1704.jpg" width="600px" >}}

Els desenvolupadors del KAlgebra segueixen el seu propi camí cap a la convergència, i han adaptat la versió mòbil de l'extens programa educatiu al Kirigami 2.0, l'entorn preferit per integrar aplicacions del KDE a les plataformes d'escriptori i mòbils.

A més, la versió d'escriptori també ha migrat el dorsal 3D al GLES, el programari que permet que el programa renderitzi les funcions 3D tant en dispositius d'escriptori com mòbils. Això fa més el codi més senzill i fàcil de mantenir.

#### <a href="http://kdenlive.org/">Kdenlive</a>

{{<figure src="/announcements/applications/17.04.0/kdenlive1704.png" width="600px" >}}

L'editor de vídeo del KDE està esdevenint més estable i amb més funcionalitats en cada versió nova. Aquesta vegada, els desenvolupadors han redissenyat el diàleg de selecció de perfil per facilitar ajustar la mida de la pantalla, la velocitat dels fotogrames, i altres paràmetres del film.

Ara es pot reproduir el vídeo directament des de la notificació en acabar la renderització. S'han corregit diverses fallades que es produïen en moure els clips per la línia de temps, i s'ha millorat l'assistent dels DVD.

#### <a href="https://userbase.kde.org/Dolphin">Dolphin</a>

{{<figure src="/announcements/applications/17.04.0/dolphin1704.png" width="600px" >}}

L'explorador de fitxers preferit i portal a tot arreu (excepte potser a l'inframon) ha rebut diversos canvis d'imatge i millores d'usabilitat per fer-lo encara més potent.

Els menús contextuals del plafó <i>Llocs</i> (de manera predeterminada a l'esquerra de l'àrea de visió principal) s'han netejat, i ara és possible interactuar amb els ginys de metadades als consells d'eina. Per cert, ara els consells d'eina també funcionen en el Wayland.

#### <a href="https://www.kde.org/applications/ca/utilities/ark/">Ark</a>

{{<figure src="/announcements/applications/17.04.0/ark1704.png" width="600px" >}}

La popular aplicació gràfica per crear, descomprimir i gestionar arxius comprimits de fitxers i carpetes ara té una funció de <i>Cerca</i> per ajudar a trobar fitxers en arxius molt atapeïts.

També permet activar i desactivar connectors directament des del diàleg <i>Configuració</i>. Pel que fa als connectors, el connector Libzip nou millora la funcionalitat dels fitxers Zip.

#### <a href="https://minuet.kde.org/">Minuet</a>

{{<figure src="/announcements/applications/17.04.0/minuet1704.png" width="600px" >}}

Si esteu ensenyant o aprenent música, heu de donar una ullada al Minuet. La versió nova ofereix més exercicis d'escales i tasques d'entrenament de l'oïda per a les escales bebop, menor/major harmònica, pentatònica, i les simètriques.

També es poden definir o fer proves usant el <i>Mode de prova</i> nou per respondre exercicis. Podeu controlar el progrés executant una seqüència de 10 exercicis i obtindreu una estadística en acabar.

### I més!

L'<a href='https://okular.kde.org/'>Okular</a>, el visualitzador de documents del KDE, ha tingut com a mínim mitja dotzena de canvis que han afegit funcionalitats i millores a la seva usabilitat en pantalles tàctils. L'<a href='https://userbase.kde.org/Akonadi'>Akonadi</a> i diverses altres aplicacions que formen el <a href='https://www.kde.org/applications/office/kontact/'>Kontact</a> (el paquet ofimàtic del KDE amb correu/calendari/treball en grup) s'han revisat, depurat i optimitzat per a ajudar a ser més productiu.

El <a href='https://www.kde.org/applications/games/kajongg'>Kajongg</a>, el <a href='https://www.kde.org/applications/development/kcachegrind/'>KCachegrind</a> i més (<a href='https://community.kde.org/Applications/17.04_Release_Notes#Tarballs_that_were_based_on_kdelibs4_and_are_now_KF5_based'>Notes del llançament</a>) ara s'han adaptat als Frameworks 5 del KDE. Estem a l'espera dels vostres comentaris i opinions sobre les funcionalitats noves introduïdes en aquest llançament.

El <a href='https://userbase.kde.org/K3b'>K3b</a> s'ha incorporat al llançament de les Aplicacions del KDE.

### Cacera d'errors

S'han resolt més de 95 errors a les aplicacions, incloent-hi el Kopete, el KWalletManager, el Marble, l'Spectacle i més!

### Registre complet de canvis
