---
aliases:
- ../announce-applications-14.12.3
changelog: true
date: '2015-03-03'
description: Es distribueixen les aplicacions 14.12.3 del KDE.
layout: application
title: KDE distribueix les aplicacions 14.12.3 del KDE
version: 14.12.3
---
3 de març de 2015. Avui KDE distribueix la tercera actualització d'estabilització per a les <a href='../14.12.0'>aplicacions 14.12 del KDE</a>. Aquesta publicació només conté esmenes d'errors i actualitzacions de traduccions, proporcionant una actualització segura i millor per a tothom.

Hi ha 19 esmenes registrades d'errors, i també s'inclouen millores al joc d'anagrames Kanagram, el modelador UML Umbrello, el visualitzador de documents Okular i l'aplicació de geometria Kig.

Aquest llançament també inclou les versions de suport a llarg termini dels espais de treball Plasma 4.11.17, la plataforma de desenvolupament KDE 4.14.6 i el paquet Kontact 4.14.6.
