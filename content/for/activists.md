---
title: KDE for Activists
description: Fight the Power Using KDE Applications.
images:
- /for/activists/thumbnail.png
sassFiles:
- /scss/for/activists.scss
other:
 - name: Mobilizon
   description: Find, create, and organize events
   link: https://joinmobilizon.org/en/
   imgName: https://mobilizon.org/img/logo-mbz-yellow.svg
   img: mobilizon-logo.png
 - name: SecureDrop
   description: Share and accept documents securely as a whistleblower
   link: https://securedrop.org/
   img: securedrop-logo.svg
 - name: Tor
   description: Circumvent censorship on a secure network
   link: https://www.torproject.org/
   img: tor-logo.png
 - name: Briar
   description: Send peer-to-peer messages via Bluetooth, your local WiFi network, or Tor
   link: https://briarproject.org/
   img: briar-logo.png
 - name: PeerTube
   description: Host your own instance of this powerful federated video service, skirt government censorship and avoid spurious corporate take-downs.
   link: https://joinpeertube.org/
   img: peertube-logo.svg

cloud-img:
- name: 'KDE Connect on Android showing some features "send files", "multimedia control", "send clipboard"'
  url: kdeconnect.jpg
- name: There is no cloud, just other people's computers
  url: cloud.png
---

{{< container class="text-center text-small" >}}

## Developed by humans, for humans

KDE e.V. is a German non-profit foundation, and our software is primarily
developed by volunteers from all over the world. Unlike our competitors, we
don't seek to make money, but instead seek to make the world a better place. We
envision a world where monopolistic corporations and repressive governments
don't have too much power over our digital lives. We actually care about data
privacy and digital well-being, and we fight for them.

{{< /container >}}

{{< section class="eco py-5" >}}

<div class="row">
  <div class="col-12 col-lg-6">
    <h2 class="mt-0">KDE Eco</h2>
    <p class="text-left">KDE supports the FEEP and BE4FOSS programs to help free and open-source communities produce more environmentally friendly software. The projects provide communities with guidelines and support to optimize software to be more energy efficient and have a longer life-span.</p>
    <a href="https://eco.kde.org/" class="btn btn-outline-light btn-lg mx-auto">Learn more</a>
    <h3>Okular Awarded Blue Angel Ecolabel</h3>
    <p>In 2022, KDE's popular multi-platform PDF reader and universal document viewer Okular became the first ever Blue Angel eco-certified computer program! Blue Angel is the German version of the U.S. Energy Star label.</p>
    <a href="https://eco.kde.org/blog/2022-03-16-press-release-okular-blue-angel/" class="btn btn-outline-light btn-lg mx-auto" alt="">Learn more</a>
  </div>
  <div class="col-12 col-lg-6">
    <img src="okular.png" class="img-fluid kirigami-devices" alt="" />
  </div>
</div>

{{< /section >}}

{{< container class="text-small" >}}

## End to End Encryption

Everyone has a right to privacy, but for activists, ensuring the
privacy and security of communication can become a matter of life
and death.

### Keep your emails private with KMail

KDE's email client KMail provides world-class GnuPG integration, which makes
it easy to encrypt your emails. GnuPG is a global standard which was for example used
by [Edward Snowden to communicate with journalists](https://theintercept.com/2014/10/28/smuggling-snowden-secrets/).

![](kmail.png)

KDE also offers [Kleopatra](https://apps.kde.org/kleopatra/), an approachable app
to manage multiple cryptographic certificates.

{{< for/app-links download="https://kontact.kde.org/download" learn="https://kontact.kde.org/" >}}

### Keep your messages private with NeoChat

For real-time end-to-end encrypted communication, KDE has developed NeoChat, a
client for the [decentralized and secure messaging protocol:
Matrix](https://matrix.org). It's secure and contains many features that make
talking with your community a breeze, including support for multiple accounts,
stickers, emoji reactions, and more.

![](https://cdn.kde.org/screenshots/neochat/application.png)

The application is available on [Linux](https://flathub.org/apps/org.kde.neochat) and [Windows](https://apps.microsoft.com/store/detail/neochat/9PNXWVNRC29H).

{{< for/app-links learn="https://apps.kde.org/neochat">}}

### Keep your files private with Plasma Vault

Plasma Vault lets you add another layer of security by storing files and folders
in a super-secure encrypted container. Plasma Vault can optionally disable WiFi and
Bluetooth when unlocked, providing extra protection on an untrusted network.

Plasma Vault can also be used on top of [full disk encryption](https://en.wikipedia.org/wiki/Disk_encryption)
for even more security.

![](plasma-vault.png)

{{< /container >}}

{{< section class="privacy text-small py-5" >}}

## Privacy

Privacy is one of our core values at KDE. We don't track you, we don't collect any
data about you or your behavior, we don't show you personalized ads (or ads at all),
we don't throw cookies at you, and we don't sell any data to third parties.

As a general rule, software produced by the KDE Community does not transmit or
otherwise transfer information from end-users devices except as a result of an
explicit user action. Examples of this would include visiting webpages, sending
emails, and downloading new content (such as desktop wallpapers).

Some pieces of KDE software may include telemetry components, which provide details
to KDE about the device it's running on. Where this functionality exists, it is
always operated on an opt-in basis and disabled by default, with the ability
to change your preferences at any time and inspect the data it sends.

{{< for/app-links learn="https://kde.org/privacypolicy-apps/" >}}

{{< /section >}}

{{< section class="cloud py-5" >}}

## Local first

Synchronize your devices with [KDE Connect](https://kdeconnect.kde.org/) using
your local network instead of storing your data on the cloud, which consumes a
lot of energy and exposes your private data to the cloud provider.

{{< screenshots name="cloud-img" >}}

Alternatively, check out [Nextcloud](https://nextcloud.com/) which lets you
create your own cloud on a private personal server, ensuring that your data
never leaves your custody. Many pieces of KDE software integrate with
NextCloud.

{{< /section >}}

{{< section class="pb-5 mt-0 tokodon" >}}

## Spread the word with Tokodon

Tokodon is a client for [Mastodon](https://joinmastodon.org/), a social network
that is not for sale and not controlled by mercurial billionaires with
over-inflated egos.

{{< /section >}}

{{< section class="pb-3 pt-5" >}}

![](https://cdn.kde.org/screenshots/tokodon/tokodon-desktop.png)

Tokodon supports most of Mastodon's features, as well as multiple accounts
so you can manage your group and personal lives in one app.

Tokodon is also compatible with other fediverse servers like [Pixelfed](https://pixelfed.org/), [GoToSocial](https://gotosocial.org/), [Akkoma](https://akkoma.dev/AkkomaGang/akkoma/) and more.

{{< for/app-links learn="https://apps.kde.org/tokodon" >}}

{{< /section >}}

{{< container class="text-small" >}}

## More open-source applications for activists

Here are some other applications from other open-source organizations that
might interest you:

{{< link-boxes >}}

{{< /container >}}

