---
title: KDE for Developers
description: For Developers Using KDE Applications.
images:
- /for/developers/thumbnail.png
sassFiles:
- /scss/for/developers.scss
jsFiles:
- /js/for/developers.js
other:
 - name: Gammaray
   description: Inspect your Qt application
   link: https://www.kdab.com/development-resources/qt-tools/gammaray/
   img: gammaray-logo.png
 - name: Hotspot
   description: Performance analysis for Linux applications
   link: https://github.com/KDAB/hotspot
   img: hotspot-logo.png
 - name: Qt Creator
   description: C++ IDE
   link: https://www.qt.io/product/development-tools
   img: qt-logo.png
 - name: Valgrind
   description: Dynamic analysis tool
   link: https://valgrind.org/
   img: valgrind-logo.png

tools:
  title: 'By Programmers, for Programmers'
  description: 'The KDE community has created a huge number of [applications](https://apps.kde.org) over the years. Along the way, we built world-class tools to make it easy.'
---

{{< container class="text-center text-small" >}}

### Kate, the Feature-Packed Text Editor

Kate is packed with features that make it easier to view and edit all your text files. It lets you edit and view many files at the same time, both in tabs and split views, and comes with a wide variety of plugins, including an embedded terminal that lets you launch console commands directly from Kate, powerful search and replace plugins, and a preview plugin that can show you what your markdown, HTML and even SVG files will look like.

![Kate tools](/for/developers/kate.png)

{{< for/app-links download="https://kate-editor.org/get-it" learn="https://kate-editor.org" centered="true" >}}

{{< /container >}}

{{< section class="frameworks py-5" >}}

<div class="row">
  <div class="col-12 col-lg-6">
    <h2>KDE Frameworks</h2>
    <p class="text-left">KDE's Frameworks consist of over 80 add-on libraries to use in your Qt applications. For example, KWidgetsAddons includes many helpful and common widgets; KConfig provides configuration and state management; KCalendarCore and KContact provide easy-to-use .ics and vcard file parsers; Kirigami is a full toolkit for developing convergent user interfaces.</p>
    <a href="https://develop.kde.org/products/frameworks/" class="btn btn-outline-light btn-lg mx-auto">Learn more</a>
  </div>
  <div class="col-12 col-lg-6">
    <img src="https://develop.kde.org/frameworks/kirigami/tokodon-desktop.png" class="img-fluid kirigami-devices"/>
  </div>
</div>

{{< /section >}}

{{< container class="text-small" >}}

## Konsole

Konsole is KDE's terminal application. It provides tabs, split views, an ssh
connection manager, a way to bookmark your favorite commands and servers, thumbnails for
your files, Sixel support, and much more.

Konsole is also integrated into multiple other KDE Applications, making it
your constant companion. For example, KDevelop, Kate, Yakuake,
and Dolphin all use Konsole as an integrated terminal emulator.

{{< for/app-links download="https://konsole.kde.org/download.html" learn="https://konsole.kde.org/" >}}

{{< video src="/for/developers/konsole-sl.mp4" controls="false" autoplay="true" muted="true" loop="true" >}}

{{< /container >}}

{{< section class="yakuake pb-5 text-small text-center" >}}

![Yakuake](yakuake.png)

## Yakuake

Konsole but as a dropdown terminal, accessible from anywhere with a quick press of the <kdb>F12</kdb> key

{{< for/app-links learn="https://apps.kde.org/yakuake/" centered="true" >}}

{{< /section >}}

{{< section class="py-5" >}}

## Clazy

Clazy is a Clang compiler plugin to ensure that your Qt application follows industry-standard best practices.

```
[26/58] Building CXX object src/CMakeFiles/myapp.dir/global.cpp.o
src/global.cpp:18:88: warning: Use multi-arg instead [-Wclazy-qstring-arg]
    : m_config(KSharedConfig::openConfig(QStringLiteral("%1/%2")
    .arg(m_configFolderName).arg(m_configFileName)))

[34/58] Building CXX object src/CMakeFiles/myapp.dir/models/subtitlesfoldersmodel.cpp.o
src/myapp/src/models/subtitlesfoldersmodel.cpp:52:23:
warning: Use an empty QLatin1String instead of an empty QStringLiteral [-Wclazy-empty-qstringliteral]
    newList.removeAll(QStringLiteral(""));

2 warnings generated.
```

{{< for/app-links learn="https://invent.kde.org/sdk/clazy/-/blob/master/README.md" >}}

{{< /section >}}

{{< section class="py-5 kdevelop" >}}

## KDevelop

A cross-platform IDE for C, C++, Python, QML/JavaScript and PHP

KDevelop is a fully-featured Integrated Development Environment, perfect for C and C++ projects and other supported languages. It has great code completion and project support, along with documentation integration that keeps you close to where you're editing code.

{{< for/app-links learn="https://kdevelop.org/" >}}

{{< laptop src="https://kdevelop.org/images/kdevelop-window.webp" caption="Screenshot of kdevelop" class="w-100 mw-1k" >}}

{{< /section >}}

{{< section class="py-5" >}}

## Profile your application

Use [Heaptrack](https://apps.kde.org/heaptrack/) to profile your Linux application's memory allocation to find hotspots and memory leaks.

![Flame graph of memory allocation](gui_flamegraph.png)

And use [ELF Dissector](https://apps.kde.org/elf-dissector/) to investigate load time performance bottlenecks and profile the size of your Linux executables.

![Elf Dissector](elf-dissector-size-treemap.png)

{{< /section >}}

{{< container class="text-small" >}}

## Other open source applications for you

Here are some other open source applications from our partners to complement your workflow. Most of them are using [KDE Frameworks](https://develop.kde.org/products/frameworks/) internally.

And check out our other [development-related applications](https://apps.kde.org/categories/development).

{{< link-boxes >}}

{{< /container >}}

