---
title: KDE for Creators
description: For Artists Using KDE Applications.
layout: creators
images:
- /for/resources/forcreator-thumb.png
sassFiles:
- /scss/forcreators.scss
kritaArt:
  - url: https://krita-artists.org/uploads/default/optimized/3X/7/2/72a0a122cdfd2aedb8bb71a753bea9dfd781af1f_2_282x400.jpeg
    name: Summer portrait by RodMartin
    link: https://krita-artists.org/t/summer-portrait/46093
  - url: https://krita-artists.org/uploads/default/optimized/3X/e/5/e58457096aa76fb612531d7b361b518e73e6b72d_2_271x400.jpeg
    name: Page of Coins by Sylvia Ritter
    link: https://krita-artists.org/t/page-of-coins/45808
  - url: https://krita-artists.org/uploads/default/optimized/2X/c/c8aa75cc24e2ace3703222a4d343c96af0751570_2_667x1024.jpeg
    name: Artwork by RoseRedTiger
    link: https://krita-artists.org/t/a-series-of-elemental-dragons-in-different-rainbow-colors-in-no-particular-order/8165
  - url: https://krita-artists.org/uploads/default/optimized/3X/a/7/a7d3f7bc9c541b0db0d675d909454b962cd5f12c_2_348x400.jpeg
    name: Cordelia by MPetrelli
    link: https://krita-artists.org/t/cordelia/44565
  - url: https://krita-artists.org/uploads/default/optimized/3X/6/f/6fb9ffeb394d140e039199dc284fd1a454c1cadc_2_365x400.jpeg
    name: Artwork by Sonny
    link: https://krita-artists.org/t/this-is-another-piece-i-made-using-krita/40394
  - url: https://krita-artists.org/uploads/default/optimized/3X/5/3/53b8a24ba399021d27f3f04bc7f1c7ec57dd5603_2_282x400.jpeg
    name: Flying cats by emilm
    link: https://krita-artists.org/t/flying-cats/19702
  - url: https://krita-artists.org/uploads/default/optimized/3X/7/1/71e81cc6e22ebc19c675307b8206c2962a50c4b4_2_282x400.jpeg
    name: Future? by Mato
    link: https://krita-artists.org/t/future/33697
other:
 - name: Blender
   description: 3D creation
   link: https://www.blender.org/
   img: /for/resources/blender_icon_512x512.png
 - name: Inkscape
   description: Vector graphics
   link: https://inkscape.org/
   img: /for/resources/inkscape-logo.svg
 - name: Scribus
   description: Publishing
   link: https://www.scribus.net/
   img: /for/resources/Scribus_logo.svg
 - name: Friction
   description: Motion Graphics
   link: https://friction.graphics/
   img: /for/resources/friction.svg
 - name: Ossia score
   description: Media art
   link: https://ossia.io/
   img: /for/resources/score-logo.png
 - name: LMMS
   description: Music creation
   link: https://lmms.io/
   img: /for/resources/lmms-logo.svg
 - name: OpenToonz
   description: 2D animation
   link: https://opentoonz.github.io/
   img: /for/resources/opentoonz-logo.svg
 - name: Natron
   description: VFX and Motion Graphics
   link: https://natrongithub.github.io/
   img: /for/resources/natron.svg


krita:
  title: 'Krita - Digital Painting for Human Beings'
  description: 'No trials.<br>No subscriptions.<br>No paywalled colors.<br>No limit to your creativity.<br>'

digikam:
 - src: https://www.digikam.org/img/carousel/carousel-10.jpg?1661288336072
   description: Analyze color and exposure of images using histograms
 - src: https://www.digikam.org/img/carousel/carousel-04.jpg?1661288336070
   description: Quickly find your best images using labels

plasma:
 - src: /for/resources/color-picker.png
   name: Color picker applet
   description: Capture color from your screen and keep track of them

 - src: /for/resources/krita-dark-theme.png
   name: Neutral color support
   description: Plasma allows you to use a desaturated dark theme.

 - src: /for/resources/preview-dolphin.png
   name: Preview support for all your images
   description: Preview your artworks Krita files directly from the file manager and image viewer
---

{{< container class="text-center text-small" >}}

Krita is a professional free and open source program for painters and graphic designers. It is made by artists who work to provide open, affordable and privacy-respecting art tools for everyone. With Krita, you can develop concept art, use texture and matte painting tools, and create high quality illustrations and comics.

![Krita tools](/for/resources/pepperandcarrot.png)

### Join the community

Join the [krita-artists.org](https://krita-artists.org) community to share your art with other artists.

{{< /container >}}

{{< screenshots name="kritaArt" centered="false" >}}

{{< container class="text-small" >}}

## Kdenlive

Kdenlive allows you to use and arrange several audio and video tracks, each one can be locked or muted to your convenience.

{{< for/app-links download="https://kdenlive.org/download" fund="https://kdenlive.org/fund" learn="https://kdenlive.org/" >}}

{{< /container >}}

{{< video src="/for/resources/kdenlive.mp4" controls="false" autoplay="true" muted="true" loop="true" >}}

{{< container class="text-small" >}}

### Use any audio / video format

Being based on the powerful FFmpeg libraries, Kdenlive can use almost any audio and video formats directly without the need to convert or re-encode your clips.

### Configurable interface and shortcuts

You can arrange and save your custom interface layouts to fit your workflow.
Keyboard shortcuts can also be configured to match your preferences.

{{< /container >}}

{{< container class="text-small" >}}

<img alt="" src="https://glaxnimate.org/images/logo.svg" width=128 height=128 />

## Glaxnimate

Glaxnimate is an open-source vector animation and motion design desktop application.

![](https://glaxnimate.org/images/screenshots/main_window/main_window.png)

{{< for/app-links download="https://glaxnimate.org/download" learn="https://glaxnimate.org/" >}}

{{< /container >}}

{{< section class="digikam py-5 text-small text-center" >}}

## Digikam

Professional Photo Management with the Power of Open Source

{{< for/app-links download="https://www.digikam.org/download" fund="https://www.digikam.org/donate" learn="https://www.digikam.org" centered="true" >}}

{{< laptop src="/for/resources/digikam.png" caption="Digikam support of raw files" class="w-100 mw-1k" >}}

{{< for/feature features="digikam" >}}

{{< /section >}}

{{< section class="plasma py-5" >}}

## KDE Plasma Desktop for Artists

[KDE Plasma](/plasma-desktop) is the ideal operating system for you with native support for drawing tablets and many small features tailored for artists.

{{< for/app-links download="/distributions" learn="/plasma-desktop" dark="true" >}}

{{< for/feature-row features="plasma" >}}

> It is mainly about more options for the tablet settings, options to set up a neutral colored theme, thumbnails for your artwork source files and easier file type association and 'open with' menu, better support for multi monitor workflow and perfect desktop integration with my core application: Krita and Kdenlive.

[David Revoy, Fedora 36 KDE Spin for a digital painting workstation](https://www.davidrevoy.com/article913/fedora-36-kde-spin-for-a-digital-painting-workstation-reasons-and-post-install-guide)


{{< /section >}}

{{< section class="kontrast py-5" >}}

## Kontrast

Make your design accessible, by making sure that the contrast is WCAG compliant.

{{< for/app-links download="https://apps.kde.org/kontrast/" learn="https://apps.kde.org/kontrast/" dark="true" >}}

![Screenshot of Kontrast with a blue background and white foreground](/for/resources/kontrast.png)

{{< /section >}}

{{< container class="text-small" >}}

## Other open source applications for you

Here are some other applications from other open source organizations to complement your workflow.
You should also check out our other [graphic related applications](https://apps.kde.org/categories/graphics).

{{< link-boxes >}}

{{< /container >}}

