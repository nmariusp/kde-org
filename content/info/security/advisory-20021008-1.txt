-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1


KDE Security Advisory: KGhostview Arbitary Code Execution
Original Release Date: 2002-10-08
URL: http://www.kde.org/info/security/advisory-20021008-1.txt

0. References

        cve.mitre.org: CAN-2002-0838
        BUGTRAQ:20020926 iDEFENSE Security Advisory 09.26.2002: 
        Exploitable Buffer Overflow in gv
        http://marc.theaimsgroup.com/?l=bugtraq&m=103305615613319&w=2


1. Systems affected:

        KGhostView of any KDE release between KDE 1.1 and KDE 3.0.3a

2. Overview:
            
        KGhostview includes a DSC 3.0 parser from GSview, which is vulnerable
        to a buffer overflow while parsing a specially crafted .ps input
        file. It also contains code from gv 3.5.x which is vulnerable to another 
        buffer overflow triggered by malformed postscript or Adobe pdf files. 

3. Impact:
        
        Viewing certain Postscript or PDF files can result in the execution of 
        arbitary code placed in the file and as a result opens possibilities for
        any remote manipulation under the local user account.
   
4. Solution:
        
        Apply the patch listed in section 5 to kdegraphics/kghostview, or update
        to KDE 3.0.4. 

        kdegraphics-3.0.4 can be downloaded from

        http://download.kde.org/stable/3.0.4 :

        6065219c825102c843ba582c4a520cac  kdegraphics-3.0.4.tar.bz2

5. Patch:

        A patch for KDE 3.0.3 is available from
        
        ftp://ftp.kde.org/pub/kde/security_patches :
        9e33962406ac123e4fbdab20b4123ccf  post-3.0.3-kdegraphics-kghostview.diff
  
        A patch for KDE 2.2.2 is available from
   
        ftp://ftp.kde.org/pub/kde/security_patches : 
        62a1178c6a1730cbab98bbc825adafe9  post-2.2.2-kdegraphics-kghostview.diff
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.0.7 (GNU/Linux)

iD8DBQE9pDXDvsXr+iuy1UoRAvfZAKCxyetx90FfIDpTeq028QUEfXM6TwCgjOMl
pLaRHeMmf/kUDz9HwpOW6fk=
=w/u0
-----END PGP SIGNATURE-----
