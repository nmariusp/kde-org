<li>Konqueror in KDE SC versions up to and including 4.6.1 have a cross-site
scripting error that can be exploited to cause a user to be redirected to
a malicious site.
Read the <a href="/info/security/advisory-20110411-1.txt">detailed advisory</a>
for more information.
</li>
