-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

KDE Security Advisory: Konqueror Frame Injection Vulnerability
Original Release Date: 2004-08-11
URL: http://www.kde.org/info/security/advisory-20040811-3.txt

0. References

        http://cve.mitre.org/cgi-bin/cvename.cgi?name=CAN-2004-0721
        http://secunia.com/advisories/11978/
        http://www.heise.de/newsticker/meldung/48793
        http://bugs.kde.org/show_bug.cgi?id=84352

1. Systems affected:

        All versions of KDE up to KDE 3.2.3 inclusive. 


2. Overview:

        The Konqueror webbrowser allows websites to load webpages into
        a frame of any other frame-based webpage that the user may have open.

        The Common Vulnerabilities and Exposures project (cve.mitre.org)
        has assigned the name CAN-2004-0721 to this issue.


3. Impact:

        A malicious website could abuse Konqueror to insert its own frames
        into the page of an otherwise trusted website. As a result the user
        may unknowingly send confidential information intended for the
        trusted website to the malicious website.
                

4. Solution:

        Source code patches have been made available which fix these
        vulnerabilities. Contact your OS vendor / binary package provider
        for information about how to obtain updated binary packages.


5. Patch:

        Patches for KDE 3.0.5b are available from
        ftp://ftp.kde.org/pub/kde/security_patches : 

  aa3ac08a45851a1c33b2fcd435e1d514  post-3.0.5b-kdelibs-htmlframes.patch
  dc4dfff2df75d19e527368f56dc92abb  post-3.0.5b-kdebase-htmlframes.patch

        Patches for KDE 3.1.5 are available from
        ftp://ftp.kde.org/pub/kde/security_patches : 

  e6cebe1f93f7497d720018362077dcf7  post-3.1.5-kdelibs-htmlframes.patch
  caa562da0735deacba3ae9170f2bf18f  post-3.1.5-kdebase-htmlframes.patch

        Patches for KDE 3.2.3 are available from
        ftp://ftp.kde.org/pub/kde/security_patches : 

  8384f2785295be7082d9984ba8e175eb  post-3.2.3-kdelibs-htmlframes.patch
  a60fd1628607d4abdeb930662d126171  post-3.2.3-kdebase-htmlframes.patch


6. Time line and credits:


        01/07/2004 Secunia publishes security advisory
	04/08/2004 Patches created
	05/08/2004 Vendors notified
        11/08/2004 Public advisory

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.2.2 (GNU/Linux)

iD8DBQFBGioxN4pvrENfboIRAi+mAJ0WMjHog9VRHoDpPodNCwV0RhR0UQCeMNE/
hjSS3bG2/H6ZeaD2VSm9hoI=
=YE7B
-----END PGP SIGNATURE-----
