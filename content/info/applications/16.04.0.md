---
title: "KDE Applications 16.04.0 Info Page"
announcement: /announcements/announce-applications-16.04.0
layout: applications
build_instructions: https://techbase.kde.org/Getting_Started/Build/Historic/KDE4
---
