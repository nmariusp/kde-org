<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top">
  <th align="left">Location</th>
  <th align="left">Size</th>
  <th align="left">MD5&nbsp;Sum</th>
</tr>
<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.3.92/src/arts-1.3.92.tar.bz2">arts-1.3.92</a></td>
   <td align="right">963kB</td>
   <td><tt>cc7dd53ca5499e370316179d5942d663</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.3.92/src/kdeaccessibility-3.3.92.tar.bz2">kdeaccessibility-3.3.92</a></td>
   <td align="right">6.4MB</td>
   <td><tt>d81bd9b7406a6c1e4325906a0af5b885</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.3.92/src/kdeaddons-3.3.92.tar.bz2">kdeaddons-3.3.92</a></td>
   <td align="right">1.4MB</td>
   <td><tt>e7eb91b5605a5660a8bbf8bfe00458db</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.3.92/src/kdeadmin-3.3.92.tar.bz2">kdeadmin-3.3.92</a></td>
   <td align="right">1.4MB</td>
   <td><tt>b692994bc3fd68ecf0fdb60474d7beb0</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.3.92/src/kdeartwork-3.3.92.tar.bz2">kdeartwork-3.3.92</a></td>
   <td align="right">17MB</td>
   <td><tt>abb464c358126e389af6c744e4770010</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.3.92/src/kdebase-3.3.92.tar.bz2">kdebase-3.3.92</a></td>
   <td align="right">21MB</td>
   <td><tt>67e33c1060229856655bc8c613e4544d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.3.92/src/kdebindings-3.3.92.tar.bz2">kdebindings-3.3.92</a></td>
   <td align="right">6.7MB</td>
   <td><tt>d2db3f4855dc0cdba9f55848551f9c9e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.3.92/src/kdeedu-3.3.92.tar.bz2">kdeedu-3.3.92</a></td>
   <td align="right">22MB</td>
   <td><tt>f2d08ae4d295314be22df40f7343c64b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.3.92/src/kdegames-3.3.92.tar.bz2">kdegames-3.3.92</a></td>
   <td align="right">8.9MB</td>
   <td><tt>2bafe82aa77a58b00005b9640f5b9eec</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.3.92/src/kdegraphics-3.3.92.tar.bz2">kdegraphics-3.3.92</a></td>
   <td align="right">6.2MB</td>
   <td><tt>70270e2a8e7926001728986e33fd0d5c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.3.92/src/kde-i18n-3.3.92.tar.bz2">kde-i18n-3.3.92</a></td>
   <td align="right">244MB</td>
   <td><tt>03efce4a0f4b7a5b33e7936f4487ba0a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.3.92/src/kdelibs-3.3.92.tar.bz2">kdelibs-3.3.92</a></td>
   <td align="right">15MB</td>
   <td><tt>876196a4f16a5cbc2da5f4f8557d138a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.3.92/src/kdemultimedia-3.3.92.tar.bz2">kdemultimedia-3.3.92</a></td>
   <td align="right">5.2MB</td>
   <td><tt>cf278fa074fa17695eb5f7045e7b1325</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.3.92/src/kdenetwork-3.3.92.tar.bz2">kdenetwork-3.3.92</a></td>
   <td align="right">6.8MB</td>
   <td><tt>11128438407210ca58687ba183e0ab30</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.3.92/src/kdepim-3.3.92.tar.bz2">kdepim-3.3.92</a></td>
   <td align="right">10MB</td>
   <td><tt>6cfa0e0e86e00c48d3af0e08ffe0aae3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.3.92/src/kdesdk-3.3.92.tar.bz2">kdesdk-3.3.92</a></td>
   <td align="right">4.3MB</td>
   <td><tt>7bd1ffb6e6526bd978e87deea8cf1923</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.3.92/src/kdetoys-3.3.92.tar.bz2">kdetoys-3.3.92</a></td>
   <td align="right">3.0MB</td>
   <td><tt>53cf71381e8bba1b543d3540253840a5</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.3.92/src/kdeutils-3.3.92.tar.bz2">kdeutils-3.3.92</a></td>
   <td align="right">2.1MB</td>
   <td><tt>4638a8f5bac946148b636540dc3199b3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.3.92/src/kdevelop-3.1.92.tar.bz2">kdevelop-3.1.92</a></td>
   <td align="right">7.8MB</td>
   <td><tt>8280848d079b6cd60c9e3c5e870234da</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.3.92/src/kdewebdev-3.3.92.tar.bz2">kdewebdev-3.3.92</a></td>
   <td align="right">5.8MB</td>
   <td><tt>dc7994049ea7f04b21fdf6f4fb71a618</tt></td>
</tr>

</table>
