    <table border="0" cellpadding="4" cellspacing="0">
      <tr valign="top">
        <th align="left">Location</th>

        <th align="left">Size</th>

        <th align="left">MD5&nbsp;Sum</th>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.5b/src/arts-1.1.tar.bz2">arts-1.1</a></td>

        <td align="right">1001KB</td>

        <td><tt>f0459975c08bcc6456a4d88d6f224e80</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.5b/src/kdeaddons-3.0.5b.tar.bz2">kdeaddons-3.0.5b</a></td>

        <td align="right">913KB</td>

        <td><tt>3aeab87659d418821194ef45f76302ab</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.5b/src/kdeadmin-3.0.5b.tar.bz2">kdeadmin-3.0.5b</a></td>

        <td align="right">1.3MB</td>

        <td><tt>2adcfec8bbca78dd40297fc184fd3f28</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.5b/src/kdeartwork-3.0.5b.tar.bz2">kdeartwork-3.0.5b</a></td>

        <td align="right">11MB</td>

        <td><tt>ba42fc6bc6e61223cbaee0168bc72f17</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.5b/src/kdebase-3.0.5b.tar.bz2">kdebase-3.0.5b</a></td>

        <td align="right">13MB</td>

        <td><tt>608f457dd8157c69bf855092e516afa2</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.5b/src/kdebindings-3.0.5b.tar.bz2">kdebindings-3.0.5b</a></td>

        <td align="right">4.8MB</td>

        <td><tt>3b2be6852011e17afd09d5f2b0bc0692</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.5b/src/kdeedu-3.0.5b.tar.bz2">kdeedu-3.0.5b</a></td>

        <td align="right">8.7MB</td>

        <td><tt>2903e02601c5b8ac8eaf4d52757da03c</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.5b/src/kdegames-3.0.5b.tar.bz2">kdegames-3.0.5b</a></td>

        <td align="right">7.0MB</td>

        <td><tt>d436bd70d9eefd57ed176268e3b1ab00</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.5b/src/kdegraphics-3.0.5b.tar.bz2">kdegraphics-3.0.5b</a></td>

        <td align="right">2.6MB</td>

        <td><tt>e9e894e173a144b0ba562fd3065a6c20</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.5b/src/kdelibs-3.0.5b.tar.bz2">kdelibs-3.0.5b</a></td>

        <td align="right">7.3MB</td>

        <td><tt>50b483665bc868f2dbc53aaaa3c2f302</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.5b/src/kdemultimedia-3.0.5b.tar.bz2">kdemultimedia-3.0.5b</a></td>

        <td align="right">5.5MB</td>

        <td><tt>ae4178f6c2eead819174f28addd0c684</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.5b/src/kdenetwork-3.0.5b.tar.bz2">kdenetwork-3.0.5b</a></td>

        <td align="right">3.7MB</td>

        <td><tt>56735b0933c28e4b6db41af72f36c703</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.5b/src/kdepim-3.0.5b.tar.bz2">kdepim-3.0.5b</a></td>

        <td align="right">3.1MB</td>

        <td><tt>8899db7bccc7369144a8cd3e4ef21f66</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.5b/src/kdesdk-3.0.5b.tar.bz2">kdesdk-3.0.5b</a></td>

        <td align="right">1.8MB</td>

        <td><tt>9c6eaf26033b16de618a5cca9f41fdfa</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.5b/src/kdetoys-3.0.5b.tar.bz2">kdetoys-3.0.5b</a></td>

        <td align="right">1.4MB</td>

        <td><tt>c548485e7551937df14a60d8c2dab1b4</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.5b/src/kdeutils-3.0.5b.tar.bz2">kdeutils-3.0.5b</a></td>

        <td align="right">1.5MB</td>

        <td><tt>4e3b780ed2dc9724214e1c3dcc8d7d4c</tt></td>
      </tr>
    </table>

