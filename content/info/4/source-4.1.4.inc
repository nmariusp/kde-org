<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top"><th align="left">Location</th><th align="left">Size</th><th align="left">MD5&nbsp;Sum</th></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.4/src/kdeaccessibility-4.1.4.tar.bz2">kdeaccessibility-4.1.4</a></td><td align="right">6,1MB</td><td><tt>da6c157741d73eb1445cd60d9b78cd2e</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.4/src/kdeadmin-4.1.4.tar.bz2">kdeadmin-4.1.4</a></td><td align="right">1,8MB</td><td><tt>c3194bc30daacae33322993f2dba0074</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.4/src/kdeartwork-4.1.4.tar.bz2">kdeartwork-4.1.4</a></td><td align="right">41MB</td><td><tt>6b53365e45fdcd227e7e7c546e25cb36</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.4/src/kdebase-4.1.4.tar.bz2">kdebase-4.1.4</a></td><td align="right">4,3MB</td><td><tt>9a76438f5453a3ba4f773db6847825f7</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.4/src/kdebase-runtime-4.1.4.tar.bz2">kdebase-runtime-4.1.4</a></td><td align="right">50MB</td><td><tt>fad82875745bacbb52001eabf9c71202</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.4/src/kdebase-workspace-4.1.4.tar.bz2">kdebase-workspace-4.1.4</a></td><td align="right">46MB</td><td><tt>bdac9ac8055b976845587fe544d568be</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.4/src/kdebindings-4.1.4.tar.bz2">kdebindings-4.1.4</a></td><td align="right">4,6MB</td><td><tt>d884a819f161fb17e66462dc4bbfeedc</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.4/src/kdeedu-4.1.4.tar.bz2">kdeedu-4.1.4</a></td><td align="right">55MB</td><td><tt>28840fa475571184994824d12a0b7684</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.4/src/kdegames-4.1.4.tar.bz2">kdegames-4.1.4</a></td><td align="right">31MB</td><td><tt>b18b4b89c83ea20a76bab5fac03bcc0b</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.4/src/kdegraphics-4.1.4.tar.bz2">kdegraphics-4.1.4</a></td><td align="right">3,3MB</td><td><tt>c2a67098d9df60e839d2af6b866989ef</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.4/src/kdelibs-4.1.4.tar.bz2">kdelibs-4.1.4</a></td><td align="right">8,8MB</td><td><tt>ffdb0340a72aaf06a0b98f354c0c59ef</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.4/src/kdemultimedia-4.1.4.tar.bz2">kdemultimedia-4.1.4</a></td><td align="right">1,4MB</td><td><tt>4a4e046f0b862e7be9e916e0e29ed188</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.4/src/kdenetwork-4.1.4.tar.bz2">kdenetwork-4.1.4</a></td><td align="right">7,1MB</td><td><tt>c67114c0e28a4e44f1466bf467a7b8dd</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.4/src/kdepim-4.1.4.tar.bz2">kdepim-4.1.4</a></td><td align="right">13MB</td><td><tt>297ffe93343bc0dd62c7d90feae0ee6c</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.4/src/kdepimlibs-4.1.4.tar.bz2">kdepimlibs-4.1.4</a></td><td align="right">1,9MB</td><td><tt>9546430309b1f34707bffdceec0fbc4b</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.4/src/kdeplasma-addons-4.1.4.tar.bz2">kdeplasma-addons-4.1.4</a></td><td align="right">3,9MB</td><td><tt>4a0df1a79854539a4022df2308629dc9</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.4/src/kdesdk-4.1.4.tar.bz2">kdesdk-4.1.4</a></td><td align="right">4,8MB</td><td><tt>1ca874a1d239018472bf27d0f2b98feb</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.4/src/kdetoys-4.1.4.tar.bz2">kdetoys-4.1.4</a></td><td align="right">1,3MB</td><td><tt>b32cfc4788e749e6240476783f0eb1a9</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.4/src/kdeutils-4.1.4.tar.bz2">kdeutils-4.1.4</a></td><td align="right">2,2MB</td><td><tt>542663eeae777978dafab2dcf9d422c2</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.4/src/kdewebdev-4.1.4.tar.bz2">kdewebdev-4.1.4</a></td><td align="right">2,5MB</td><td><tt>7a55f117e45a5022160926fc5a1defb0</tt></td></tr>
</table>
