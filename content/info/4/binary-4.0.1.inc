<ul>
<!--
       <li>
       An alpha version of KDE4-based <strong>Arklinux 2008.1</strong> is expected
       shortly after this release, with an expected final release within 3 or 4 weeks.
    </li>
-->
       <li>
       <strong>Debian</strong> KDE 4.0.1 packages are available in the experimental branch.
       The KDE Development Platform is in <em>Sid</em>. Watch for
       announcements by the <a href="http://pkg-kde.alioth.debian.org/">Debian KDE Team</a>.
       </li>
<!--
       <li>
       <strong>Fedora</strong> will feature KDE 4.0.1 in Fedora 9, to be <a
       href="http://fedoraproject.org/wiki/Releases/9">released</a>
       in April, with Alpha releases being available from
       24th of January.  KDE 4.0.1 packages are in the pre-alpha <a
       href="http://fedoraproject.org/wiki/Releases/Rawhide">Rawhide</a> repository.
       </li>
       <li>
       <strong>Gentoo Linux</strong> provides KDE 4.0.1 builds on
       <a href="http://kde.gentoo.org">http://kde.gentoo.org</a>.
       </li>
-->
       <li>
                <strong>Kubuntu</strong> packages are included in the upcoming "Hardy Heron"
                (8.04) and also made available as updates for the stable "Gutsy Gibbon" (7.10).
                More details can be found in  the <a href="http://kubuntu.org/announcements/kde-4.0.1.php">
                announcement on Kubuntu.org</a>.
        </li>
<!--
        <li>
                <strong>Mandriva</strong> will provide packages for 
                <a href="http://download.kde.org/binarydownload.html?url=/stable/4.0.1/Mandriva/">2008.0</a> and aims
                at producing a Live CD with the latest snapshot of 2008.1.
        </li>a
	-->
        <li>
                <strong>openSUSE</strong> packages <a href="http://en.opensuse.org/KDE/KDE4">are available</a> 
                for openSUSE 10.3 
                (<a href="http://download.opensuse.org/repositories/KDE:/KDE4:/STABLE:/Desktop/openSUSE_10.3/KDE4-BASIS.ymp">one-click 
                install</a>), openSUSE Factory 
                (<a href="http://download.opensuse.org/repositories/KDE:/KDE4:/STABLE:/Desktop/openSUSE_Factory/KDE4-BASIS.ymp">one-click 
                install</a>) and openSUSE 10.2. A <a href="http://home.kde.org/~binner/kde-four-live/">KDE 
                Four Live CD</a> with these packages is also available. KDE 4.0.1 will be part of the upcoming 
                openSUSE 11.0 release.
        </li>
</ul>
