---
description: KDE Gear ⚙️ 23.04 adds new apps, and brings improvements and features to dozens of your favorite tools
authors:
  - SPDX-FileCopyrightText: 2023 Paul Brown <paul.brown@kde.org>
SPDX-License-Identifier: CC-BY-4.0
date: 2023-04-20
hero_image: hero.png
images:
  - /announcements/gear/23.04.0/Hero.webp
layout: gear
noContainer: true
scssFiles:
- /scss/gear-23-04.scss
outro_img:
  link: many_apps.png
  alt: Screenshots of many applications
title: "KDE Gear 23.04"
subtitle: 🛸 New Arrivals 👽
---

{{< container class="intro" >}}

New apps join KDE Gear ⚙️ 23.04 and unlock access to fledgling social media platforms, online video streaming services, podcasts, and much more. This new generation of apps are designed to work both on your computer and mobile phone right out of the box.

But, of course, if you are also looking forward to new versions of the KDE tools you already know and trust, those are here too! Classics like Spectacle, Dolphin, Kdenlive, Kate, and Okular all boast shiny new features, code optimizations, and performance improvements.

{{< announcements/donorbox >}}

Read on to find out what's new and what has been improved in KDE Gear ⚙️ 23.04:

{{< /container >}}

{{< section class="tokodon py-5" >}}

{{< figure src="https://apps.kde.org/app-icons/org.kde.tokodon.svg" height="96" width=96 >}}

## [Tokodon](https://apps.kde.org/tokodon/)

**Tokodon** brings the Mastodon federated social media platform to your fingertips, allowing you to easily read, post, and message.

With version 23.04, communicating with your fellow Fediverse users is even easier, thanks to new features like the ability to see previous messages when replying and the ability to send polls to your followers. Tokodon's mobile version also features a dedicated search page to streamline the process of finding specific posts.

![Tokodon poll](tokodon.png)

Furthermore, version 23.04 enhances your privacy by allowing you to configure a proxy before logging into your Mastodon account. You can also view a list of follow requests, giving you more control over who you interact with on the Fediverse.

{{< /section >}}

{{< section class="audiotube py-5" >}}

![](https://apps.kde.org/app-icons/org.kde.audiotube.svg)

## [AudioTube](https://apps.kde.org/audiotube/)

**AudioTube** brings all the music on YouTube to your desktop. This elegant and minimalistic player has undergone a visual redesign and also sports a smart new icon, as well as improved compatibility on mobile phones.

You can search for music, make playlists, create playlists from your favorite songs, all songs from the playback history, or your most listened songs. You can also share links to your songs with others.

![Audiotube](https://plasma-mobile.org/2023/03/29/this-month-plasma-mobile/audiotube-playlists-MPRIS.png)

Finally, a major rewrite of parts of the code has made AudioTube lighter than ever.

{{< /section >}}

{{< container >}}

![](https://apps.kde.org/app-icons/org.kde.neochat.svg)

## [NeoChat](https://apps.kde.org/neochat/)

**NeoChat** is a native KDE app for chatting on Matrix. NeoChat improves its design with tweaks that provide a more compact layout and a simpler menu which works better for the collapsed room list.

We have also improved the video controls, added a new command `/knock <room-id>` to send a *knock* event to a room, and  you can now edit a prior message inline, within the chat pane.

Other usability improvements include an overhaul of the keyboard navigation and shortcuts like <kbd>Ctrl</kbd>+<kbd>PgUp</kbd>/<kbd>PgDn</kbd> that allow you to skip from room to room.

{{< video src="neochat.mp4" ratio="0.84" loop="true" muted="true" autoplay="true" controls="false" width="1048" height="874" >}}

{{< /container >}}


{{< diagonal-box color="purple" logo="https://apps.kde.org/app-icons/org.kde.spectacle.svg" >}}

## [Spectacle](https://apps.kde.org/spectacle/)

**Spectacle** is KDE's screenshot and screen recorder app. This release features a total redesign of the user interface to make everything faster and easier to use. As part of this redesign, it's now possible to annotate screenshots directly from the rectangular region selection.

![Spectacle rectangular selection](spectacle.png)

In addition, Spectacle now offers built-in screen recording functionality on Wayland!

![Spectacle new ui](spectacle-new-ui.png)

{{< /diagonal-box >}}

{{< container >}}

![](https://apps.kde.org/app-icons/org.kde.dolphin.svg)

## [Dolphin](https://apps.kde.org/dolphin/)

**Dolphin** is KDE's versatile and powerful file browser that not only lets you explore your local file system, but also remote servers and devices on the local network.

In the area of file security and privileges, Dolphin now lets you configure how permissions are shown in the Details view. Dolphin has also become more flexible and now supports browsing Apple iOS devices using its native `afc://` protocol, file dialogs, and other file management tools. It can display the number of pages in a document in a metadata display, too.

A much-requested feature is the possibility to run Dolphin in superuser mode, giving you full command of the entire file system. As this is quite dangerous, Dolphin does not allow itself to be run with `sudo` and tells you as much if you try. In this release, Dolphin will now also tells you how you can accomplish this goal in a fully supported matter using the new `kio-admin` add-on.

![Open as administrator context menu](dolphin-admin.png)

Despite these added features, Dolphin has not become slower. In fact, its code for counting directory sizes is faster, which improves performance, especially with manually-mounted network shares that aren't detected as such.

{{< /container >}}

{{< section class="tokodon py-5" >}}

![](https://apps.kde.org/app-icons/org.kde.gwenview.svg)

## [Gwenview](https://apps.kde.org/gwenview/)

**Gwenview** lets you view your images and do some basic editing and annotations on them.

When running on Plasma Wayland, you can now zoom in and out on images in Gwenview using pinch gestures on your touchpad!

Gwenview 23.04 includes changes that make it more predictable, like inhibiting sleep and screen locking during a slideshow only if it is in the foreground, or zooming smoothly rather than in steps when you use <kbd>Ctrl</kbd> + scroll on a touchpad. Finally, we have fixed Gwenview's most commonly-experienced crash, often seen when rotating images quickly.

{{< /section >}}

{{< section class="elisa py-5" >}}

![](https://apps.kde.org/app-icons/org.kde.elisa.svg)

## [Elisa](https://elisa.kde.org/)

**Elisa** is a smart and elegant music player available for both your computer and phone on most major platforms.

In this release, Elisa allows you to collapse the large header area for a more streamlined appearance, if that's what you prefer. And now the *Frequently Played* view is a simple list of songs arranged by play count, doing away with the prior complicated time-based heuristic that was unclear and made the contents look random.

{{< video src="elisa.mp4" loop="true" muted="true" autoplay="true" controls="false" width="1308" height="950" >}}

As for playlists, Elisa now supports creating and opening `.pls` playlist files, and when you ask Elisa to open a playlist file with invalid paths, it skips them, shows you a message explaining what happened, and offers you the possibility to open the file so you can edit it and fix the broken paths.

If listening to the radio is your jam, Elisa now includes more popular stations by default.

{{< /section >}}

{{< container >}}

![](https://apps.kde.org/app-icons/org.kde.okular.svg)

## [Okular](https://apps.kde.org/okular/)

**Okular** is KDE's feature-rich document viewer.

Following the trend of making KDE apps easier to use without sacrificing their flexibility, Okular's default toolbar layout has been tweaked for greater usability and relevance: now it includes  the View Mode menu and the zoom and view buttons on the left side, with the tools on the right side. Additionally, the sidebar itself can now be relocated to either side of the window or un-docked to become a free-floating window.

{{< video src="okular.mp4" loop="true" muted="true" autoplay="true" controls="false" width="1128" >}}

Another subtle tweak that makes using Okular more pleasurable is the zoom that now works smoothly rather than in steps when you use <kbd>Ctrl</kbd> + the scroll on your touchpad or a high-resolution scroll wheel.

{{< /container >}}

{{< section class="tokodon py-5" >}}

![](https://apps.kde.org/app-icons/org.kde.filelight.svg)

## [Filelight](https://apps.kde.org/filelight)

**[Filelight](https://apps.kde.org/filelight/)** gives you a clear and easy-to-understand overview of your disk space and usage. Version 23.04 comes with a list view on the left side of the window, providing a simple text-based method of viewing size information.

![Filelight new sidebar](filelight.png)

{{< /section >}}

{{< section class="kdenlive py-5" >}}

![](https://apps.kde.org/app-icons/org.kde.kdenlive.svg)

## [Kdenlive](https://kdenlive.org)

The big news for KDE's powerful video editor is support for *nested timelines.* This new feature allows you to select several clips from your tracks, group them together, and then treat the group as one single sequence.

You can edit the sequence, apply effects, and add transitions between nested and regular clips. You can later modify the nested sequence and the whole will update accordingly, making editing even more efficient and flexible.

{{< video src="kdenlive_timelines.mp4" loop="true" muted="true" autoplay="true" controls="false" width="1920" >}}

{{< /section >}}

{{< section class="kalendar py-5" >}}

![](https://apps.kde.org/app-icons/org.kde.kalendar.svg)

## [Kalendar](https://apps.kde.org/kalendar)

**[Kalendar](https://apps.kde.org/kalendar/)** is a modern and highly interactive calendaring and contact management app that works well on your computer and mobile phone. Kalendar has completely re-vamped its address book which is now more functional and easier to use.

![Calendar contact book redesign](kalendar.png)

Additionally, it is now possible to define custom reminder times.

{{< /section >}}

{{< section class="plasmatube py-5" >}}

![](https://apps.kde.org/app-icons/org.kde.plasmatube.svg)

## [PlasmaTube](https://apps.kde.org/plasmatube)

**[PlasmaTube](https://apps.kde.org/plasmatube/)** is a video player that allows you to stream YouTube videos directly to your desktop or phone. Apart from overhauling its looks, PlasmaTube now protects your privacy, as it accesses videos through [Invidious](https://github.com/iv-org/invidious), a YouTube frontend that does not require a login, blocks tracking and ads and does not require JavaScript.

![PlasmaTube screenshot showing a video from Nicco Loves Linux](plasmatube.png)

{{< /section >}}

{{< section class="itinerary py-5" >}}

![](https://apps.kde.org/app-icons/org.kde.itinerary.svg)

## [Itinerary](https://apps.kde.org/itinerary/)

{{< announcements/feature-container src="itinerary.png" >}}

**Itinerary**, KDE's travel assistant for computers and mobile phones, comes with a complete redesign of the pages showing your travel information. The new design is easier to navigate, includes more information about your trip, and makes it simpler to use the QR at the airport.

{{< /announcements/feature-container >}}

{{< /section >}}

{{< section class="plasmatube py-5" >}}

![](https://apps.kde.org/app-icons/org.kde.kasts.svg)

## [Kasts](https://apps.kde.org/kasts)

**Kasts** is KDE's podcast app that works both on your computer and mobile phone. Now you can minimize Kasts to the system tray and customize the playback speed of your favorite shows. Another new feature is that you can now search through your catalog of subscribed podcasts.

![Kasts showing search results for KDE](kasts.png)

{{< /section >}}

{{< section class="kate py-5" >}}

![](https://apps.kde.org/app-icons/org.kde.kate.svg)

## [Kate](https://apps.kde.org/kate)

**[Kate](https://kate-editor.org/)** and **[KWrite](https://apps.kde.org/kwrite/)** can now open each new file you want to edit in its own window, rather than in tabs under one window.

Kate received various improvements to its Language Server Protocol (LSP) support. There is now a Code Formatting plugin that provides a simple way to format your code. Another new feature is the support of LSP Inlay Hints to show virtual text for e.g. parameter name at the call site which can enhance code readability.

Kate now provides a unified diagnostics view that collect the diagnostics from all your plugins.

{{< video src="kate.mp4" loop="true" muted="true" autoplay="true" controls="false" width="1338" >}}

{{< /section >}}

{{< section class="py-5" >}}

![](https://apps.kde.org/app-icons/org.kde.konsole.svg)

## [Konsole](https://apps.kde.org/konsole)

Konsole now works on Windows! This means that Konsole embedded in other applications like Kate, Dolphin, or KDevelop will also work on Windows.

You can download the latest binary [here](https://binary-factory.kde.org/job/Konsole_Nightly_win64/).

![Konsole on windows](konsole.png)

{{< /section >}}

{{< container >}}

## ...And also this

* **[Ark](https://apps.kde.org/ark/)** is an easy-to-use file compression/decompression tool. The new version comes with a richer welcome screen, and the app now supports extracting *Stuffit* files.
* **[Dragon Player](https://apps.kde.org/dragonplayer/)** is a minimalistic KDE-native video and audio player. Dragon Player has undergone a major overhaul of its looks, including adopting the hamburger menu of many other KDE apps, and a welcome screen. It has also gained a streamlined and more intuitive set of default toolbar buttons.

---

{{< announcements/gear_major_outro >}}

{{< /container >}}
