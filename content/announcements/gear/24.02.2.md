---
date: 2024-04-11
appCount: 180
layout: gear
---
+ kcachegrind:  Fix crash when opening history menu ([Commit](https://commits.kde.org/kcachegrind/bb9982b3a0300a92e8b252ef0439e1cdbc017faf), fixes bug [#483973](https://bugs.kde.org/483973))
+ gwenview: No longer inhibit suspend when viewing an image ([Commit](https://commits.kde.org/gwenview/1592fa119db3370cf963cf627c37955f65eebc59), fixes bug [#481481](https://bugs.kde.org/481481))
+ elisa: Fix broken volume slider with Qt Multimedia backend ([Commit](http://commits.kde.org/elisa/12163b9d1a0bfad327559d5be09a87daa2fef6ed), fixes bug [#392501](https://bugs.kde.org/392501))
