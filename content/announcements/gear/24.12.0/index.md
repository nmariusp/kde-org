---
title: KDE ⚙️ Gear 24.12
subtitle: 2024 Fundraiser Edition
layout: gear-24-12
noContainer: true
date: 2024-12-12
scssFiles:
- /scss/gear/24.12.scss
hero_image: hero.png
images:
  - Hero.webp
draft: false
intro: |
  Thanks to the continued [support of generous donors](https://kde.org/fundraisers/yearend2024/), every 4
  months KDE releases a long list of new versions of applications all at the same time.
  
  Today we are releasing KDE ⚙️ Gear 24.12 with new versions of KDE classics such as [Dolphin](#dolphin),
  our feature-rich file manager and explorer; [Kate](#kate), the developer-friendly text editor; 
  [Itinerary](#itinerary), a travel assistant that will get you safely to your destination. …and much,
  much more!
  
  These apps exist thanks to KDE's volunteers and donors. You too can contribute and express support for
  your favorite apps [by adopting them](https://kde.org/fundraisers/yearend2024/#adopt)!
  
  Let's take a look at just a few of the applications — some updated and some brand new — which will be
  landing on your desktop in just a few days.

marble:
- name: Marble Maps
  url: https://blogs.kde.org/2024/09/22/this-week-in-kde-apps/marble-maps.png
- name: Information about places
  url: https://blogs.kde.org/2024/09/22/this-week-in-kde-apps/marble-maps-info.png
- name: Routing in Marble Maps
  url: https://blogs.kde.org/2024/10/06/this-week-in-kde-apps/marble-route-map.png
- name: Routing in Marble Maps
  url: https://blogs.kde.org/2024/10/06/this-week-in-kde-apps/marble-route.png
---

{{< announcements/donorbox >}}

{{< section class="surf pb-5" >}}

{{< colorful-heading content="Work" class="display-3 mt-0 text-center" >}}

{{< app-title appid="okular" >}}

Okular is much more than a PDF reader: it can open all sorts of files, sign and verify the signatures of official documents, and annotate and fill in embedded forms.

Speaking of which, we implemented support for more types of items in comboboxes of PDF forms, and improved the speed and correctness of printing.

We also made it easier to digitally sign a document, and no longer hide the signing window prematurely until the signing process is actually finished.

![Okular simplifies digital signing.](Okular.png)

{{< app-title appid="kleopatra" >}}

Kleopatra keeps track of your digital signatures, encryption keys, and certificates. It helps you sign, encrypt, and decrypt emails and confidential messages.

We redesigned Kleopatra's notepad and signing encryption dialog, as well as making the resulting messages and errors clearer. In the notepad, the text editor and the recipients view are now also shown side-by-side.

![Kleopatra has a new editor for encrypting messages.](https://blogs.kde.org/2024/11/03/this-week-in-kde-apps/kleopatra-notepad.png)

Which brings us to…

{{< app-title appid="merkuro" >}}

…Where the OpenPGP and S/MIME certificates of a contact are now displayed directly in Merkuro Contact. Clicking on them will open Kleopatra and show additional information.

![Merkuro displays your contacts key info.](https://blogs.kde.org/2024/10/13/this-week-in-kde-apps/merkuro-contact-crypto.png)

{{< /section >}}

{{< section class="blue gear-section py-5" >}}

{{< colorful-heading content="Create" class="display-3 mt-5 text-center" >}}

{{< app-title appid="kdenlive" >}}

Kdenlive, KDE's acclaimed video editor, keeps adding features and now lets you resize multiple items on the timeline at the same time.

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Apps/24.12/kdenlive.webm" src="https://cdn.kde.org/promo/Announcements/Apps/24.12/kdenlive.mp4" poster="https://cdn.kde.org/promo/Announcements/Apps/24.12/kdenlive.png" fig_class="mx-auto max-width-800" >}}

{{< app-title appid="kwave" >}}

Kwave, KDE's native audio editor, has long been on the development backburner, but is now receiving updates again.

First it was ported to Qt6, which means it will work natively with Plasma 6. After that, the interface received some visual improvements in the way of new and more modern icons and a better visual indication when playback is paused.

![Kwave boasts a renewed look.](KWave.png)

{{< /section >}}

{{< section class="surf py-5" >}}

{{< colorful-heading content="Manage" class="display-3 text-center mt-0" >}}

{{< app-title appid="dolphin" >}}

The latest changes to KDE's file explorer/manager tend heavily towards accessibility<sup>[*](#d_annot)</sup> and usability.

For starters, the main view of Dolphin was completely overhauled to make it work with screen readers, and improved the keyboard navigation: pressing <kbd>Ctrl</kbd>+<kbd>L</kbd> multiple times will switch back and forth between focusing and selecting the location bar path and focusing the view. Pressing <kbd>Escape</kbd> in the location bar will now move the focus to the active view. The keyboard navigation in the toolbar has also been improved, as now the elements are focused in the right order.

Dolphin's sorting of files is more natural and "human" in this version: a file called "a.txt", for example, will appear before "a 2.txt", and you can also sort your videos by duration.

When it comes to your safety and checking your files, Dolphin has overhauled the checksum and permissions tab in the _Properties_ dialog to make it easier for you. You will see this improvement in other KDE applications too.

![Dolphin makes checking the integrity of files easier.](dolphin_md5.png)

Finally… Dolphin goes mobile! Dolphin now includes a mobile-optimized interface for Plasma Mobile. After the addition of a selection mode and improvements to touchscreen-compatibility, Dolphin works surprisingly well on phones! That said, more work is still needed and planned over time to more closely align the user interface with typical expectations for mobile apps.

![Dolphin's mobile look.](dolphin-mobile_composite.png)

---
<sup><a name="d_annot"></a>*</sup> Many of the accessibility improvements made to Dolphin 24.12 were possible thanks to funding provided by the NGI0 Entrust Fund, a fund established by NLnet with financial support from the European Commission's Next Generation Internet program.

{{< app-title appid="kcron" >}}

A less well-known utility, but also very useful, is KCron. UNIX old-timers will recognize this as a frontend for the venerable `cron` command of yore. For the rest of you, it lets you schedule any kind of jobs to run at any time on your machine.

Once installed, you will find it in _System Settings_ under _Session_ > _Task Scheduler_. In the new version, KCron's configuration page was ported to QML and given a fancy new look.

![KCron simplifies programming scheduled tasks.](KCron.png)

{{< app-title appid="kdeconnect" >}}

KDE Connect is our popular app for connecting your desktop with your phone and, indeed, all your other devices. It allows you to share files, clipboards, and resources, as well as providing a remote control for media players, input devices, and presentations.

Great news: Bluetooth support for KDE Connect now works! Plus KDE Connect starts up much faster on macOS, dropping from from 3s to 100ms!

In the looks department, the list of devices you can connect to now shows the connected and remembered devices separately, and the list of plugins can be filtered and comes with icons.

![KDE Connect presents devices in a much more orderly fashion now.](kdeconnect.png)

{{< app-title appid="krdc" >}}

If you need to access a remote desktop from your computer, you can start KRDC by opening a _.rdp_ file containing the RDP connection configuration. KRDC now works much better on Wayland too.

{{< /section >}}

{{< section class="blue gear-section py-5 tokodon" >}}

{{< colorful-heading content="Travel" class="display-3 mt-0 text-center" >}}

{{< app-title appid="itinerary" >}}

The biggest change to your KDE travel assistant is how it handles concert, train, bus, and flight tickets, as well as hotel reservations. Itinerary now groups entries into individual trips, with each of them having their own timeline.

Itinerary suggests an appropriate existing trip when importing a new ticket, and displays some statistics about your trip, like the CO<sub>2</sub> emissions, the distance traveled, and the costs (if available). Whole trips can also be exported directly and displayed on a map.

![Itinerary shows you details of your trip.](itinerary_phone.png)

![Itinerary can display your trips on a map.](itinerary-map.png)

Itinerary can now handle _geo://_ URLs by opening the "Plan Trip" page with a pre-selected arrival location. This is supported both on Android and Linux.

Itinerary now supports search for places (e.g. street names) in addition to stops, and can show the date of the connection when searching for a public transport connection.

New services supported by Itinerary include:

- GoOut tickets (an event platform in Poland, Czechia and Slovakia)
- The Luma and Dimedis Fairmate event ticket sale systems
- Colosseum Ticket in Rome
- Droplabs, a Polish online ticket sale system
- The Leo Express train booking platform 
- Google Maps links
- European Sleeper seat reservations in French
- Thai state railway tickets
- VietJet Air
- planway.com
- Koleo
- Reisnordland ferries
- Reservix

…And more.

{{< app-title appid="kongress" >}}

Kongress is an app which helps you navigate conferences and events.

The newest version will display more information in the event list. This includes whether the event is in your bookmarked events and the locations within the event (e.g. the rooms).

{{< app-title appid="marble" >}}

Marble is a virtual globe and world atlas. It has recently been ported to Qt6 and its very old Kirigami looks were largely rewritten and modernized.

{{< screenshots name="marble" >}}

Marble Behaim — a special version of Marble that lets you explore the oldest globe representation of the Earth known to exist — now also works.

![](behaim.png)

{{< /section >}}

{{< section class="surf py-5" >}}

{{< colorful-heading content="Communicate" class="display-3 mt-0 text-center" >}}

{{< app-title appid="tokodon" >}}

Tokodon is your gateway into the Fediverse.

Developers of KDE's desktop and phone app have worked hard to improve your experience when accessing Mastodon for the first time. We have redesigned the welcome page, and, more importantly, Tokodon now fetches a list of public servers to simplify the registration process.

We have also focused on safety, so now you can forcibly remove users from your followers list. A safety page has been added to the Tokodon settings to manage the list of muted and blocked users.

So you can travel further through the Fediverse, Tokodon has improved the support for alternative server implementations, such as GoToSocial, Iceshrimp.NET, and Pixelfed. Tokodon has also added "News" and "Users" tabs to the Explore page.

![Tokodon guides you in your exploration of Mastodon.](tokodon_explore.png)

We also added a new "Following" feed, to quickly page through your follows and their feeds. It's now easier to start private conversations or mention users right from their profile page.

![Converse with your followers using private messages.](tokodon_following.png)

Tokodon now supports quoting posts, and when you are writing a post, your user info is on display, which is useful if you post from multiple accounts. Right clicking on a link on a post will show a context menu allowing users to copy or share the URL directly.

Finally, a proper grid view for the media tab has been added in the profile page.

![Examine the media you posted in a grid.](tokodon_mediagrid.png)

{{< app-title appid="neochat" >}}

NeoChat gives you a convenient way to interact with users on the [Matrix](https://matrix.org) chat network.

As your trust and safety are important when talking with strangers, you now have the option to block images and videos by default, and we implemented a Matrix Spec that redirects searches for harmful and potentially illegal content to a support message.

Besides that, when replying to users you ignored, your message will not be shown, avoiding accidentally interacting with disagreeable people. We have also improved the Security settings page to be more relevant and useful to normal users.

NeoChat's looks and usability have also improved and include a nicer emoji picker, more room list sorting options, a more complete message context menu, and better-looking polls.

![NeoChat's improved emoji selector.](neochat_emojis.png)

{{< /section >}}

{{< section class="blue gear-section py-5 tokodon" >}}

{{< colorful-heading content="Develop" class="display-3 mt-0 text-center" >}}

{{< app-title appid="kate" >}}

Instead of big features, devs have concentrated on the small things this time around, aiming to improve the overall experience. For example, Kate now starts up faster and gives visual cues of the Git status ("modified" or "staged") within the _Project_ tree.

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Apps/24.12/Kate_Git.webm" src="https://cdn.kde.org/promo/Announcements/Apps/24.12/Kate_Git.mp4" poster="https://cdn.kde.org/promo/Announcements/Apps/24.12/Kate_Git.png" fig_class="mx-auto max-width-800" >}}


The order of the tabs is correctly remembered when restoring a previous session, and the options of the LSP Servers are more easily discoverable as they are no longer only available via a context menu, but also within a menu button at the top.

Kate's inline code formatting tooltips have been improved and can now also be displayed in a special context tool view, plus plugins now work on Windows, and have been expanded to include an out-of-the-box support for Flutter debugging.

The Quick Open tool lets you search and browse the projects open in the current session, and a _Reopen latest closed documents_ option has been added to the tab context menu.

{{< /section >}}

{{< container >}}

## And all this too…

* [Francis](https://apps.kde.org/francis/), the app that helps you plan your work sessions and avoid fatigue, lets you skip the current phase of work or break time in its new version.
* [Konqueror](https://apps.kde.org/konqueror/), our venerable file explorer/web browser, comes with improved auto-filling of login information.
* The [Elisa](https://apps.kde.org/elisa/) music player supports loading lyrics from _.lrc_ files sitting alongside the song files.
* [Falkon](https://www.falkon.org/) comes with a context menu for Greasemonkey. Greasemonkey lets you run little scripts that make on-the-fly changes to web page content.
* The [Alligator](https://apps.kde.org/alligator/) RSS feed reader offers bookmarks for your favorite posts.
* [Telly Skout](https://apps.kde.org/telly-skout/), one of the newcomer apps for scheduling your TV viewing, comes with a redesigned display that lists your favorite TV channels and the TV shows that are currently airing.

![Telly Skout lets you schedule your TV viewing.](telly-skout.png)

{{< announcements/gear_major_outro >}}

{{< /container >}}
