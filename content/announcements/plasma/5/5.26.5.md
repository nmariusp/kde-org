---
date: 2023-01-03
changelog: 5.26.4-5.26.5
layout: plasma
video: false
asBugfix: true
draft: false
---

+ kscreenlocker: Fix wallpaper not loading (leaving the background black). [Commit.](http://commits.kde.org/kscreenlocker/50fc20dc940c87ffd62be0650b935324e164b59d) 
+ Libdiscover: fix permission text not being translated. [Commit.](http://commits.kde.org/discover/327f8ec98ae37697b1b0c4388c4a27244012e8c9) 
+ Plasma Addons Alternatecalendar: always use day name in full label. [Commit.](http://commits.kde.org/kdeplasma-addons/d3f0524126224dd9c49d345ff79cef7d00398834) 
