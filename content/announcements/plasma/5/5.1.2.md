---
aliases:
- ../../plasma-5.1.2
date: '2014-12-16'
layout: plasma
---

{{< figure src="/announcements/plasma/5/5.1.0/plasma-main.png" class="text-center float-right ml-3" caption="Plasma 5" width="600px" >}}

{{< i18n_date >}}

{{< i18n "annc-plasma-bugfix-intro" "5" "5.1.2" >}}

{{% i18n "annc-plasma-bugfix-minor-release-10" "5.1" "/announcements/plasma/5/5.1.0" "2014" %}}

{{< i18n "annc-plasma-bugfix-worth-5" >}}

{{< i18n "annc-plasma-bugfix-last" >}}

{{% hg_stop %}}

### Plasma Desktop

- Fix vertical aligment.
- Fix test by removing old test install directory
- Read/Write ColorScheme to configGroup General instead of KDE
- Remove KGlobal check and KComponentData object that overrode applications KAboutData when showing dialogs.
- Ignore bottom margin unless view is overflowing.
- Highlight first entry when searching
- Fix 'Forget App' in Recent Apps not working immediately.
- [desktop/views] Set right margin properly
- Don't duplicate entries when the Rever button is clicked
- Bouncing cursor is the default
- Fix installing cursor themes from GHNS
- Fix target install dir
- Fix crash on Defaults
- Fix install target dir
- Disable checkbox buddies when unchecked.
- Yank duplicate group header.
- X-KDE-Keywords separator is ,
- Fix regression in event handler causing favorites to launch on right-click release.
- Don't put scripts from ~/.config/autostart in autostart kcm list.
- Don't parse window titles as rich text.

### Oxygen

- Fixed color role for accelerator
- Added inputWidgetEngine, for abstract item views, line editors, spinboxes and comboboxes it is needed to avoid animation conflicts between lists and buttons (when, e.g. there is a checkbox inside a list)
- Removed support for Q3ListView
- Manually calculate viewport update rect on header hover change
- Remove mnemonic underlines when alt-tabbing
- CMake: use the kde4workspace include dir
- CMake: fix searching for pkg-config
- Fixed member initialization removed unused headers backported from breeze

### Breeze

- Fix ui files
- Fixed color role for accelerator
- Added inputWidgetEngine, for abstract item views, line editors, spinboxes and comboboxes it is needed to avoid animation conflicts between lists and buttons (when, e.g. there is a checkbox inside a list)
- Removed Q3ListView support
- Add LGPL 3 for breeze icons with clairification as in Oxygen
- Manually calculate viewport update rect on header hover change BUG: 340343
- Fixed KDE4 compilation CCBUG: 341006
- Remove mnemonic underlines when alt-tabbing
- Removed unused members CCMAIL: staniek@kde.org
- Fixed uninitialized member \_value CCMAIL: staniek@kde.org

### Powerdevil

- Fix battery remaining time update with upower >= 0.99

### KDE CLI Tools

- Use QFile::decodeName for command in KDEsuDialog to fix encoding
- port away from Q_WS_X11

### System Settings

- Restore KStandardGuiItems

### KWin

- [kcmkwin/screenedges] Drop check whether Compositing is enabled
- Cleanup electric border handling in leaveMoveResize
- Fixuifiles
- Require OpenGL 2.0 in the SceneOpenGL2 constructor

### Plasma Workspace

- [lookandfeel/osd] Make the OSD timeout shorter
- Fix ui files
- Fix lockscreen theme fallback
- Add the next wallpaper action when necessary
- [applets/notifications] Revert i18n changes cherry-picked from master
- Only play indeterminate animation when plasmoid is expanded
- Refactor JobDelegate
- Don't leave a gap when label1 is not visible
- Don't emit a Job finished notification if the message would be empty
- Finish cleanup and remove unused config.ui remnant
- When no percentage is exposed over the dataengine make the progress bar indeterminate
- Cleanup Jobs code and remove dead code
- Alleviate the annoyance of Job Finished notifications
- Escape ampersands in notifications
- [dataengines/notifications] Replace \n with <br/>
- Fix dialog minimum height (always add the margins afterwards)
- Also take the title label into account when calculating the dialog size
- Enforce StyledText in notifications
- Hide popup when opening configure dialog
- Fix notification configure button
- Fix binding loops and make popup even more compact
- Cleanup Loaders
- Cleanup Notifications code
- Move duplicated code from NotificationDelegate and NotificationPopup into a new NotificationItem
- Make sure lock screen package name does not overlap highlight rectangle
- Actually save the kscreensaver config
- fix resetting and reverting to default values
- Fix qml warnings in splash kcm
- Separator for Keywords is ;
- Hopefully fix shell resizing in VirtualBox
- Take into account the primary screen can be redundant
- Delayed primaryOutput processing needs to be delayed
- [digital-clock] Also set minHeight in vertical panels
- BUG: 337742 REVIEW: 120431. Fix and future-proof security in Dr Konqi.
- [freespacenotifier] Fix hiding the SNI when avail space goes up again
- [freespacenotifier] Make the SNI just Active when free space raised
- Find messages in QML as well

### Plasma Addons

- Handle onExternalData event in notes applet
- Properly wrap to the next hour

### KIO Extras

- Sanitize path
