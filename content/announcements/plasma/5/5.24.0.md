---
aliases:
- ../../plasma-5.24.0
title: "Plasma 5.24"
subtitle: "Perfect Harmony"
description: "Plasma 5.24 improves in looks, ease of use and consistency."
date: "2022-02-08"
images:
 - /announcements/plasma/5/5.24.0/fullscreen_with_apps.png
layout: plasma-5.24
scssFiles:
- /scss/plasma-5-24.scss
authors:
- SPDX-FileCopyrightText: 2022 Nate Graham <nate@kde.org>
- SPDX-FileCopyrightText: 2022 Paul Brown <paul.brown@kde.org>
- SPDX-FileCopyrightText: 2022 Aniqa Khokhar <aniqa.khokhar@kde.org>
- SPDX-FileCopyrightText: 2022 Phu Hung Nguyen <phu.nguyen@kdemail.net>
SPDX-License-Identifier: CC-BY-4.0
highlights:
  title: Highlights
  items:
  - header: Overview
    text: Use Overview to manage all your desktops and applications
    hl_video:
      poster: https://cdn.kde.org/promo/Announcements/Plasma/5.24/Overview_720p.png
      mp4: https://cdn.kde.org/promo/Announcements/Plasma/5.24/Overview_720p.mp4
      webm: https://cdn.kde.org/promo/Announcements/Plasma/5.24/Overview_720p.webm
    hl_class: overview
  - header: KRunner
    text: Discover KRunner features with the help assistant
    hl_video:
      poster: https://cdn.kde.org/promo/Announcements/Plasma/5.24/Krunner_assistant_720p.png
      mp4: https://cdn.kde.org/promo/Announcements/Plasma/5.24/Krunner_assistant_720p.mp4
      webm: https://cdn.kde.org/promo/Announcements/Plasma/5.24/Krunner_assistant_720p.webm
    hl_class: hl-krunner
  - header: Fingerprint Readers
    text: Unlock the screen and authenticate apps with your fingerprints
    hl_video:
      poster: https://cdn.kde.org/promo/Announcements/Plasma/5.24/Fingerprint_720p.png
      mp4: https://cdn.kde.org/promo/Announcements/Plasma/5.24/Fingerprint_720p.mp4
      webm: https://cdn.kde.org/promo/Announcements/Plasma/5.24/Fingerprint_720p.webm
    hl_class: fingerprint
draft: false
---

{{< container >}}

Today the KDE Community releases Plasma 5.24, a Long Term Support (LTS) release that will receive updates and bugfixes until the final Plasma 5 version, before we transition to Plasma 6.

This new Plasma release focuses on smoothing out wrinkles, evolving the design, and improving the overall feel and usability of the environment.

Things to look for in Plasma 5.24:

{{< /container >}}

{{< highlight-grid >}}

{{< container >}}

### Desktop and Panel

{{< img src="Desktop.png" alt="Plasma 5.24 desktop" img_class="max-width-800" >}}

The first thing you will notice is Ken Vermette's spectacular new wallpaper for Plasma 5.24. Ken [created the *Honeywave* wallpaper](https://youtu.be/ofxf7xCOuQQ) and [streamed the process live on YouTube](https://youtu.be/6PExe8ZmMic), giving Plasma a whole new look.

{{< img src="Ken_Vermett_Wallpaper.png" alt="Ken Vermett designing the Honeywave wallpaper live on YouTube" img_class="max-width-800" >}}

If you happen to prefer something else as your background, you can now set your wallpaper just by right-clicking on any picture in Dolphin and selecting *Set as Wallpaper* from the context menu. Furthermore, if you use the *Picture of the Day* wallpaper plugin, it can now pull images from [Simon Stålenhag's gallery](http://simonstalenhag.se), which is full of cool and creepy sci-fi images.

{{< img src="PotD_wallpaper02.png" alt="Get a new wallpaper every day using the Picture of the day feature" img_class="max-width-800" >}}

Bring up the desktop context menu again and you will notice that there is a new *Configure Display Settings* item. This lets you adjust your screens' resolution, orientation, scaling, and position relative to other monitors.

Desktop Panels are now easier to move around and stick to any edge you want, as you can now drag them from anywhere on their toolbar while in *Edit Mode*. And when you drag-and-drop widgets onto the desktop and move them around, they now smoothly animate moving to their final position rather than instantly teleporting there.

{{< /container >}}

{{< twilight >}}

### Looks and Themes

{{< feature-video webm="https://cdn.kde.org/promo/Announcements/Plasma/5.24/light_dark_loop_720.webm" mp4="https://cdn.kde.org/promo/Announcements/Plasma/5.24/light_dark_loop_720.mp4" poster="https://cdn.kde.org/promo/Announcements/Plasma/5.24/light_dark_loop_720.png" >}}

After the spectacular new wallpaper, the next thing you will notice are the changes to [Breeze](https://community.kde.org/Get_Involved/design/Breeze), Plasma's default theme. It has received a visual refresh to make it more closely match the Breeze style for apps, improving the visual consistency between them.

Plasma is all about customization, and we have worked on making the process clear and simple so you can adapt your environment exactly to how you like it. That is why the default Breeze color scheme has been renamed to *Breeze Classic*, to better distinguish it from the *Breeze Light* and *Breeze Dark* color schemes.

{{< /feature-video >}}

{{< feature-video webm="https://cdn.kde.org/promo/Announcements/Plasma/5.24/Accents_folders_720p.webm" mp4="https://cdn.kde.org/promo/Announcements/Plasma/5.24/Accents_folders_720p.mp4" poster="https://cdn.kde.org/promo/Announcements/Plasma/5.24/Accents_folders_720p.png" >}}

Speaking of colors, Breeze now extends the feature that lets you pick accent colors ([introduced in Plasma 5.23](https://kde.org/announcements/plasma/5/5.23.0/)) and now lets you pick any color you'd like, should the pre-selected colors fail to tickle your fancy. Not only that, but changing the color scheme now toggles the standardized FreeDesktop light/dark color scheme preference. This means that non-KDE apps that respect this preference will automatically switch to light or dark mode based on your chosen color scheme. *Global Themes* can also specify and change the layouts of KDE's [Latte Dock](https://apps.kde.org/latte-dock/).

A theme must not only be beautiful, but it must make life easy for users. That is why we made the focus effect for user interface controls into a large "focus ring". This makes the item that has keyboard focus much easier to perceive at a glance.

{{< /feature-video >}}

{{< /twilight >}}

{{< container class="monitor" >}}

![](/breeze-icons/preferences-desktop-notification-bell.svg)

### Notifications

To make critically important Plasma notifications stand out, they now come with an orange strip on the side to visually distinguish them from less urgent messages. Meanwhile, we have improved the contrast and visibility of the header and title labels in all notifications to make them easier to read.

Another visual enhancement is that notifications about video files now display a thumbnail of the video in the notification itself, just like for image files.

### System Tray and Widgets

Many widgets have received new features and subtle enhancements that improve their looks, the relevance of their information, and ease of navigating them. For example, we have added a *Date always below time* option for the *Digital Clock*. The *Weather* widget asks you to configure it when you add it for the first time to the tray, and now automatically searches through all available weather sources. The *Battery & Brightness* widget has been made clearer and now shows a more accurate title and icon for computers with no batteries. The new versions of the *Clipboard* and *Network* widgets can now be navigated with just your keyboard.

### Task Manager

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Plasma/5.24/audio_720.webm" src="https://cdn.kde.org/promo/Announcements/Plasma/5.24/audio_720.mp4" poster="https://cdn.kde.org/promo/Announcements/Plasma/5.24/audio_720.png" fig_class="mx-auto max-width-800" >}}

When you hover the cursor above a *Task Manager* item, the window thumbnail *tooltip* that pops up is now significantly faster to load, and display a volume slider for windows playing audio.

The Task Manager's context menus have been clarified and simplified. *Start New Instance*, for example, is now *Open New Window* and no longer appears for apps marked as having a single main window. Meanwhile, the *More Actions* item has been moved to the bottom of the menu and changed to just *More*.

{{< /container >}}

{{< container class="monitor" >}}

![System Settings](/breeze-icons/systemsettings.svg)

### System Settings

Version after version, KDE developers and designers work to make *System Settings* easier and faster to use. You can see the progress and attention to detail in such small things as System Settings' own settings: there were so few options that developers have moved them to the hamburger menu that you can see next to the *Search* text box, preventing the need to clutter up your workspace with another window.

{{< img src="Sytems_Setting_settings.png" alt="System Setting simplified in every way" >}}

If you are using Plasma in a language other than English, we also have you covered. You can use the *Search* field to find terms both in your language and English without having to switch from one to the other, just in case it has not been translated yet or you are more familiar with the English term. The *Add a keyboard layout* dialog has been redesigned for simplicity and ease of use.

Other design and usability improvements have been added to *Night Color*, the speaker test sheet in System Settings' *Audio* page, and the battery charge limit feature. For people wanting to fine-tune their monitors, the *Display & Monitor* section now shows the scale factor used for each screen as well as their proper physical resolution.

For your convenience, when you enable auto-login, you now get a warning about some changes you might want to make to your KWallet setup for optimal usability.

{{< /container >}}

{{< diagonal-box color="purple" nocol="true" >}}

### Window Management

You can better leverage the power of multiple desktops in Plasma 5.24 with the new *Overview* effect. It is still in beta testing, but you can enable it in *System Settings'* *Workspace Behavior* > *Desktop Effects* page and give it a whirl.

{{< img src="Overview_720p.png" alt="Window Management" >}}

Activate it by holding down the <kbd>Meta</kbd> (<kbd>Windows key</kbd>) on your keyboard and pressing the <kbd>w</kbd> key to see an overview of all your open windows and virtual desktops. You can activate windows, drag and drop windows from one desktop to another, and use the powerful KRunner assistant to search, calculate and run tasks all from the same place.

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Plasma/5.24/switch_720.webm" src="https://cdn.kde.org/promo/Announcements/Plasma/5.24/switch_720.mp4" poster="https://cdn.kde.org/promo/Announcements/Plasma/5.24/switch_720.png" fig_class="mx-auto max-width-800" >}}

In addition, the *Cover Flip* and *Flip Switch* effects are back! You can find them in the *System Settings* *Window Management* > *Task Switcher* page. These effects let you visually flip through open windows as if they were a deck of cards.

Among the more subtle changes are that Plasma now "remembers" which window was on which screen even when the screen in question is turned off, and performance has been improved for machines with NVIDIA video cards.

Finally, new windows are now opened in the center of the screen by default.

{{< /diagonal-box >}}

{{< container class="monitor" >}}

![Discover](/breeze-icons/discover.svg)

### [Discover](https://apps.kde.org/discover/)

Discover now gives you the option to automatically restart after an update has completed. Simply click the checkbox at the bottom of the Updates page, grab a caffeinated beverage, and go enjoy a break. By the time you return, Plasma will have rebooted into the brand new updated system.

We have also added a mobile mode for when you use it on a phone: If it is on a wide screen, its home page will show a two-column card view. Open it on a phone and it will show a bottom tab bar in narrow/mobile mode.

Other improvements are that checking for updates is much faster, and the *Update* page is now less overwhelming, with a nicer design to select updates. Additionally, it tells you the source of the updates.

Speaking of which... Discover has supported [Flatpaks](https://www.flatpak.org/) for quite some time, but now locally downloaded Flatpak packages can be opened and installed with Discover, and their repositories automatically get added to your system.

{{< img src="flatpakrepos.png" alt="Configure new Flatpak repos easily" >}}

Discover also tries to be smarter and friendlier and helps with suggestions if it can't find exactly what you are looking for. Installation error messages are also more user-friendly and come with a new *Report a problem* link at the bottom of each description page. If you have a problem with an update, click the link and it takes you straight to the bug tracker page for your operating system. Developers have also included a new safety measure in Discover that stops you from uninstalling your Plasma desktop by mistake.

{{< /container >}}

{{< container class="monitor" >}}

![KRunner](/breeze-icons/krunner.svg)

### KRunner

KRunner can already find documents and web pages, run commands, convert units and currencies, give you definitions of words, do high level math — and new features are being added with plugins all the time. It can be hard to keep up with everything it can do! But now it's easy to become a KRunner whiz: click on the question mark icon on the toolbar and KRunner will show a list of available plugins and how to use them. Click on a plugin from the list and you'll see all the information you need to use it to its fullest.

{{< /container >}}

{{< container class="monitor" >}}

![](/breeze-icons/preferences-desktop-user-password.svg)

### Login/Lock Screen

Fingerprint authentication is now incorporated into Plasma. You can also enroll up to 10 fingerprints and use them to unlock the screen, provide authentication when an app asks for your password, and also authenticate you when you need to run a command with <code>sudo</code>from the command line.

Speaking of the lock screen, now you can put your machine to sleep or hibernate it from the lock screen without having to unlock it first.

{{< /container >}}

{{< diagonal-box color="blue" logo="/images/wayland.png" alt="Wayland logo" >}}

### Wayland

Wayland is the upcoming graphical system that will soon replace the aging X11 system. Wayland opens up a world of possibility for more stable and visually consistent applications, support for touch screens and devices that detect orientation, and more.

Adapting KDE's desktop to Wayland continues at a brisk pace with a massive number of improvements, including support for colors greater than 8 bits, VR headsets with optimal performance, and drawing tablets. In that vein, when using a stylus, you can now activate other windows from their titlebars and interact more with titlebars in general. Another improvement for tactile input is that the system tray item for showing and hiding the virtual keyboard becomes active only in tablet mode, and the virtual keyboard itself only appears when you explicitly focus a text control with a touch or stylus poke.

Wayland versions of apps and components are quickly acquiring all the features that are putting them on par with their X11 equivalents. For example, you can now set a primary monitor, the Spectacle screen-grabber app has now access to the *Active Window* mode, and the *Minimize All Windows* widget now works. Likewise, many keyboard shortcuts available in X11 are all being gradually implemented; like the <kbd>Meta</kbd> + <kbd>Tab</kbd> key combo, which is used to cycle through activities, and already works in Wayland.

{{< /diagonal-box >}}

{{< container >}}

{{< feature-grid title="Other Updates" >}}

* Scrollable controls in Plasma now only change their contents when you start scrolling on them. This means you can't accidentally change things while scrolling the view.
* Plasma now shuts down faster, as it no longer accepts new connections after it has begun the shutdown process. This is especially helpful when using [KDE Connect](https://kdeconnect.kde.org/).

... And there's much more going on. If you would like to see the full list of changes, [check out the changelog for Plasma 5.24](/announcements/changelogs/plasma/5/5.23.5-5.24.0).

{{< /container >}}
