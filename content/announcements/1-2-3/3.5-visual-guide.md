---
aliases:
- ../visualguide-3.5
custom_about: true
custom_contact: true
hidden: true
title: 'KDE 3.5: A Visual Guide to New Features'
---

KDE 3.5 brings improvements in aesthetics, usability and performance as well as adding new functionality and features. A full list can be found in the <a href="http://developer.kde.org/development-versions/kde-3.5-features.html">developer changelog</a>, but for humans here is a visual guide to some of the best improvements.

<img src="/announcements/1-2-3/3.5-visual-guide/k-desktop.png" alt="KDE 3.5" width="376" height="286" />


### The desktop

Since being taken over by an energetic new developer team, Kicker has seen a wealth of small but useful improvements. The pager now shows you the application icons of each window to help distinguish between them, and allows you to drag and drop windows from one desktop to another. In response to a great deal of user feedback, the pager and taskbar have three default styles: elegant, classic and transparent, which have been tweaked to make them more usable.

<img src="/announcements/1-2-3/3.5-visual-guide/kicker-styles.png" alt="Kicker with the three styles" width="376" height="154" />

The new Kicker tooltips that appeared for KDE 3.4 have been extended to be used throughout Kicker, providing a consistent and visually unique desktop. An animated tooltip has been added to give visual feedback of the location of a newly added applet when adding through the add applet dialog.

<img src="/announcements/1-2-3/3.5-visual-guide/kicker-applets.png" alt="Kicker with the add applet dialogue, applets and tooltips" width="376" height="273" />

SuperKaramba, a tool that allows you to easily create interactive eye-candy on the desktop, is now included in KDE. Using SuperKaramba you can quickly add functionality such as weather and news updates, system monitors and alternatives to the existing functions of Kicker. New applets can be easily downloaded using the KHotNewStuff functionality.

<img src="/announcements/1-2-3/3.5-visual-guide/superkaramba.png" alt="SuperKaramba, Get Hot New Stuff and the weather widget" width="376" height="279" />


### Konqueror

Konqueror has now become the second browser to pass the arduous 'Acid2' css compliance test. Apple's Safari browser was the first, which makes use of Konqueror's advanced rendering engine KHTML. Thanks to some fixes that were integrated back into Konqueror from Safari improvements, and the hard work of the KHTML programmers, Konqueror can now boast a high level of CSS compliance.

<img src="/announcements/1-2-3/3.5-visual-guide/konq-acid2.png" alt="Konqueror passes the Acid2 css test" width="376" height="267" />

Hot on the tails of Firefox, Konqueror now also has the much-requested adblock feature, allowing you to filter out advertisements on web pages. Not to be outdone on any front by the fox, Konqueror's new search bar provides a flexible and powerful way of searching any web site by using the existing web shortcut technology. Click on the <em>Select Search Engines...</em> menu entry to add existing engines to the menu and create new providers with ease.

<img src="/announcements/1-2-3/3.5-visual-guide/konq-adblock-search.png" alt="Konqueror with the adblock and searchbar features" width="376" height="236" />


### Communications

Kopete has benefited from a stable base with a raft of new features added since KDE 3.4. With support for audio and video devices, Kopete users can now make use of MSN and Yahoo! webcams. Managing several accounts has been made easier with the ability to export a global nickname and display picture via the global identity. For those who love to customise, Kopete can download new emoticon themes and chat window styles using KHotNewStuff, and also now supports custom MSN emoticons.

<img src="/announcements/1-2-3/3.5-visual-guide/msn-webcam.png" alt="Kopete with a MSN webcam" width="376" height="166" />


### Edutainment

For families, schools and the young at heart KDE's edutainment team have added a surprising number of new applications and features. KGeography lets you browse maps, learn about a country's capital and flag, and to test your knowledge. Kanagram is a fully featured anagram game with a vocabulary editor, many built-in vocabularies and sound effects, and new data packs available through KHotNewStuff. Finally, blinKen challenges players to remember sequences of increasing length, based upon an electronic game released in 1978.

<img src="/announcements/1-2-3/3.5-visual-guide/edugames.png" alt="blinKen, KGeography and Kanagram" width="376" height="242" />

For chemists, the periodic table viewer Kalzium has been redesigned and sports new artwork, a glossary, a sidebar for calculations and lots of new information on each element.

<img src="/announcements/1-2-3/3.5-visual-guide/kalzium.png" alt="Kalzium for chemists" width="376" height="238" />


### Under the hood

KDE has made an exciting breakthrough in its support for removable devices. On detection of specific media types KDE presents the user with a list of optional actions. These actions are configurable in KDE's control center and can be disabled entirely. This goes a great deal of the way toward fixing an old complaint, that managing removable media is too difficult in Linux, by exposing existing features in KDE and Linux to the user in an obvious fashion.

<img src="/announcements/1-2-3/3.5-visual-guide/device-popup.png" alt="Device popup dialogue" width="376" height="366" />

Finally, administrators will be pleased to see that KDE now supports POSIX file access control lists (ACL). If your filesystem supports them (most new Linux filesystems do) and you've mounted the filesystem with ACL support enabled, then the folder properties dialog will autodetect this and enable an extended permissions dialogue. You can then assign specific permissions to particular users or groups with the ease of a GUI.

Written and illustrated by Jes Hall, Tom Chance and Roland Wolters.