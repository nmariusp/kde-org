---
aliases:
- ../../kde-frameworks-5.5.0
date: '2014-12-11'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Use all of QT_PLUGIN_PATH paths rather than just QLibraryInfo path to look for plugins

### KActivities

- Fix plugin loading with KDE_INSTALL_USE_QT_SYS_PATHS ON

### KCMUtils

- Restore KStandardGuiItems to get consistent icons and tooltips

### KCodecs

- Introduce KEmailAddress class for email validation
- Use more robust implementation of MIME codecs taken from the KMime library
- Add KCodecs::encodeRFC2047String()

### KCompletion

- Fix PageUp/Down actions in the completion popup box

### KCoreAddons

- Add KTextToHTML class for plaintext->HTML conversion
- Add KPluginMetaData::metaDataFileName()
- Allow to read KPluginMetaData from .desktop files
- Kdelibs4Migration now gives priority to distro-provided KDE4_DEFAULT_HOME

### KDeclarative

- Use Qt's method of blocking for component completion rather than our own
- Make it possible to delay initialization of object incubated from QmlObject
- Add guard when trying to access root object before component is complete

### KEmoticons

- Add KEmoticonsIntegrationPlugin for KTextToHTML from KCoreAddons

### KHTML

- A number of forward-ported fixes from kdelibs, no API changes.

### KIO

- Fix Size columns being empty in the KFileWidget detailed views
- Do not drop ASN passed to KRun when executing desktop files
- Fix passing of DESKTOP_STARTUP_ID to child process in kioexec
- Fix compilation with Qt 5.2, which also fixes a race condition
- KFileItem: cleanup overlay icon usage
- Implement back/forward side mouse buttons to navigate in the history
- Allow user to cancel out of the certificate accept duration dialog box.

### KJobWidgets

- Fix compilation with Qt 5.2.0

### KNewStuff

- Also allow absolute filepaths for configfile parameter.
- Fix compilation on Windows

### KNotifications

- Make KNotificationPlugin a public class
- KPassivePopup - Set default hide delay

### KRunner

- Add a simple cli tool to run a query on all runners

### KService

- Fix KPluginTrader::query() for old JSON
- Deprecate kservice_desktop_to_json for kcoreaddons_desktop_to_json
- Implement KPluginTrader::query() using KPluginLoader::findPlugins()
- Fix KPluginInfo::entryPath() being empty when not loaded from .desktop

### KTextEditor

- Fix bug #340212: incorrect soft-tabs alignment after beginning-of-line
- Add libgit2 compile-time check for threads support

### KWidgetsAddons

- Add class KSplitterCollapserButton, a button which appears on the side of
  a splitter handle and allows easy collapsing of the widget on the opposite side
- Support monochrome icon themes (such as breeze)

### KWindowSystem

- Add KStartupInfo::createNewStartupIdForTimestamp
- Add support for more multimedia keys
- Add support for initial mapping state of WM_HINTS
- Drop incorrect warnings when using KXMessages without QX11Info

### Plasma Framework

- Fix compilation with Qt 5.2.0
- Fix the platformstatus kded module
- Migrate BusyIndicator, ProgressBar to QtQuick.Controls
- Add thumbnailAvailable property to PlasmaCore.WindowThumbnail

### Solid

- Fix warning: No such signal org::freedesktop::UPower::Device...

### Extra cmake modules

- Set CMAKE_INSTALL_SYSCONFDIR to /etc when CMAKE_INSTALL_PREFIX is /usr (instead of /usr/etc)
- Enable -D_USE_MATH_DEFINES on Windows

### Frameworkintegration

- Implement standardButtonText().
- Fix restoring the view mode and sizes in the file dialog

You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.
