---
date: 2022-03-16
title: "First Ever Eco-Certified Computer Program: KDE's Popular PDF Reader Okular"
description: "Okular, KDE’s popular multi-platform PDF reader and universal document viewer, has officially been recognized for sustainable software design as reflected in the Blue Angel award criteria for software."
origin: "https://eco.kde.org/blog/2022-03-16-press-release-okular-blue-angel/"
custom_about: true
custom_contact: true
---
