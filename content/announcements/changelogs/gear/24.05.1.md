---
aliases:
- ../../fulllog_releases-24.05.1
title: KDE Gear 24.05.1 Full Log Page
type: fulllog
gear: true
hidden: true
---
{{< details id="akonadi-mime" title="akonadi-mime" link="https://commits.kde.org/akonadi-mime" >}}
+ Explicitly link against QtDBus. [Commit](http://commits.kde.org/akonadi-mime/699cb46aafde7ec378f695282b21503ceef70fee).
{{< /details >}}
{{< details id="ark" title="ark" link="https://commits.kde.org/ark" >}}
+ Explicitly link against Qt::DBus. [Commit](http://commits.kde.org/ark/70d407368ff525ddc4b3b1eb9c7584caa81a891b).
{{< /details >}}
{{< details id="audex" title="audex" link="https://commits.kde.org/audex" >}}
+ Update logo. [Commit](http://commits.kde.org/audex/864168e506d1071cf8682bcedfece1c67a05854a).
{{< /details >}}
{{< details id="audiotube" title="audiotube" link="https://commits.kde.org/audiotube" >}}
+ Update ytmusicapi to 1.7.0. [Commit](http://commits.kde.org/audiotube/be5317eb74b6ec0d2365b159d31bb7da46225da7).
{{< /details >}}
{{< details id="dolphin" title="dolphin" link="https://commits.kde.org/dolphin" >}}
+ Baloo Widgets and kio-extras are both part of KDE Gear - which is also what Dolphin is part of. [Commit](http://commits.kde.org/dolphin/b34f98aa1e522b6c05ce2c94ad8851a04fdc1110).
+ Explicitly link against Qt::DBus. [Commit](http://commits.kde.org/dolphin/bf7d0ba4765a12f68211319adc5d82ec364cd422).
{{< /details >}}
{{< details id="elisa" title="elisa" link="https://commits.kde.org/elisa" >}}
+ Fix running on Windows (QtWinExtras no longer exists). [Commit](http://commits.kde.org/elisa/d4054de9d1b992c1cf88c017eab6feb223564247).
+ Fix potential crash when enqueueing from empty files view. [Commit](http://commits.kde.org/elisa/246178d77c77f89a430829e5113ffd20805acb54). Fixes bug [#441525](https://bugs.kde.org/441525). See bug [#484972](https://bugs.kde.org/484972).
+ Fix DBus service activation. [Commit](http://commits.kde.org/elisa/ec8ae492467320e6cb09b88167d220493dba9479). Fixes bug [#487905](https://bugs.kde.org/487905).
+ Remove unneeded function. [Commit](http://commits.kde.org/elisa/9434ff5bcd1f44451243c803e26f3f1c3f85ae59).
+ Fix 1px padding between playlist panel and right edge of window. [Commit](http://commits.kde.org/elisa/bd7b4f719150fa53455149ca0ac9694d8fb9db11).
+ Adapt to source incompatible changes in Qt's 6.7 JNI API. [Commit](http://commits.kde.org/elisa/0bd1050d519ac4b1da8d76d2feadde0782499561).
{{< /details >}}
{{< details id="filelight" title="filelight" link="https://commits.kde.org/filelight" >}}
+ Disable the context menu on fake shapes. [Commit](http://commits.kde.org/filelight/aac344478ee60edbfc32c52871d2d81c55590541). Fixes bug [#487930](https://bugs.kde.org/487930).
+ Manually position tooltip so it's correct. [Commit](http://commits.kde.org/filelight/062b80adb44f0d1c9dc41dcd3e23a9ccb4a60994). Fixes bug [#487339](https://bugs.kde.org/487339).
+ Use a better name than object. [Commit](http://commits.kde.org/filelight/bc44cee8336f3ca844247c0e729099addfd98ae8).
+ Assert or continue. [Commit](http://commits.kde.org/filelight/3fd2b05eb35658c413640b03acc982855421f683).
+ Repair action icons after qt6 port. [Commit](http://commits.kde.org/filelight/2224714d563666ecbe5540564b287d4d5db0707b).
+ Windows: remove unused kdewin dep. [Commit](http://commits.kde.org/filelight/6f81d05c6f8713676ff815126ef185beecdbd404).
+ Style++. [Commit](http://commits.kde.org/filelight/b481f9581d6275336d686265ad4ce9de76e3dd88).
+ Windows: treat unpinned files as compressed. [Commit](http://commits.kde.org/filelight/5d345574db3f5a142ef00fcfd575b93ce89353d3). Fixes bug [#486549](https://bugs.kde.org/486549).
{{< /details >}}
{{< details id="granatier" title="granatier" link="https://commits.kde.org/granatier" >}}
+ Avoid Qt warning about QPixmap::scaled used with a null pixmap. [Commit](http://commits.kde.org/granatier/bb80cc363f9acde8ec03038061de511274d1db05).
+ Arena selector: use default theme info from themeProvider, not own lookup. [Commit](http://commits.kde.org/granatier/aea97613d0051ef9ad28f2ae24eceab06ddd725b).
+ Player rendering cache: use KGameTheme-compatible theme id. [Commit](http://commits.kde.org/granatier/bcdbd60975d5afe050a52a19f3cc5ec3a918fe1d).
{{< /details >}}
{{< details id="itinerary" title="itinerary" link="https://commits.kde.org/itinerary" >}}
+ Fix disabling development mode. [Commit](http://commits.kde.org/itinerary/3de9a48f8f45480cd0d9170731d5ae7ad71a2ca9).
+ Fix storing the arrival time when editing a ferry reservation. [Commit](http://commits.kde.org/itinerary/dc898e36a96fb97e22c563335b61e0b3e3c43f97). Fixes bug [#487885](https://bugs.kde.org/487885).
+ Fix notifyrc qrc path. [Commit](http://commits.kde.org/itinerary/9b9fc33742f500e36bd8e12e7d0e5fdc4ee93646).
+ Less eagerly continue trip group searching after finding a loop. [Commit](http://commits.kde.org/itinerary/6cc729000300f7927366ed8deedd0ca7cb250f79).
+ Make sure we only show arrival delays if we show the arrival time. [Commit](http://commits.kde.org/itinerary/c37743d36613a198d7c8bbd4b094ff5f76c3b51c).
+ Work around Kirigami Addons now having a QtWidgets dependency. [Commit](http://commits.kde.org/itinerary/778521d586b2437a8211e7471ebb09f517d7aa9e).
+ Add Google Play publishing job template. [Commit](http://commits.kde.org/itinerary/da1e6d01a3e4226d400cbd9acd256d3ff066193a).
+ Work around Emoji fonts configuration being broken in Flatpak. [Commit](http://commits.kde.org/itinerary/e8f1163c06382fa4c2b179a7e0dacaca35b44136). See bug [#487245](https://bugs.kde.org/487245).
+ Fix FreeBSD asserts due to QCoreApplication being used during teardown. [Commit](http://commits.kde.org/itinerary/537df85c692d3fe7a6fb2bcfe0cc543acb627ab1).
+ Fix invalid XML in SVG icons. [Commit](http://commits.kde.org/itinerary/2b64b51dcb0d89dd6f20ecdaf6565960f5699c84).
{{< /details >}}
{{< details id="kaddressbook" title="kaddressbook" link="https://commits.kde.org/kaddressbook" >}}
+ Fix: remove duplicate entry. [Commit](http://commits.kde.org/kaddressbook/f7c942462f8619697ef9c71ebe694bd9158a09e5).
+ Explicitly link against QtDBus. [Commit](http://commits.kde.org/kaddressbook/d7aee568129e0f50dd40cd778c8788813508dfc7).
{{< /details >}}
{{< details id="kalm" title="kalm" link="https://commits.kde.org/kalm" >}}
+ Fix Appstream for new release. [Commit](http://commits.kde.org/kalm/a01e90e6e7d8ffa78078767a09125e7a50f670c1).
{{< /details >}}
{{< details id="kate" title="kate" link="https://commits.kde.org/kate" >}}
+ Guard more dbus headers. [Commit](http://commits.kde.org/kate/b3d319a55ddcfbf8b96df0bce2bad5f0eadc5d29).
+ Add missing KWindowSystem include. [Commit](http://commits.kde.org/kate/555eeac59cd1c00ecbe3611a182edc27deb3b28f).
+ Guard fillinRunningKateAppInstances. [Commit](http://commits.kde.org/kate/591f426acaba033611b761f62d7b43a506d054c9).
+ Guard dbus header. [Commit](http://commits.kde.org/kate/1d226c0c97638252a79f702a4bfdd76c3b16751d).
+ Guard dbus app adaptor. [Commit](http://commits.kde.org/kate/499d1debd619b279174bb3316616f25d25adf7db).
{{< /details >}}
{{< details id="kblocks" title="kblocks" link="https://commits.kde.org/kblocks" >}}
+ Fix support for themes packaged in subdirs, find sound files again. [Commit](http://commits.kde.org/kblocks/78f0235d190e86bba7efbc92cfca784fb60c4ce9).
{{< /details >}}
{{< details id="kcalc" title="kcalc" link="https://commits.kde.org/kcalc" >}}
+ Add edge case handler for stack reduction. [Commit](http://commits.kde.org/kcalc/1e2716452a5a29a9a5bbca92b19afcda3a4f16a3). Fixes bug [#487614](https://bugs.kde.org/487614).
{{< /details >}}
{{< details id="kcalutils" title="kcalutils" link="https://commits.kde.org/kcalutils" >}}
+ Fix unit test with KF 6.3 embedded Breeze icons. [Commit](http://commits.kde.org/kcalutils/9acb554df561c6b100899311884b9c8d9c62b69a).
{{< /details >}}
{{< details id="kcolorchooser" title="kcolorchooser" link="https://commits.kde.org/kcolorchooser" >}}
+ Allow dbus processing in qt-base to enable color-picking via portal. [Commit](http://commits.kde.org/kcolorchooser/7a03f1ccac935cb9c1a8e030a198d9267b0479f1). Fixes bug [#479406](https://bugs.kde.org/479406).
{{< /details >}}
{{< details id="kdeconnect-kde" title="kdeconnect-kde" link="https://commits.kde.org/kdeconnect-kde" >}}
+ Make clang-format happy. [Commit](http://commits.kde.org/kdeconnect-kde/f33abeefb356aa3e249406df5ab9e9cefeee5895).
+ Add Qt::DBus dependency to kdeconnect_runcommand_config. [Commit](http://commits.kde.org/kdeconnect-kde/5b17aa28067a7edee3a29177ae38f0db6b56ae81).
{{< /details >}}
{{< details id="kdenlive" title="kdenlive" link="https://commits.kde.org/kdenlive" >}}
+ Don't try renaming sequence on double click in empty area of timeline tab bar. [Commit](http://commits.kde.org/kdenlive/3ff481deebad133a2eb1affafc00eb22612fb1a6).
+ Fix deletion of wrong effect wihh multiple instances of an effect and group effects enabled. [Commit](http://commits.kde.org/kdenlive/03c3e2f2aef6d22bfcf872326aabf2213c5d879f).
+ Fix single selected clip disappearing from timeline when dragging a new clip in timeline. [Commit](http://commits.kde.org/kdenlive/2ca54a3904383089be7a41603623866aceb275a9).
+ [cmd rendering] Ensure proper kdenlive_render path for AppImage. [Commit](http://commits.kde.org/kdenlive/70c4b029f501da660b8ae7bf96069bb704634c97).
+ Fix freeze/crash on undo/redo track insertion. [Commit](http://commits.kde.org/kdenlive/ccb0e2bec67699837555826da1f3edd39df08c95).
+ Fix crash on undo/redo multiple track insertion. [Commit](http://commits.kde.org/kdenlive/7190ef37dfaa445855ea65509c00426b114cb081).
+ Project settings: don't list embedded title clips as empty files in the project files tab. [Commit](http://commits.kde.org/kdenlive/63df37b4d69b6170eced5a7183401c333324dc1a).
+ Fix undo move effect up/down. On effect move, also move the active index, increase margins between effects. [Commit](http://commits.kde.org/kdenlive/b89a60c45bd4337cfd3f8aa20ce81798ad5b4133).
+ Fix removing a composition from favorites. [Commit](http://commits.kde.org/kdenlive/8a13b96f643b06bd3062fb4da170a6a97285ef10).
+ Properly activate effect when added to a timeline clip. [Commit](http://commits.kde.org/kdenlive/ca7b9058b8f17e1791d4c42dbbc7d56f5ef53ed4).
+ Fix spacer tool can move backwards and overlap existing clips. [Commit](http://commits.kde.org/kdenlive/fd7a5b5d46067e632aaedc4ffb5efb7546694a3a).
+ Fix crash deleting subtitle when the file url was selected. [Commit](http://commits.kde.org/kdenlive/1b48bb0919472bd29aee7acea7b37e546ff645ff). Fixes bug [#487872](https://bugs.kde.org/487872).
+ Fix build when using openGLES. [Commit](http://commits.kde.org/kdenlive/c8efd29521ce1a945e29e134364c2aeb0e1b76d3). Fixes bug [#483425](https://bugs.kde.org/483425).
+ Fix possible crash on project opening. [Commit](http://commits.kde.org/kdenlive/2519cfcaadd9fa4cf14f21729a4c162e0f242c82).
+ Fix extra dash added to custom clip job output. [Commit](http://commits.kde.org/kdenlive/51d4aab3ba1fcbeefa55ff9e635b469758b7dfce). See bug [#487115](https://bugs.kde.org/487115).
+ Fix usage of QUrl for LUT lists. [Commit](http://commits.kde.org/kdenlive/75f150856323c0fff215155d15575e1715e5dc1e). See bug [#487375](https://bugs.kde.org/487375).
+ Fix default keyframe type referencing the old deprecated smooth type. [Commit](http://commits.kde.org/kdenlive/75d07595c5da760b116a3d72919a334336902b8c).
+ Be more clever splitting custom ffmpeg commands around quotes. [Commit](http://commits.kde.org/kdenlive/f9b61a0adc6d89a4248a858b44b7a7c24537c803). See bug [#487115](https://bugs.kde.org/487115).
+ Fix effect name focus in save effect. [Commit](http://commits.kde.org/kdenlive/7a29ef962e4917b001bc1989183860e180763e2e). See bug [#486310](https://bugs.kde.org/486310).
+ Fix tests. [Commit](http://commits.kde.org/kdenlive/80f5213bbd1d0800f96c3414217278d76868239d).
+ Fix selection when cutting an unselected clip under mouse. [Commit](http://commits.kde.org/kdenlive/c7f6cb483d79e179fd25e2339ba3cbcaea4aef69).
+ Fix loading timeline clip with disabled stack should be disabled. [Commit](http://commits.kde.org/kdenlive/50df3927a741a8d1619a3535b48fb751cc22e3ff).
+ Fix crash trying to save effect with slash in name. [Commit](http://commits.kde.org/kdenlive/5f20e207267244c702c0980d8b86b291da8a3473). Fixes bug [#487224](https://bugs.kde.org/487224).
+ Remove quotes in custom clip jobe, fix progress display. [Commit](http://commits.kde.org/kdenlive/152f36d89141813423f82b8ba57e0f9d42d57c4e). See bug [#487115](https://bugs.kde.org/487115).
+ Fix setting sequence thumbnail from clip monitor. [Commit](http://commits.kde.org/kdenlive/68d27e7683e65fcae17e35bc0bb3fbf3a1e3534f).
+ Fix locked track items don't have red background on project open. [Commit](http://commits.kde.org/kdenlive/30e7af218691936041bb1c3dc00964222230376d).
+ Fix spacer tool doing fake moves with clips in locked tracks. [Commit](http://commits.kde.org/kdenlive/dc954530a2450a9a8e88bfa0a162dd0ca16c611a).
+ Hide timeline clip status tooltip when mouse leaves. [Commit](http://commits.kde.org/kdenlive/96b4a57f94c536e7b5a8a27b87a070edf6ff087c).
{{< /details >}}
{{< details id="keditbookmarks" title="keditbookmarks" link="https://commits.kde.org/keditbookmarks" >}}
+ Make D-Bus dependency explicit. [Commit](http://commits.kde.org/keditbookmarks/58531be996667fca6f2298e59b229a21aa0ccacc).
{{< /details >}}
{{< details id="khealthcertificate" title="khealthcertificate" link="https://commits.kde.org/khealthcertificate" >}}
+ FreeBSD Qt 6.7 seems to need a QCoreApplication for accessing TLS features. [Commit](http://commits.kde.org/khealthcertificate/672e7261ba41005ad8d8e87ffbc56fe9c2bfc715).
{{< /details >}}
{{< details id="kidentitymanagement" title="kidentitymanagement" link="https://commits.kde.org/kidentitymanagement" >}}
+ Search Qt6DBus. [Commit](http://commits.kde.org/kidentitymanagement/2c797ab29a0ba08b9a41fe0d187f18c0e312d074).
{{< /details >}}
{{< details id="kio-extras" title="kio-extras" link="https://commits.kde.org/kio-extras" >}}
+ Don't build filenamesearch on Windows/macOS. [Commit](http://commits.kde.org/kio-extras/e1c61cd282a15758a05f475616606789351d9892).
+ [filenamesearch] Explicitly link against QtDBus. [Commit](http://commits.kde.org/kio-extras/e034d8f92a17941cd34636f2e73c9d294c6ca28f).
+ [kcms/webshortcuts] Explicitly link against QtDBus. [Commit](http://commits.kde.org/kio-extras/162091a397dcb8c378abc2bccec4d5d5d26b3732).
+ [kcms/proxy] Explicitly link against QtDBus. [Commit](http://commits.kde.org/kio-extras/6fe5fb61fd768a77d5bded0919337fd301243ada).
{{< /details >}}
{{< details id="kitinerary" title="kitinerary" link="https://commits.kde.org/kitinerary" >}}
+ Deal with Flixbus disruption notes. [Commit](http://commits.kde.org/kitinerary/d601d815e12602c4547fc5c6ae0e180a6dc66570).
+ Fix wrong results for Flixbus multi-column seat information. [Commit](http://commits.kde.org/kitinerary/7eb4499823b9176b452de6789ad0b2c44868be5e).
+ Handle more Flixbus ticket token variants in JSON-LD data. [Commit](http://commits.kde.org/kitinerary/c6da9c7a2576d58acdbfe458bcada93511d8ccaa).
+ Loosen the Flixbus extractor trigger pattern a bit. [Commit](http://commits.kde.org/kitinerary/e2caafcbbfb129a3eb4bf03f1680bcaff394adcb).
+ Special-case comparing Flixbus ticket tokens. [Commit](http://commits.kde.org/kitinerary/8a7a2c81106e0341025d1778525b5b842b6e45f5).
+ Handle Air Asia booking email with departure terminals. [Commit](http://commits.kde.org/kitinerary/6af6f1957c203762b4d370fd696d5e9a046b2b3a).
+ Add generic extractor for ERA FCB customer card data. [Commit](http://commits.kde.org/kitinerary/0c7ff2272958948d4c940b7f4216328eab811c51).
+ Decode ERA FCB CustomerCardData validity time ranges. [Commit](http://commits.kde.org/kitinerary/0fe5e30b01050fa40a055ad776ce9def177d4ec1).
+ Support Amadeus Cytric booking confirmations. [Commit](http://commits.kde.org/kitinerary/9cd977918987a8fa289a337b9ebd1b9ba84a24a2).
+ Handle German language variants of Trenitalia tickets. [Commit](http://commits.kde.org/kitinerary/9c5a3a850b7f8cade4690a1968ef280440c4493f).
+ Remove implausible time zones during post-processing. [Commit](http://commits.kde.org/kitinerary/fafe7a13d7413745bd379f49eb358d6cba400b07).
+ Fix filtering of invalid IATA BCBP child nodes. [Commit](http://commits.kde.org/kitinerary/cd4d5be3e1acfe8c9f21030249dcc256b1361668).
+ Make the Eurostar PDF extractor more robust against layout variations. [Commit](http://commits.kde.org/kitinerary/19eeef23d07e3d6d879f4f4034b90db2bf4bec10).
{{< /details >}}
{{< details id="kjumpingcube" title="kjumpingcube" link="https://commits.kde.org/kjumpingcube" >}}
+ Remove unused kcfg_* name from settings.ui QGroupBox item. [Commit](http://commits.kde.org/kjumpingcube/bce1451a2f6c35a93e1005f4acc5cec8c32e614f).
{{< /details >}}
{{< details id="kleopatra" title="kleopatra" link="https://commits.kde.org/kleopatra" >}}
+ Prefer -symbolic momochrome system tray icon if exists. [Commit](http://commits.kde.org/kleopatra/eed159ebbca61e2188e3132fb7ab26f0b1c9863b).
+ Revert "Add config option for adding a designated revoker for all new keys". [Commit](http://commits.kde.org/kleopatra/271636f4c19f4d00fb62b6ee9fdc438e302ce2fb).
{{< /details >}}
{{< details id="klines" title="klines" link="https://commits.kde.org/klines" >}}
+ Fix bonus for hidden preview not being conserved after new game. [Commit](http://commits.kde.org/klines/b27867848342368ad781dc24a5f66c53425014d1). Fixes bug [#487737](https://bugs.kde.org/487737).
+ Egyptian theme: remove broken <use> from SVG referring to non-existing id. [Commit](http://commits.kde.org/klines/d39d1779761c10442c832a31f098bb3d4022afbe).
{{< /details >}}
{{< details id="kmahjongg" title="kmahjongg" link="https://commits.kde.org/kmahjongg" >}}
+ SVG app icon: drop broken <use> entries href'ing to non-existing id. [Commit](http://commits.kde.org/kmahjongg/5eadebc95795957a3b00dbf2385e03bb66d12675).
{{< /details >}}
{{< details id="kmailtransport" title="kmailtransport" link="https://commits.kde.org/kmailtransport" >}}
+ Search QtDBus. [Commit](http://commits.kde.org/kmailtransport/08961096d03710e6a82303ec7b9ef96a7f61230a).
{{< /details >}}
{{< details id="kmines" title="kmines" link="https://commits.kde.org/kmines" >}}
+ Graveyard Mayhem theme: remove left-over empty <g> from SVG. [Commit](http://commits.kde.org/kmines/6b8f02fca1deaf0af1a24059f8bee180868da174).
{{< /details >}}
{{< details id="kmplot" title="kmplot" link="https://commits.kde.org/kmplot" >}}
+ Explicitly link against Qt::DBus. [Commit](http://commits.kde.org/kmplot/79f6b1bad61982c3f11cbe3b9ccba99d51ccb20d).
{{< /details >}}
{{< details id="knights" title="knights" link="https://commits.kde.org/knights" >}}
+ Remove left-over debug output for background key. [Commit](http://commits.kde.org/knights/9ebaa9ac49acef691d945f061e26c3d5de6301c3).
+ Skip updating graphics if there is no scene size yet. [Commit](http://commits.kde.org/knights/feafe75d28896bfdea0d7c80933c29950237736d).
{{< /details >}}
{{< details id="kolourpaint" title="kolourpaint" link="https://commits.kde.org/kolourpaint" >}}
+ Fix frameworks detection. [Commit](http://commits.kde.org/kolourpaint/3a18dd3d7ed4deec584b7b583c2c1ac707615b81).
{{< /details >}}
{{< details id="kontact" title="kontact" link="https://commits.kde.org/kontact" >}}
+ Explicitly link against QtDBus. [Commit](http://commits.kde.org/kontact/961f54877f3ad78c09f0fe64a90321f7648620a6).
{{< /details >}}
{{< details id="kontactinterface" title="kontactinterface" link="https://commits.kde.org/kontactinterface" >}}
+ Search Qt6DBus. [Commit](http://commits.kde.org/kontactinterface/eaee22b15904929d552afd6a25b23d92ecfa1053).
+ Explicitly link against QtDBus. [Commit](http://commits.kde.org/kontactinterface/b912b93da71be3e80cb03e5374d9b5e19080f12b).
{{< /details >}}
{{< details id="konversation" title="konversation" link="https://commits.kde.org/konversation" >}}
+ Provide scripts with an env var to find qdbus. [Commit](http://commits.kde.org/konversation/9f0a87cbc0376bb188076c9c23c09e337754e9ce). Fixes bug [#349675](https://bugs.kde.org/349675).
{{< /details >}}
{{< details id="kopeninghours" title="kopeninghours" link="https://commits.kde.org/kopeninghours" >}}
+ Update python package version. [Commit](http://commits.kde.org/kopeninghours/3be698261df327dc4ee0af7f5fac57a4254c9b49).
{{< /details >}}
{{< details id="kpimtextedit" title="kpimtextedit" link="https://commits.kde.org/kpimtextedit" >}}
+ Backport fix handling of nested lists. [Commit](http://commits.kde.org/kpimtextedit/91054841da48e1f9c52caf5be5efd4273349bc32).
{{< /details >}}
{{< details id="kpmcore" title="kpmcore" link="https://commits.kde.org/kpmcore" >}}
+ Fix detection of MBR partition table with zeroed signature. [Commit](http://commits.kde.org/kpmcore/2acc26ca582f6556a682881e624a7ae5deb1711d). Fixes bug [#487650](https://bugs.kde.org/487650).
{{< /details >}}
{{< details id="kpublictransport" title="kpublictransport" link="https://commits.kde.org/kpublictransport" >}}
+ Update DB and ÖBB coverage data from Transport API repository. [Commit](http://commits.kde.org/kpublictransport/38a5dc7a13421cde6230e22d3e2724f663d16e3d).
{{< /details >}}
{{< details id="krdc" title="krdc" link="https://commits.kde.org/krdc" >}}
+ VNC: replace m_stopped with QThread's interruptionRequested. [Commit](http://commits.kde.org/krdc/e6a169e40b80b4fcd5e42dbba17d82f3f7bf4feb). Fixes bug [#486178](https://bugs.kde.org/486178).
{{< /details >}}
{{< details id="krfb" title="krfb" link="https://commits.kde.org/krfb" >}}
+ Wayland: Add support for the persistence feature of the remote desktop portal. [Commit](http://commits.kde.org/krfb/01739fbf313d54abb1afb12367e7772b39a4dbfc).
{{< /details >}}
{{< details id="ksirk" title="ksirk" link="https://commits.kde.org/ksirk" >}}
+ Avoid runtime warning about missing argument in strings from ui file. [Commit](http://commits.kde.org/ksirk/7ba346ab25a89acd41527eb609abba1e6a5f57e3).
{{< /details >}}
{{< details id="kturtle" title="kturtle" link="https://commits.kde.org/kturtle" >}}
+ Explicitly link against Qt::DBus. [Commit](http://commits.kde.org/kturtle/bf96e9f9516e43f5cab314fc62935e161f11cdf8).
{{< /details >}}
{{< details id="kubrick" title="kubrick" link="https://commits.kde.org/kubrick" >}}
+ Settings dialog: fix layout parenting accidentally to child widget. [Commit](http://commits.kde.org/kubrick/6ecec7edda88dcba31ffed2035850f37f3ce1ba0).
{{< /details >}}
{{< details id="kwordquiz" title="kwordquiz" link="https://commits.kde.org/kwordquiz" >}}
+ Add missing KF::DBusAddons dependency. [Commit](http://commits.kde.org/kwordquiz/1a926e528d792eaad4e80ce742793587be6b0c0a).
{{< /details >}}
{{< details id="libkdegames" title="libkdegames" link="https://commits.kde.org/libkdegames" >}}
+ Fix KGameThemeProvider to also deal with uninstalled/removed themes. [Commit](http://commits.kde.org/libkdegames/0d596ae45ed098ed48d71e249efa2aa1623ffff7).
{{< /details >}}
{{< details id="lokalize" title="lokalize" link="https://commits.kde.org/lokalize" >}}
+ Fix language tool not working. [Commit](http://commits.kde.org/lokalize/fff48e0cc6eef46330b6ec17511993af27735d89). Fixes bug [#458681](https://bugs.kde.org/458681).
{{< /details >}}
{{< details id="messagelib" title="messagelib" link="https://commits.kde.org/messagelib" >}}
+ Revert "Mark encrypted part as mime". [Commit](http://commits.kde.org/messagelib/4582efa45cb5614e50356829474c85cfd37f07d7). Fixes bug [#487029](https://bugs.kde.org/487029).
{{< /details >}}
{{< details id="neochat" title="neochat" link="https://commits.kde.org/neochat" >}}
+ Fix keyboard navigation on search pages. [Commit](http://commits.kde.org/neochat/e54fef747667b6cf9482ae10b5c66ec6043af60c).
+ Fix the tooltips for the two drawer buttons at the top. [Commit](http://commits.kde.org/neochat/a96e3d00fcef5d7e2dcf27e3c7aca2fa8b3796d1).
+ Fix keyboard navigation in space drawer. [Commit](http://commits.kde.org/neochat/67f5c0b1ed4d2fb83f929da89e2d1904e7d3440f).
+ Add keyboard navigation for server selection in room search dialog. [Commit](http://commits.kde.org/neochat/b097f1c0ec1f5f6bf89ec2498270f3ef258ace4f).
+ Remove room member highlight on click. [Commit](http://commits.kde.org/neochat/1467e4395696320ae21a998fada8f42e87ea5e09).
+ Use Qt.alpha in ThemeRadioButton. [Commit](http://commits.kde.org/neochat/c8c26a0b237d71c7bab9e02f99b31f78f0ff29d5).
+ Add focus border for the theme radio button, used on the Appearance page. [Commit](http://commits.kde.org/neochat/f344a8d690d2faa543de4e39bc69a650b68e03fa).
+ Fix QR code not showing when tapping the button under account settings. [Commit](http://commits.kde.org/neochat/696ce3af5e5bccdfa517c806546883b4f5fa6377).
+ Fix map copyright link activation. [Commit](http://commits.kde.org/neochat/5c220f3c53a25682de9ba2a6703bf0094114ce2d).
+ Don't show the map if there's no locations available. [Commit](http://commits.kde.org/neochat/e7fe65bf57095e3ddc9db9f508fc17c54cf93a43).
+ Fixup AttachDialog. [Commit](http://commits.kde.org/neochat/321bc293f3ab80d6bcc6f2680cdf13aa1ea0147b).
+ CreateRoomDialog: Add missing formcard separators. [Commit](http://commits.kde.org/neochat/e1448a54783e989be8463d5c0a08c36d336e2a72).
+ Fix if X11 on apple. [Commit](http://commits.kde.org/neochat/13846d485b9e0dbe595af6f0feff0061620c6f95).
+ Cleanup quick switcher. [Commit](http://commits.kde.org/neochat/c8d53b9e794ac42646f365dd774d014a1833f12c).
+ Fix spacing of HiddenDelegate. [Commit](http://commits.kde.org/neochat/f8ff0dd3ce9b1bbd5e91f8fd554731e9b6722036).
+ Fix micro spacing inconsistency in SpaceHierarchyDelegate. [Commit](http://commits.kde.org/neochat/e577af65d3e535fb17db8a5940704879dc157733).
{{< /details >}}
{{< details id="okular" title="okular" link="https://commits.kde.org/okular" >}}
+ Dvi: Add another check for index to glyphtable being in range. [Commit](http://commits.kde.org/okular/b071e49cc109c8e46787b4984acffceed17282e6).
{{< /details >}}
{{< details id="palapeli" title="palapeli" link="https://commits.kde.org/palapeli" >}}
+ Work around wrong warning of KWidgetItemDelegate about deleted widgets. [Commit](http://commits.kde.org/palapeli/eaf483c594881a25da14f0b02b708e5fb97f2628).
+ Avoid Qt warnings about removing items from a QGraphicsScene not part of. [Commit](http://commits.kde.org/palapeli/5c2cf1b4fe20076ee4982c6457f90a4e8fb06a8e).
{{< /details >}}
{{< details id="spectacle" title="spectacle" link="https://commits.kde.org/spectacle" >}}
+ AnnotationDocument: Fix highlighter tool not blending with base image. [Commit](http://commits.kde.org/spectacle/0c215c788e38b39b1126af14ab0c6b3187f15711). Fixes bug [#488025](https://bugs.kde.org/488025).
{{< /details >}}
{{< details id="telly-skout" title="telly-skout" link="https://commits.kde.org/telly-skout" >}}
+ Fix Appstream for new release. [Commit](http://commits.kde.org/telly-skout/a39b7981afeee729a734167b12867133cba2c3f9).
{{< /details >}}
{{< details id="tokodon" title="tokodon" link="https://commits.kde.org/tokodon" >}}
+ Support PageUp/PageDown keys to quickly navigate the timeline. [Commit](http://commits.kde.org/tokodon/297ceb0c191a3a63100cc6d55b85df251a312d2f). Fixes bug [#487507](https://bugs.kde.org/487507).
+ 🍒 Fix possible segfault after logout on timeline pages. [Commit](http://commits.kde.org/tokodon/ffa2c8fa35631483bea91cfb2f58e7e9eddcc933).
+ Only clear page stack if authentication was successful. [Commit](http://commits.kde.org/tokodon/e2b44dda2185437937e132b2004fe901dfaaa4e7).
+ Don't allow continuing without inputting an authorization code. [Commit](http://commits.kde.org/tokodon/d08f5b09defee6716f898ee4ee2648677e8b01a2).
+ 🍒 Fix non-functional authorization link. [Commit](http://commits.kde.org/tokodon/d2307efadfdb764792201dd4480dbf276005ec1b).
+ Fix wrong "%1 boosted" label on certain posts. [Commit](http://commits.kde.org/tokodon/0003fa67e0c2a309f1b7f727ef15d635da6663dd). Fixes bug [#485637](https://bugs.kde.org/485637).
{{< /details >}}
