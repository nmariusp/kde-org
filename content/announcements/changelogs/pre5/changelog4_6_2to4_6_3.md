---
aliases:
- ../changelog4_6_2to4_6_3
hidden: true
title: KDE 4.6.3 Changelog
---

<h2>Changes in KDE 4.6.3</h2>
    <h3 id="kdebase"><a name="kdebase">kdebase</a><span class="allsvnchanges"> [ <a href="4_6_3/kdebase.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="konsole">konsole</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Correct issue where new tabs start in wrong directories. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=227156">227156</a>.  See Git commit <a href="http://commits.kde.org/konsole/edec4bedbc9a3ed49d79ac31a5c3d425a47dc47b">edec4be</a>. </li>
        <li class="bugfix normal">Allow profile filenames to have multiple '.'s. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=250399">250399</a>.  See Git commit <a href="http://commits.kde.org/konsole/74c3ea27cc00b9469375c5bc3f800298cc3db254">74c3ea2</a>. </li>
        <li class="bugfix normal">Restore 'Close Tab' on the tab context menu and the close tabbar button. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=267896">267896</a>.  See Git commit <a href="http://commits.kde.org/konsole/05367a7e7463435e7b0a50827875f6dd6ee1940c">05367a7</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdemultimedia"><a name="kdemultimedia">kdemultimedia</a><span class="allsvnchanges"> [ <a href="4_6_3/kdemultimedia.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kmix">kmix</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Fix memory leak when DialogSelectMaster dialogs are closed. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=260926">260926</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1227026&amp;view=rev">1227026</a>. </li>
      </ul>
      </div>
      <h4><a href="http://developer.kde.org/~wheeler/juk.html" name="juk">JuK</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Use the proper directory-only file dialog for the file renamer's music folder. Patch by Giorgos Kylafas. See SVN commit <a href="http://websvn.kde.org/?rev=1228552&amp;view=rev">1228552</a>. </li>
        <li class="bugfix normal">Allow translated locales to insert Artist, Genre, Year categories to file renamer. Patch by Giorgos Kylafas. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=236993">236993</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1228556&amp;view=rev">1228556</a>. </li>
        <li class="bugfix severe">Fix collection changes altering the tag editor track title, artist name, or genre if the tag editor is open. Patch by Giorgos Kylafas. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=264092">264092</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=271054">271054</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1229047&amp;view=rev">1229047</a>. </li>
        <li class="bugfix normal">Use appropriate translation routine for some strings in the File Renamer. Patch by Giorgos Kylafas. See SVN commit <a href="http://websvn.kde.org/?rev=1229136&amp;view=rev">1229136</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeplasmaaddons"><a name="kdeplasmaaddons">kdeplasmaaddons</a><span class="allsvnchanges"> [ <a href="4_6_3/kdeplasmaaddons.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="lancelot">Lancelot</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Fix untranslated strings in gui. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=257520">257520</a>.  See Git commit <a href="http://commits.kde.org/kdeplasma-addons/06df2f1abebd166397d1c29262614cdc04ecf21e">06df2f1</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdenetwork"><a name="kdenetwork">kdenetwork</a><span class="allsvnchanges"> [ <a href="4_6_3/kdenetwork.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kopete">kopete</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Fix Now Listening plugin does not work with amarok or juk. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=155865">155865</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1226904&amp;view=rev">1226904</a>. </li>
        <li class="bugfix normal">Fix Now Listening advertising mode "Append to your status message" keeps adding song names. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=222147">222147</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=261989">261989</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=121097">121097</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1227262&amp;view=rev">1227262</a>. </li>
        <li class="bugfix normal">Improves Statistics plugin startup and shutdown times. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=117989">117989</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=138903">138903</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1227534&amp;view=rev">1227534</a>. </li>
        <li class="bugfix normal">Make Yahoo/Wlm mail notifications persistent. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=240595">240595</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1227624&amp;view=rev">1227624</a>. </li>
        <li class="bugfix normal">Fix for do not flash tray icon when users leave chat sessions. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=187977">187977</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1227629&amp;view=rev">1227629</a>. </li>
        <li class="bugfix normal">Provides close button for tabs in chat windows. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=224388">224388</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1228391&amp;view=rev">1228391</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeworkspace"><a name="kdeworkspace">kdeworkspace</a><span class="allsvnchanges"> [ <a href="4_6_3/kdeworkspace.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="krunner">krunner</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Fix SimulateUserActivity not working for a second when resuming from ram/disk. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=269737">269737</a>.  See Git commit <a href="http://commits.kde.org/kde-workspace/5c47f16d67065e239c380cc369a1da854acef19a">5c47f16</a>. </li>
        <li class="bugfix normal">Fix crash in Solid's Wid backend. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=271408">271408</a>.  See Git commit <a href="http://commits.kde.org/kde-workspace/a789bceeb1a62fb4d1b6ec9246f1e98fd96e6921">a789bce</a>. </li>
      </ul>
      </div>
    </div>