------------------------------------------------------------------------
r1216427 | ilic | 2011-01-23 23:26:18 +1300 (Sun, 23 Jan 2011) | 1 line

Ignore buttonGroup attribute of QToolButton. (bport: 1216426)
------------------------------------------------------------------------
r1216726 | scripty | 2011-01-25 01:20:19 +1300 (Tue, 25 Jan 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1217501 | shaforo | 2011-01-28 04:36:07 +1300 (Fri, 28 Jan 2011) | 4 lines

the most basic workaround for sqlite leaking (see QTBUG-16967)
this makes lokalize consume 7megs less on a database as large as 48 megs


------------------------------------------------------------------------
r1217582 | shaforo | 2011-01-28 12:11:34 +1300 (Fri, 28 Jan 2011) | 2 lines

disable getting additional stats - it is not displayed anyway

------------------------------------------------------------------------
r1217764 | scripty | 2011-01-29 02:06:21 +1300 (Sat, 29 Jan 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1217788 | kossebau | 2011-01-29 05:58:01 +1300 (Sat, 29 Jan 2011) | 5 lines

fixed: crashed if the file to export to could not be opened

BUG: 264581
FIXED-IN: 0.6.1

------------------------------------------------------------------------
r1217790 | kossebau | 2011-01-29 06:19:55 +1300 (Sat, 29 Jan 2011) | 1 line

bump version number
------------------------------------------------------------------------
r1217887 | scripty | 2011-01-30 00:49:57 +1300 (Sun, 30 Jan 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1218155 | arichardson | 2011-02-01 08:23:47 +1300 (Tue, 01 Feb 2011) | 5 lines

Backport 1218154:

Move the call to setFixedFontByGlobalSettings() to right after the QTreeView is initialized. This ensures the font is set before the model and therefore avoids calculating the width of everything in the model.

In my tests this improved the startup time from just over 2 secs to round about 1 sec (Callgrind says from 8 billion instr down to 4)
------------------------------------------------------------------------
r1218179 | kossebau | 2011-02-01 11:09:11 +1300 (Tue, 01 Feb 2011) | 9 lines

fixed: crashed if an editor for any float type in the Decoding Table tool was closed while having uncomplete data

reason: QLineEdit only emits editingFinished() if validator() returns QValidator::Acceptable,
so in that case mEditor was not set to 0. Now catching deconstruction of any editor with QPointer.

BUG: 259867
FIXED-IN: 0.6.1


------------------------------------------------------------------------
r1218647 | shaforo | 2011-02-04 01:44:53 +1300 (Fri, 04 Feb 2011) | 4 lines

add pointer check
BUG:265118


------------------------------------------------------------------------
r1219104 | cullmann | 2011-02-07 00:07:51 +1300 (Mon, 07 Feb 2011) | 1 line

fix missing actions in tree view + backport some small fixes
------------------------------------------------------------------------
r1219108 | cullmann | 2011-02-07 00:24:46 +1300 (Mon, 07 Feb 2011) | 1 line

fix menu buttons for prev/next document and co, this should not change i18n, I only removed strings
------------------------------------------------------------------------
r1220104 | kossebau | 2011-02-13 12:25:38 +1300 (Sun, 13 Feb 2011) | 1 line

fixed: digitsFilledLimit wrong for KOctalByteCodec, should be dez32, not dez64 (partial backport)
------------------------------------------------------------------------
r1220147 | kossebau | 2011-02-13 13:54:56 +1300 (Sun, 13 Feb 2011) | 1 line

backport of 1220129: fixed: BinaryByteCodec::encodeShort() created empty string for value 0, not "0"
------------------------------------------------------------------------
r1220196 | kossebau | 2011-02-13 14:35:18 +1300 (Sun, 13 Feb 2011) | 1 line

backport of 1220190: fixed: OctalByteCodec::encodeShort() dropped the second digit if 0 and the first != 0
------------------------------------------------------------------------
r1220197 | kossebau | 2011-02-13 14:36:04 +1300 (Sun, 13 Feb 2011) | 1 line

backport of 1220194: fixed: DecimalByteCodec::encodeShort() dropped the second digit if 0 and the first != 0
------------------------------------------------------------------------
r1220407 | kossebau | 2011-02-14 10:31:47 +1300 (Mon, 14 Feb 2011) | 1 line

backport of 1220404: fixed: some currently used codepages have bytes without defined chars
------------------------------------------------------------------------
r1220411 | kossebau | 2011-02-14 10:47:55 +1300 (Mon, 14 Feb 2011) | 1 line

backport of 1220410: changed: use unicode replacementCharacter as DefaultUndefinedChar
------------------------------------------------------------------------
r1220425 | kossebau | 2011-02-14 12:09:44 +1300 (Mon, 14 Feb 2011) | 1 line

backport of 1220423: fixed: include all needed headers
------------------------------------------------------------------------
r1220426 | kossebau | 2011-02-14 12:12:13 +1300 (Mon, 14 Feb 2011) | 1 line

backport of 1220424: changed: use global DefaultUndefinedChar
------------------------------------------------------------------------
