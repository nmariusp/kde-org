------------------------------------------------------------------------
r878276 | ewoerner | 2008-10-31 18:53:35 +0000 (Fri, 31 Oct 2008) | 3 lines

Backport fixes from revision 830450 and revision 830455
BUG 169126

------------------------------------------------------------------------
r878293 | ewoerner | 2008-10-31 20:01:10 +0000 (Fri, 31 Oct 2008) | 3 lines

Backporting fix for reversed direction in cache settings
CCBUG: 171602

------------------------------------------------------------------------
r878664 | ltoscano | 2008-11-01 15:45:48 +0000 (Sat, 01 Nov 2008) | 6 lines

Backport SVN commit 876159 by ltoscano:

Fix a small rendering issue for the text in the "Timers" configuration
panel. Global settings are used now.


------------------------------------------------------------------------
r879447 | mueller | 2008-11-03 10:30:38 +0000 (Mon, 03 Nov 2008) | 2 lines

add python 2.6 support

------------------------------------------------------------------------
r881008 | scripty | 2008-11-07 07:37:38 +0000 (Fri, 07 Nov 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r882244 | scripty | 2008-11-10 07:36:24 +0000 (Mon, 10 Nov 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r883107 | scripty | 2008-11-12 08:15:18 +0000 (Wed, 12 Nov 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r884847 | ewoerner | 2008-11-16 01:10:16 +0000 (Sun, 16 Nov 2008) | 3 lines

Backport: Do not use qDebug() with anything that has side-effects
CCBUG: 175003

------------------------------------------------------------------------
r889452 | nielsslot | 2008-11-26 21:16:37 +0000 (Wed, 26 Nov 2008) | 10 lines

Backport commit 889448.

Fix for loops with a variable start or end value. The following KTurtle code should now work:

$x = 1
$y = 10
for $a = $x to $y {
 forward 10
}

------------------------------------------------------------------------
r891886 | scripty | 2008-12-03 08:23:12 +0000 (Wed, 03 Dec 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r892458 | rahn | 2008-12-04 12:46:19 +0000 (Thu, 04 Dec 2008) | 6 lines

Backporting fixes.

BUG: 176193
BUG: 176854


------------------------------------------------------------------------
r894250 | asimha | 2008-12-08 10:47:22 +0000 (Mon, 08 Dec 2008) | 6 lines

Backporting fix for Bug #175818 to 4.1 branch. Coordinates for Amiens
are now correct.

CCMAIL: kstars-devel@kde.org


------------------------------------------------------------------------
r894259 | asimha | 2008-12-08 11:01:44 +0000 (Mon, 08 Dec 2008) | 7 lines

Applying Médéric's patch fixing bug 157318. The theme's colours are
correctly set now. (Backporting to 4.1 branch)

CCMAIL: kstars-devel@kde.org



------------------------------------------------------------------------
r894598 | asimha | 2008-12-08 22:31:52 +0000 (Mon, 08 Dec 2008) | 13 lines

Fixing bug #164493. The trouble was that the coordinates of the object
were not refracted in SkyMap::checkVisibility(). The result was that
some stars that were supposed to be visible in the field of view were
not considered to be so at high zoom levels.

There are a lot of other refraction-related problems to investigate
into as well!

[Backporting fix to 4.1 branch]

CCMAIL: kstars-devel@kde.org


------------------------------------------------------------------------
r894694 | scripty | 2008-12-09 07:17:09 +0000 (Tue, 09 Dec 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r898710 | rdieter | 2008-12-18 20:12:17 +0000 (Thu, 18 Dec 2008) | 2 lines

add some pkgconfig love (fixes fedora)

------------------------------------------------------------------------
r899008 | kbal | 2008-12-19 14:56:11 +0000 (Fri, 19 Dec 2008) | 1 line

backported the icon fixes from trunk
------------------------------------------------------------------------
r899885 | cniehaus | 2008-12-21 19:21:34 +0000 (Sun, 21 Dec 2008) | 4 lines

Do not allow negative ranges in the plotting dialog. 

Patch by Kashyap R Puranik

------------------------------------------------------------------------
r906161 | asimha | 2009-01-05 17:35:59 +0000 (Mon, 05 Jan 2009) | 24 lines

Backporting bug-fix for the FOV symbol editor. There is still an issue
which I will resolve shortly.

Original commit log:
=============================
SVN commit 903047 by asimha:

Fixing localization issues.

+ We want to use only the correct decimal symbol while formatting FOV
  sizes and avoid using the appropriate digit spacing.

+ The ok button refused to accept correctly localized strings and was
  looking for the standard '.' as the decimal symbol. Fixing that.

The FOV editor now generates, and accepts strings of the form "1000.0"
(where the . represents the appropriate decimal symbol in the locale)

This closes Bug #179034
=============================

CCMAIL: kstars-devel@kde.org


------------------------------------------------------------------------
r906170 | asimha | 2009-01-05 17:52:12 +0000 (Mon, 05 Jan 2009) | 17 lines

Backporting further fix to bug 179034. Quoting the original commit
log:

----

Fixing (further) problems with the FOV editor. The localized versions
would accept FOVs that were formatted in the localized format (60,00
in German, for instance) but wouldn't work. This was because the same
bug had to be fixed in two other places.

The FOV editor now seems to work correctly.

----

CCMAIL: kstars-devel@kde.org


------------------------------------------------------------------------
r906171 | asimha | 2009-01-05 17:53:37 +0000 (Mon, 05 Jan 2009) | 5 lines

Version microbump for 4.1 branch.

CCMAIL: kstars-devel@kde.org


------------------------------------------------------------------------
r906173 | asimha | 2009-01-05 18:07:10 +0000 (Mon, 05 Jan 2009) | 11 lines

Backporting fix to bug 141372 to 4.1 branch. Original commit log
reads as follows:

Localizing Information / Image link titles in the detail dialog. (The
localized strings are already present as the same are used in the
popup menu.)

CCBUG: 141372
CCMAIL: kstars-devel@kde.org


------------------------------------------------------------------------
r906243 | asimha | 2009-01-05 19:51:31 +0000 (Mon, 05 Jan 2009) | 7 lines

Backporting fix from revision 902624:

Make the tab order in the Location Dialog more intuitive.

CCMAIL: kstars-devel@kde.org


------------------------------------------------------------------------
