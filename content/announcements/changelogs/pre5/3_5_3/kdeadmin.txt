2006-03-30 23:48 +0000 [r524704]  jriddell

	* branches/KDE/3.5/kdeadmin/knetworkconf/backends/platform.pl.in,
	  branches/KDE/3.5/kdeadmin/knetworkconf/backends/network.pl.in,
	  branches/KDE/3.5/kdeadmin/knetworkconf/backends/service.pl.in,
	  branches/KDE/3.5/kdeadmin/knetworkconf/backends/network-conf.in:
	  Ubuntu delayed, now version 6.06

2006-04-17 15:47 +0000 [r530779]  dfaure

	* branches/KDE/3.5/kdeadmin/kfile-plugins/deb/kfile_deb.desktop:
	  The mimetype is now x-deb (kdelibs defines both, with
	  x-debian-package being an alias for x-deb) BUG: 125694

2006-04-18 17:20 +0000 [r531200]  jriddell

	* branches/KDE/3.5/kdeadmin/kpackage/debAptInterface.cpp: Patch
	  from Debian * dash has an internal echo that does not know about
	  -e. force it to use /bin/echo instead. (Closes: #358296).

2006-05-04 08:12 +0000 [r537228]  mueller

	* branches/KDE/3.5/kdeadmin/kpackage/cache.cpp: fix memory leak
	  (CID 1715)

2006-05-23 09:36 +0000 [r543971]  mueller

	* branches/KDE/3.5/kdeadmin/kdat/KDat.cpp (removed),
	  branches/KDE/3.5/kdeadmin/kdat/TapeDrive.cpp,
	  branches/KDE/3.5/kdeadmin/kdat/main.cpp,
	  branches/KDE/3.5/kdeadmin/kdat/KDatMainWindow.h (added),
	  branches/KDE/3.5/kdeadmin/kdat/Node.cpp,
	  branches/KDE/3.5/kdeadmin/kdat/BackupProfileInfoWidget.cpp,
	  branches/KDE/3.5/kdeadmin/kdat/KDat.h (removed),
	  branches/KDE/3.5/kdeadmin/kdat/Makefile.am,
	  branches/KDE/3.5/kdeadmin/kdat/Tape.cpp,
	  branches/KDE/3.5/kdeadmin/kdat/KDatMainWindow.cpp (added): avoid
	  case-filename clash kdat.h vs KDat.h

2006-05-23 10:34 +0000 [r543997]  binner

	* branches/KDE/3.5/kdevelop/configure.in.in,
	  branches/KDE/3.5/kdesdk/kdesdk.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteversion.h,
	  branches/KDE/3.5/kdepim/kontact/src/main.cpp,
	  branches/arts/1.5/arts/configure.in.in,
	  branches/KDE/3.5/kdepim/akregator/src/aboutdata.h,
	  branches/KDE/3.5/kdepim/kmail/kmversion.h,
	  branches/KDE/3.5/kdelibs/README,
	  branches/KDE/3.5/kdepim/kaddressbook/kabcore.cpp,
	  branches/KDE/3.5/kdepim/kdepim.lsm,
	  branches/KDE/3.5/kdeutils/kcalc/version.h,
	  branches/KDE/3.5/kdevelop/kdevelop.lsm,
	  branches/KDE/3.5/kdebase/startkde,
	  branches/KDE/3.5/kdeaccessibility/kdeaccessibility.lsm,
	  branches/arts/1.5/arts/arts.lsm,
	  branches/KDE/3.5/kdeaddons/kdeaddons.lsm,
	  branches/KDE/3.5/kdeadmin/kdeadmin.lsm,
	  branches/KDE/3.5/kdelibs/kdelibs.lsm,
	  branches/KDE/3.5/kdenetwork/kdenetwork.lsm,
	  branches/KDE/3.5/kdeartwork/kdeartwork.lsm,
	  branches/KDE/3.5/kdemultimedia/kdemultimedia.lsm,
	  branches/KDE/3.5/kdebase/kdebase.lsm,
	  branches/KDE/3.5/kdelibs/kdecore/kdeversion.h,
	  branches/KDE/3.5/kdegames/kdegames.lsm,
	  branches/KDE/3.5/kdeedu/kdeedu.lsm,
	  branches/KDE/3.5/kdebindings/kdebindings.lsm,
	  branches/KDE/3.5/kdebase/konqueror/version.h,
	  branches/KDE/3.5/kdetoys/kdetoys.lsm,
	  branches/KDE/3.5/kdegraphics/kdegraphics.lsm,
	  branches/KDE/3.5/kdeutils/kdeutils.lsm: bump version for KDE
	  3.5.3

