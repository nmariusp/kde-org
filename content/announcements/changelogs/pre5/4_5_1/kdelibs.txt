------------------------------------------------------------------------
r1168696 | mueller | 2010-08-27 09:09:35 +0100 (dv, 27 ago 2010) | 2 lines

bump version

------------------------------------------------------------------------
r1168462 | dantti | 2010-08-26 19:08:59 +0100 (dj, 26 ago 2010) | 3 lines

Backport r1168413:
Fix bug #163693 by correcting KPageView's connection to selectionChanged() instead of currentChanged()

------------------------------------------------------------------------
r1168450 | trueg | 2010-08-26 18:13:50 +0100 (dj, 26 ago 2010) | 4 lines

Backport:
* remove value from cache instead of storing an invalid one.
* fall back to removing the property alltogether when setting an empty/invalid value

------------------------------------------------------------------------
r1168427 | mart | 2010-08-26 17:13:25 +0100 (dj, 26 ago 2010) | 2 lines

backport: delete statusNotifierWatcher only in serviceChange

------------------------------------------------------------------------
r1168078 | scripty | 2010-08-26 03:39:58 +0100 (dj, 26 ago 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1168060 | aseigo | 2010-08-26 02:26:20 +0100 (dj, 26 ago 2010) | 4 lines

make the checks consistent with the rest of the methods: check the count of d->pages, not the tabwidget proxy; use value() instead of [] as it is protected a
gainst OOB indexes
BUG:248976

------------------------------------------------------------------------
r1167997 | trueg | 2010-08-25 21:44:35 +0100 (dc, 25 ago 2010) | 2 lines

Backport: USe KUrl instead of QUrl to parse the links from the meta date items. This fixes usage of query items in the URLs (example: Nepomuk query URLs)

------------------------------------------------------------------------
r1167621 | mpyne | 2010-08-25 05:18:40 +0100 (dc, 25 ago 2010) | 11 lines

Backport two KSharedDataCache bugfixes to KDE Platform 4.5.1.

1. Do not attempt to free more pages than are actually allocated. This might
fix bug 243573, but I cannot get the testcase to crash. (I even disabled
desktop effect for maximum resizing speed ;)
2. Force the cache to have a certain minimum number of pages (currently 256) to
avoid crashes if the cache contains only a single page.

The commit log for the trunk commit, r1167620, has the detailed reasoning.
CCBUG:243573

------------------------------------------------------------------------
r1167553 | cfeck | 2010-08-24 22:37:06 +0100 (dt, 24 ago 2010) | 4 lines

Emit changes from QPlainTextEdit (backport r1167551)

CCBUG: 248693

------------------------------------------------------------------------
r1167508 | cfeck | 2010-08-24 20:50:56 +0100 (dt, 24 ago 2010) | 4 lines

Fix KArchive::copyTo() for large files (backport r1167506)

CCBUG: 237124

------------------------------------------------------------------------
r1167268 | dfaure | 2010-08-24 09:21:17 +0100 (dt, 24 ago 2010) | 5 lines

Backport SVN commit 1167267:
Fix KHistoryComboBox not emitting currentIndexChanged when using the down/up keys.
With addition to the example program and unittests.
CCMAIL: wilderkde@gmail.com

------------------------------------------------------------------------
r1166279 | grossard | 2010-08-21 13:01:36 +0100 (ds, 21 ago 2010) | 2 lines

added a localized entity for krdc

------------------------------------------------------------------------
r1166129 | scripty | 2010-08-21 03:24:48 +0100 (ds, 21 ago 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1165920 | skelly | 2010-08-20 13:22:17 +0100 (dv, 20 ago 2010) | 1 line

Backport r1165918
------------------------------------------------------------------------
r1165913 | skelly | 2010-08-20 13:05:19 +0100 (dv, 20 ago 2010) | 1 line

Backport r1165905 and r1165908 to 4.5
------------------------------------------------------------------------
r1165845 | grossard | 2010-08-20 09:53:33 +0100 (dv, 20 ago 2010) | 2 lines

added a translator

------------------------------------------------------------------------
r1165842 | skelly | 2010-08-20 09:46:00 +0100 (dv, 20 ago 2010) | 1 line

Backport r1165838
------------------------------------------------------------------------
r1165136 | stikonas | 2010-08-18 11:37:52 +0100 (dc, 18 ago 2010) | 1 line

Updated user.entities file.
------------------------------------------------------------------------
r1164886 | mart | 2010-08-17 22:05:31 +0100 (dt, 17 ago 2010) | 2 lines

backport show mneminics

------------------------------------------------------------------------
r1164822 | dfaure | 2010-08-17 19:08:22 +0100 (dt, 17 ago 2010) | 6 lines

Backport SVN commit 1164731:
Fix useless space on the right (where the clear button would be) and too early wrapping/squeezing
when a KLineEdit is made readonly via a QLineEdit pointer
(example: combo->lineEdit()->setReadOnly(true), in kdepim/libkdepim/kcheckcombobox.cpp).
setReadOnly is not virtual in QLineEdit, so KLineEdit has to handle becoming readonly without being told so.

------------------------------------------------------------------------
r1164799 | adawit | 2010-08-17 17:49:45 +0100 (dt, 17 ago 2010) | 2 lines

Updated with bug report # for missing Spell checking support.

------------------------------------------------------------------------
r1164782 | adawit | 2010-08-17 17:19:40 +0100 (dt, 17 ago 2010) | 2 lines

Backported the fix for disregarding empty cookie strings...

------------------------------------------------------------------------
r1164778 | skelly | 2010-08-17 17:08:39 +0100 (dt, 17 ago 2010) | 1 line

Backport reset fix from trunk.
------------------------------------------------------------------------
r1164755 | dfaure | 2010-08-17 16:30:30 +0100 (dt, 17 ago 2010) | 2 lines

Backport 1164379 and 1164381: 10% less access() and 25% less stat() calls (on my machine) in iconloader tests.

------------------------------------------------------------------------
r1164655 | skelly | 2010-08-17 12:50:18 +0100 (dt, 17 ago 2010) | 1 line

Fix maintenance of mappings in the proxy.
------------------------------------------------------------------------
r1164474 | dfaure | 2010-08-16 21:55:41 +0100 (dl, 16 ago 2010) | 2 lines

Backport fix for 247552, completionbox regression

------------------------------------------------------------------------
r1164385 | orlovich | 2010-08-16 17:47:57 +0100 (dl, 16 ago 2010) | 11 lines

Fix problems with ghost first-letter RenderTextFragments staying around 
(and keeping dangling pointers w/them) when the inline containing their 
text has changed by keeping a link from the main text's RenderTextFragment 
to the letter's, to permit invalidating it (Stolen from WebCore).

This fixes the crashes, but on change first-letter isn't reapplied properly 
as still keeps a useless anonymous inline wrapper and RenderBlock::updateFirstLetter
isn't smart enough to walk past it or reuse it. 

BUG: 161989

------------------------------------------------------------------------
r1164305 | mutz | 2010-08-16 14:39:09 +0100 (dl, 16 ago 2010) | 5 lines

Make DBusMenuQt optional

http://reviewboard.kde.org/r/4898/

MERGE: trunk
------------------------------------------------------------------------
r1164139 | adawit | 2010-08-16 04:14:02 +0100 (dl, 16 ago 2010) | 1 line

Updated content
------------------------------------------------------------------------
r1164054 | orlovich | 2010-08-15 18:58:58 +0100 (dg, 15 ago 2010) | 4 lines

Go ahead and be far more strict about keeping the special child object lists up-to-date.

BUG: 170165

------------------------------------------------------------------------
r1164025 | orlovich | 2010-08-15 16:38:20 +0100 (dg, 15 ago 2010) | 6 lines

Set the reverse link in the childframe tree earlier, might need it in setWidget.
Also some extra unrelated debug output I need anyway, hence not worth the effort 
to pull it out.

BUG: 245691

------------------------------------------------------------------------
r1163756 | scripty | 2010-08-15 03:07:21 +0100 (dg, 15 ago 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1163312 | adawit | 2010-08-13 18:27:03 +0100 (dv, 13 ago 2010) | 3 lines

- Backported the changes in trunk that normalizes the handling of MMB actions to  
  the more correct implementation found in KParts::BrowserExtension::pasteRequest.

------------------------------------------------------------------------
r1162615 | mmrozowski | 2010-08-12 10:53:34 +0100 (dj, 12 ago 2010) | 1 line

[KTar] Rely solely on magic detected by findByFileContent - by David Faure.
------------------------------------------------------------------------
r1162452 | scripty | 2010-08-12 03:06:31 +0100 (dj, 12 ago 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1162222 | mmrozowski | 2010-08-11 18:44:18 +0100 (dc, 11 ago 2010) | 3 lines

[KTar] Fix writing archive when created with mode=WriteOnly (filters were not applied hence file in tar format was written even when .tar.gz was requested) - by David Faure.
Rework file magic guessing in createArchive, update unit tests - by me.

------------------------------------------------------------------------
r1161874 | cfeck | 2010-08-11 02:31:03 +0100 (dc, 11 ago 2010) | 5 lines

Fix insertation in default (alphabetical) order (backport r1161872)

BUG: 247214
FIXED-IN: 4.5.1

------------------------------------------------------------------------
r1161819 | aseigo | 2010-08-10 22:46:42 +0100 (dt, 10 ago 2010) | 3 lines

pass through mouse events if nobody is connected to us
CCBUG:244958

------------------------------------------------------------------------
r1161762 | trueg | 2010-08-10 20:08:54 +0100 (dt, 10 ago 2010) | 1 line

Fixed compilation bugs caused by faulty backporting
------------------------------------------------------------------------
r1161722 | mmrozowski | 2010-08-10 18:28:05 +0100 (dt, 10 ago 2010) | 3 lines

[KTar] Determine archive mimetype from filename as a fallback when unable to detect from contents. Also use KMimeType::is( .. ) + a little code cleanup.

Review request: http://reviewboard.kde.org/r/4966/
------------------------------------------------------------------------
r1161636 | trueg | 2010-08-10 14:44:42 +0100 (dt, 10 ago 2010) | 1 line

Backport: Remove duplicates without changing the order of the terms - this fixes the unit test.
------------------------------------------------------------------------
r1161632 | dfaure | 2010-08-10 14:40:39 +0100 (dt, 10 ago 2010) | 2 lines

backport: application/x-webarchive is a .tar.gz, not a .zip

------------------------------------------------------------------------
r1161623 | trueg | 2010-08-10 14:16:43 +0100 (dt, 10 ago 2010) | 1 line

Backport: Remove duplicates during optimization
------------------------------------------------------------------------
r1161569 | trueg | 2010-08-10 12:10:19 +0100 (dt, 10 ago 2010) | 1 line

Backport: Do not create an order term for ask and count queries.
------------------------------------------------------------------------
r1161162 | aseigo | 2010-08-09 21:49:23 +0100 (dl, 09 ago 2010) | 2 lines

if unversioned, let it through based on trust

------------------------------------------------------------------------
r1161104 | grossard | 2010-08-09 20:01:14 +0100 (dl, 09 ago 2010) | 2 lines

added two entities for rekonq

------------------------------------------------------------------------
r1161048 | mart | 2010-08-09 15:52:32 +0100 (dl, 09 ago 2010) | 2 lines

backport: Plassma::Theme::windowTranslucencyEnabled() caches the selection owner, should be faster than KWindowsystem::compositingActive() that doesn't

------------------------------------------------------------------------
r1161016 | ppenz | 2010-08-09 15:07:54 +0100 (dl, 09 ago 2010) | 2 lines

Let the receiver know, for which items the request for meta-data has been finished

------------------------------------------------------------------------
r1160713 | mwolff | 2010-08-08 22:15:26 +0100 (dg, 08 ago 2010) | 10 lines

The python-encoding kate plugin isn't built anymore in 4.5 but it's still in the default config.
This causes kate to display an error message whenever saving a python file because it can't find the plugin.

So python-encoding should removed from katemoderc in 4.5.
In trunk it's enabled again (but disabled by default):
http://websvn.kde.org/?view=revision&revision=1145919

Review Request: http://reviewboard.kde.org/r/4935

Thanks to Felix Geyer for the patch.
------------------------------------------------------------------------
r1160712 | mwolff | 2010-08-08 22:15:24 +0100 (dg, 08 ago 2010) | 1 line

install missing Kate forward headers
------------------------------------------------------------------------
r1160688 | mart | 2010-08-08 20:58:13 +0100 (dg, 08 ago 2010) | 2 lines

backport event blocking in AppletOverlayWidget

------------------------------------------------------------------------
r1160677 | ppenz | 2010-08-08 20:29:49 +0100 (dg, 08 ago 2010) | 3 lines

Backport of SVN commit 1158055: Emit a signal, if the meta-data request has been finished. This e. g. allows tooltips to postpone the opening of the tooltip, until all data could be received.
CCBUG: 245491

------------------------------------------------------------------------
r1160566 | cfeck | 2010-08-08 12:34:18 +0100 (dg, 08 ago 2010) | 4 lines

Fix crash in pixmap transition (backport r1160565)

CCBUG: 242272

------------------------------------------------------------------------
r1160099 | scripty | 2010-08-07 03:09:28 +0100 (ds, 07 ago 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1159972 | gladhorn | 2010-08-06 17:34:30 +0100 (dv, 06 ago 2010) | 4 lines

Show details for static xml again, use the small preview if there is no big one and limit the scaling for preview images.

backport r1159971

------------------------------------------------------------------------
r1159963 | krake | 2010-08-06 17:03:27 +0100 (dv, 06 ago 2010) | 1 line

Fixing windows build: friend declarations also need export macro
------------------------------------------------------------------------
r1159909 | cfeck | 2010-08-06 14:50:56 +0100 (dv, 06 ago 2010) | 8 lines

Fix invalid model access (backport r1159907)

After clicking on an Install button with a drop down menu,
Qt does not put the focus back on the button widget, but on
the list. We can get the view's model by caching it instead.

CCBUG: 240898

------------------------------------------------------------------------
r1159734 | aseigo | 2010-08-06 04:43:25 +0100 (dv, 06 ago 2010) | 2 lines

prevent infinite recursion

------------------------------------------------------------------------
r1159627 | jacopods | 2010-08-05 21:30:44 +0100 (dj, 05 ago 2010) | 2 lines

Make sure to actually make good use of the cached mask.

------------------------------------------------------------------------
r1159552 | mutz | 2010-08-05 16:45:29 +0100 (dj, 05 ago 2010) | 4 lines

Add Q_DECLARE_METATYPE macros for KBookmark

This will become a requirement for Akondai payloads (and therefore has to be in 4.5-branch).
MERGE: trunk
------------------------------------------------------------------------
r1159472 | skelly | 2010-08-05 13:50:49 +0100 (dj, 05 ago 2010) | 1 line

Backport off-by-one fix.
------------------------------------------------------------------------
r1159468 | gladhorn | 2010-08-05 13:47:05 +0100 (dj, 05 ago 2010) | 5 lines

Use KUrl::toLocalFile instead of urlOrPath which should fix installation on windows.

backport r1159467 
BUG: 241523

------------------------------------------------------------------------
r1159317 | scripty | 2010-08-05 03:14:43 +0100 (dj, 05 ago 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1159261 | djarvie | 2010-08-04 23:04:29 +0100 (dc, 04 ago 2010) | 1 line

Add since KDE 4.3
------------------------------------------------------------------------
