------------------------------------------------------------------------
r1108373 | adawit | 2010-03-29 06:58:25 +1300 (Mon, 29 Mar 2010) | 1 line

Correct KWebKitPart integration except for showing only selected links.
------------------------------------------------------------------------
r1108639 | adawit | 2010-03-30 02:52:43 +1300 (Tue, 30 Mar 2010) | 2 lines

Added copyright.

------------------------------------------------------------------------
r1110086 | rdieter | 2010-04-02 09:36:54 +1300 (Fri, 02 Apr 2010) | 2 lines

backport r1080212 , fix for Qt 4.7

------------------------------------------------------------------------
r1110428 | scripty | 2010-04-03 14:51:23 +1300 (Sat, 03 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1110828 | rjarosz | 2010-04-04 20:58:10 +1200 (Sun, 04 Apr 2010) | 7 lines

Backport commit 1110827.
Don't resend stealth setting on login, because server will block us.                                                                        
Fix stealth notification.                                                                                                                   
                                                                                                                                            
CCBUG: 226699


------------------------------------------------------------------------
r1111187 | scripty | 2010-04-05 13:48:53 +1200 (Mon, 05 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1112750 | scripty | 2010-04-09 13:50:31 +1200 (Fri, 09 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1115182 | mfuchs | 2010-04-16 03:01:10 +1200 (Fri, 16 Apr 2010) | 4 lines

Backport r1115181
Do not manually kill jobs for ChecksumSearch, let them do that themselves.

CCBUG:234292
------------------------------------------------------------------------
r1115287 | mfuchs | 2010-04-16 08:46:07 +1200 (Fri, 16 Apr 2010) | 7 lines

Backport r1115285
Adding downloads to httpserver works again:
* Uses the defaultFolder of the selected group first, if no folder is defined then it first tries the RegExp rules and then the generalDestDir
* Crash fixed by not accessing list that might be empty with "first()"
* Correctly set the selected group when creating the download

CCBUG:229285
------------------------------------------------------------------------
r1116154 | woebbe | 2010-04-19 06:59:05 +1200 (Mon, 19 Apr 2010) | 2 lines

-pedantic

------------------------------------------------------------------------
r1116274 | scripty | 2010-04-19 13:52:07 +1200 (Mon, 19 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1117326 | mzanetti | 2010-04-22 09:26:47 +1200 (Thu, 22 Apr 2010) | 4 lines

backport of revision 1117324:

fix html/plaintext problem when reloading a message from cache

------------------------------------------------------------------------
r1117335 | scripty | 2010-04-22 09:38:19 +1200 (Thu, 22 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1117354 | mzanetti | 2010-04-22 10:23:11 +1200 (Thu, 22 Apr 2010) | 5 lines

backport of rev 1117353:

fix otr opportunistic mode initialisation


------------------------------------------------------------------------
r1117617 | mfuchs | 2010-04-23 04:03:32 +1200 (Fri, 23 Apr 2010) | 5 lines

Whenever a DataSourceFactory is created use a default segSize.
Only offer one DataSourceFactory.
Check if size is specified.

BUG:234815
------------------------------------------------------------------------
r1117790 | scripty | 2010-04-23 13:57:49 +1200 (Fri, 23 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1117967 | mfuchs | 2010-04-24 03:26:09 +1200 (Sat, 24 Apr 2010) | 4 lines

When download finished do nepomuk tagging etc. after the status text has changed.
That way 100% is always showed, even if tagging might take very long.

CCBUG:233732
------------------------------------------------------------------------
r1118222 | scripty | 2010-04-24 14:03:46 +1200 (Sat, 24 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1119743 | murrant | 2010-04-28 07:07:30 +1200 (Wed, 28 Apr 2010) | 8 lines

Backport to 4.4 branch:
SVN Commit 1119700 by murrant:

Return to window mode if there are no more open connections.

CCBUG: 235223


------------------------------------------------------------------------
r1119744 | murrant | 2010-04-28 07:11:09 +1200 (Wed, 28 Apr 2010) | 7 lines

Backport to 4.4 branch:
SVN commit 1119724 by murrant:

Check for close events on the fullscreen window and quit KRDC.

CCBUG: 232228

------------------------------------------------------------------------
r1119910 | murrant | 2010-04-28 11:17:09 +1200 (Wed, 28 Apr 2010) | 3 lines

Simple fix for this bug for 4.4, refuse to resize the tab widget when in fullscreen mode.
BUG: 208845

------------------------------------------------------------------------
r1119953 | scripty | 2010-04-28 13:55:48 +1200 (Wed, 28 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1120423 | murrant | 2010-04-29 16:34:24 +1200 (Thu, 29 Apr 2010) | 5 lines

Backport to 4.4 branch:
SVN commit 1120422 by murrant:

Fix tray icon behaviour with fullscreen window.

------------------------------------------------------------------------
r1120625 | murrant | 2010-04-30 01:47:23 +1200 (Fri, 30 Apr 2010) | 5 lines

Backport r1120624 by murrant from trunk to the 4.4 branch:

Need to check if m_systemTrayIcon exists before trying to access member functions.


------------------------------------------------------------------------
