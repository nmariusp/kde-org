2006-08-06 14:51 +0000 [r570366]  repinc

	* branches/KDE/3.5/kdemultimedia/kmix/mdwslider.cpp: Fix the label
	  so that the entire number 100 is shown for relative volume
	  indicator. Make frame a bit thiner. I think it looks better this
	  way.

2006-08-09 10:30 +0000 [r571342]  mueller

	* trunk/extragear/utils/Makefile.am.in,
	  trunk/extragear/graphics/Makefile.am.in,
	  branches/KDE/3.5/kdeaddons/Makefile.am.in,
	  branches/KDE/3.5/kdebase/Makefile.am.in,
	  branches/KDE/3.5/kdeedu/Makefile.am.in,
	  branches/KDE/3.5/kdesdk/Makefile.am.in,
	  branches/koffice/1.5/koffice/Makefile.am.in,
	  branches/KDE/3.5/kdepim/Makefile.am.in,
	  branches/KDE/3.5/kdeaccessibility/Makefile.am.in,
	  branches/KDE/3.5/kdeadmin/Makefile.am.in,
	  branches/KDE/3.5/kdenetwork/Makefile.am.in,
	  branches/KDE/3.5/kdelibs/Makefile.am.in,
	  branches/KDE/3.5/kdeartwork/Makefile.am.in,
	  branches/arts/1.5/arts/Makefile.am.in,
	  trunk/extragear/pim/Makefile.am.in,
	  branches/KDE/3.5/kdemultimedia/Makefile.am.in,
	  branches/KDE/3.5/kdegames/Makefile.am.in,
	  trunk/playground/sysadmin/Makefile.am.in,
	  trunk/extragear/network/Makefile.am.in,
	  trunk/extragear/libs/Makefile.am.in,
	  branches/KDE/3.5/kdebindings/Makefile.am.in,
	  branches/KDE/3.5/kdetoys/Makefile.am.in,
	  trunk/extragear/multimedia/Makefile.am.in,
	  branches/KDE/3.5/kdeutils/Makefile.am.in,
	  branches/KDE/3.5/kdegraphics/Makefile.am.in: update automake
	  version

2006-08-15 12:22 +0000 [r573243]  carewolf

	* branches/KDE/3.5/kdemultimedia/kfile-plugins/mpeg/kfile_mpeg.h,
	  branches/KDE/3.5/kdemultimedia/kfile-plugins/mpeg/kfile_mpeg.cpp:
	  Correctly detect and parse CD-XA MPEGs as well.

2006-08-19 09:47 +0000 [r574518]  kling

	* branches/KDE/3.5/kdemultimedia/krec/krecfileview.cpp: Fixed an
	  uninitialized variable.

2006-08-19 10:06 +0000 [r574529]  kling

	* branches/KDE/3.5/kdemultimedia/krec/krecfile.cpp: Stop leakin
	  KTar objects in KRecFile constructor. BUG: 132621

2006-08-19 21:15 +0000 [r574685]  adridg

	* branches/KDE/3.5/kdemultimedia/juk/configure.in.in: Need to obey
	  --with-extra-* to pick up tunepimp on FBSD. CCMAIL:
	  kde@freebsd.org

2006-08-21 15:04 +0000 [r575423]  jriddell

	* branches/KDE/3.5/kdemultimedia/COPYING-DOCS (added): Add FDL
	  licence for documentation

2006-08-21 20:04 +0000 [r575631]  adridg

	* branches/KDE/3.5/kdemultimedia/juk/ktrm.cpp: tunepimp-0.5 doesn't
	  have the GetResults notions at all anymore, even though its
	  documentation mentions them. #ifdef them out to get things to
	  compile; it's a hack though. CCMAIL: kde-multimedia-devel@kde.org

2006-08-22 05:00 +0000 [r575789]  helio

	* branches/KDE/3.5/kdemultimedia/kioslave/audiocd/kcmaudiocd/kcmaudiocd.cpp,
	  branches/KDE/3.5/kdemultimedia/kioslave/audiocd/kcmaudiocd/kcmaudiocd.h:
	  - Setting module to use system defaults

2006-08-24 09:08 +0000 [r576487]  hasso

	* branches/KDE/3.5/kdemultimedia/libkcddb/genres.cpp,
	  branches/KDE/3.5/kdemultimedia/libkcddb/categories.cpp: Avoid
	  using translation from kdelibs.po which isn't the correct one in
	  most of languages in this context ... I guess. CCMAIL:
	  kde-i18n-doc@kde.org

2006-08-29 18:46 +0000 [r578607]  adridg

	* branches/KDE/3.5/kdemultimedia/juk/ktrm.cpp,
	  branches/KDE/3.5/kdemultimedia/juk/configure.in.in: Back out
	  previous -- consider tunepimp 0.5 to be the same as 'not
	  installed'

2006-09-06 18:50 +0000 [r581568]  helio

	* branches/KDE/3.5/kdemultimedia/kmix/kmix.cpp: - Fixed Mandriva
	  bug http://qa.mandriva.com/show_bug.cgi?id=21221, wich applies to
	  upstream KDE: Global shortcuts should follow master channel
	  change.

2006-09-23 22:40 +0000 [r587757]  mpyne

	* branches/KDE/3.5/kdemultimedia/akode_artsplugin/configure.in.in,
	  branches/KDE/3.5/kdemultimedia/configure.in.in: Fix bug 134355
	  (configure script fails when using --without-akode option) by
	  using the patches provided by Rex Dieter and Juuso Alasuutari.
	  The AM_CONDITIONAL checks for whether to install the aKode
	  .mcopclass files must always be run it seems, even if aKode is
	  configured to not install, otherwise a autoconf sanity check will
	  fail when running configure. I've also taken the liberty of
	  moving the aKode stuff to akode_artsplugin since that is the
	  Makefile.am that uses these particular AM_CONDITIONALS(). (This
	  was the Rex Dieter patch). P.S. Bug 116958 wasn't really a
	  duplicate of 134355, 134355 was caused by the fix for 116958. ;)
	  BUG:134355

2006-09-24 11:08 +0000 [r587879]  coolo

	* branches/KDE/3.5/kdemultimedia/kioslave/audiocd/audiocd.cpp:
	  check if ioctl device name is given

2006-10-02 11:08 +0000 [r591332]  coolo

	* branches/KDE/3.5/kdemultimedia/kdemultimedia.lsm: updates for
	  3.5.5

