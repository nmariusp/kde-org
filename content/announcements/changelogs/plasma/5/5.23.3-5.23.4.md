---
title: Plasma 5.23.4 complete changelog
version: 5.23.4
hidden: true
plasma: true
type: fulllog
---
{{< details title="Breeze" href="https://commits.kde.org/breeze" >}}
+ Reduce groove opacity for greater contrast with scrollbar/slider/etc. [Commit.](http://commits.kde.org/breeze/dc4236dab2999209f1574dae83e8eb001cf77608) Fixes bug [#444203](https://bugs.kde.org/444203)
{{< /details >}}

{{< details title="breeze-gtk" href="https://commits.kde.org/breeze-gtk" >}}
+ Gtk3, gtk4: refactor buttons heavily, make look near-identical to breeze qstyle. [Commit.](http://commits.kde.org/breeze-gtk/ed314cf17880f71a897bd4a67cce2ffc9468610e) Fixes bug [#426557](https://bugs.kde.org/426557). Fixes bug [#438185](https://bugs.kde.org/438185). See bug [#443919](https://bugs.kde.org/443919)
+ Gtk3, gtk4: format with prettier. [Commit.](http://commits.kde.org/breeze-gtk/c950952f5904d4cb4b9a1af12bdfb1dcb36e6e58) 
+ Gtk3, gtk4: improve build times. [Commit.](http://commits.kde.org/breeze-gtk/1b3a52dcc20ecbf4f9551d7cb5af61cb5edd2776) 
+ Fixed toolbar buttons style, removing unneeded outline. [Commit.](http://commits.kde.org/breeze-gtk/6834ee83d09b7c56fbe3d137b660db25484573f3) Fixes bug [#443626](https://bugs.kde.org/443626)
{{< /details >}}

{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ Reverse screenshots arrows when in RTL mode. [Commit.](http://commits.kde.org/discover/704e826a3e7c0dc65f49accd2c8deaf22580be5d) See bug [#440650](https://bugs.kde.org/440650)
+ Flatpak: Do not discard remotes with different installations as duplicates. [Commit.](http://commits.kde.org/discover/67cf4a37fab2eed05ef5f8c406e762d4e9824e84) Fixes bug [#443745](https://bugs.kde.org/443745)
+ Flatpak: Do not call flatpak function with a nullptr. [Commit.](http://commits.kde.org/discover/e36f4d08ea337b8958573754f25acaf218ed7f0b) Fixes bug [#445819](https://bugs.kde.org/445819)
+ Flatpak: Check for errors before using the value. [Commit.](http://commits.kde.org/discover/1e2895018d7a2a7babc51b3b7ed335e89df131fb) 
+ Flatpak: Do not include the resource type in the FlatpakResource::Id. [Commit.](http://commits.kde.org/discover/dc67ef22f47b9e8b46d668b811982f33600f1aae) 
{{< /details >}}

{{< details title="Plasma Addons" href="https://commits.kde.org/kdeplasma-addons" >}}
+ Applets/weather: Make cursor a pointing hand when hovering over source link. [Commit.](http://commits.kde.org/kdeplasma-addons/ea18868f6b49577b1349438d6e058199705a25b0) 
+ [mediaframe] Make files added from paths into URLs. [Commit.](http://commits.kde.org/kdeplasma-addons/5e16b5ab644748fbf38c2b7fa7113d8dc99bcc28) Fixes bug [#445071](https://bugs.kde.org/445071)
{{< /details >}}

{{< details title="KDE Hotkeys" href="https://commits.kde.org/khotkeys" >}}
+ When deleting an entry, also delete the widgets. [Commit.](http://commits.kde.org/khotkeys/a4137ac8f7b18824568fbee0f3e2dce7551841b7) Fixes bug [#443656](https://bugs.kde.org/443656)
{{< /details >}}

{{< details title="KScreen" href="https://commits.kde.org/kscreen" >}}
+ Kcm: make revert message wrap. [Commit.](http://commits.kde.org/kscreen/29343eb7586509dc37ecf5adcb3b6086803fa0ed) Fixes bug [#445341](https://bugs.kde.org/445341)
{{< /details >}}

{{< details title="ksystemstats" href="https://commits.kde.org/ksystemstats" >}}
+ Don't update disks if no one is listening. [Commit.](http://commits.kde.org/ksystemstats/7406648cfa06ee220f65b6f2940fe30bbb620796) Fixes bug [#445039](https://bugs.kde.org/445039)
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Wayland: Enable window rules for all xdg-toplevel. [Commit.](http://commits.kde.org/kwin/1ba7b5ec63b61fa00b7eac59a1beca12323fefb3) 
+ Revert "wayland: Check workspace position when preferred deco mode changes". [Commit.](http://commits.kde.org/kwin/325208347c40d7311aea83572b5150ba3750cba9) Fixes bug [#445444](https://bugs.kde.org/445444)
+ Fix XWayland abstract socket address. [Commit.](http://commits.kde.org/kwin/5a9a3e96898bd29ccbf31d9ff92dfb80274ce265) Fixes bug [#442362](https://bugs.kde.org/442362)
{{< /details >}}

{{< details title="libksysguard" href="https://commits.kde.org/libksysguard" >}}
+ Don't update SensorsFeatureSensor if it is not subscribed. [Commit.](http://commits.kde.org/libksysguard/447c4d81b2429b4a9c5abe1ec1b6b5861bd340d0) 
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Kickoff: Do not reset action for KickoffItemDelegate in Places tab. [Commit.](http://commits.kde.org/plasma-desktop/5f49586043e32c6d72c2c1d82ba9fdc539af9053) Fixes bug [#446269](https://bugs.kde.org/446269)
+ Applets/taskmanager: Always show album art for single non-browser windows. [Commit.](http://commits.kde.org/plasma-desktop/0a5f0368a44e53348de0fa5228c8baca8c01e1da) 
+ Kcms/touchpad: make applet read-only. [Commit.](http://commits.kde.org/plasma-desktop/57d5f02d9aff1c784d08b4ff865ac9f4f0af6f0e) See bug [#445095](https://bugs.kde.org/445095)
+ Revert "Delete the Touchpad applet". [Commit.](http://commits.kde.org/plasma-desktop/523184bc7d3706ce49b95db4d717511368a7f8fd) Fixes bug [#445095](https://bugs.kde.org/445095)
{{< /details >}}

{{< details title="Plasma Phone Components" href="https://commits.kde.org/plasma-phone-components" >}}
+ Homescreen: Fix silly type name error. [Commit.](http://commits.kde.org/plasma-phone-components/3a4a2a52a56f31effbac0b44993bf728fc11c90e) 
+ Fix broken reset homescreen position signal, and minor refactor. [Commit.](http://commits.kde.org/plasma-phone-components/e77b7c5c2acec116d2ed2c3858686e0a8d2285a4) 
+ Rework app drawer layout and open/close behaviour logic. [Commit.](http://commits.kde.org/plasma-phone-components/554e01e4aa90ec3940a7b5af2c0bebda7e9ca32f) 
{{< /details >}}

{{< details title="Plasma Systemmonitor" href="https://commits.kde.org/plasma-systemmonitor" >}}
+ Don't make right click popup modal. [Commit.](http://commits.kde.org/plasma-systemmonitor/874d867b161fa134305c86be63740a44bc39fcaf) 
{{< /details >}}

{{< details title="plasma-vault" href="https://commits.kde.org/plasma-vault" >}}
+ Add missing translation domain. [Commit.](http://commits.kde.org/plasma-vault/1f7843decc8cb85315a5cc977fcfe6f0fbe4cdb5) Fixes bug [#445726](https://bugs.kde.org/445726)
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ Lock & login screens: Use RTL-friendly arrows when in RTL mode. [Commit.](http://commits.kde.org/plasma-workspace/a61e7db8bb9ebb7f802692cc59ed5ae38d1a06b2) See bug [#440650](https://bugs.kde.org/440650)
+ [logout-greeter] Set window class name. [Commit.](http://commits.kde.org/plasma-workspace/22bd1fb8cbb23e7aeb0237f1389ef35b0553a040) Fixes bug [#444898](https://bugs.kde.org/444898). Fixes bug [#444899](https://bugs.kde.org/444899)
+ Systemtray: Connect to StatusNotifierWatcher before initializing QDBusPendingReply. [Commit.](http://commits.kde.org/plasma-workspace/a9fba8b5416dd3b130045ccac40e5412714563ea) 
+ Systemtray: Check if a service is already added before processing QDBusReply. [Commit.](http://commits.kde.org/plasma-workspace/931a5441746daf10d9476409f347263719ef6c63) Fixes bug [#443961](https://bugs.kde.org/443961)
+ Baloosearchruner: Emit DBus error when baloo is disabled. [Commit.](http://commits.kde.org/plasma-workspace/afff43effadd78ab682ecf4c6c9919bdcbbedbcd) Fixes bug [#445342](https://bugs.kde.org/445342)
{{< /details >}}

{{< details title="Powerdevil" href="https://commits.kde.org/powerdevil" >}}
+ Always dismiss critical battery notification. [Commit.](http://commits.kde.org/powerdevil/d8c5490c99bae9f979f6483dc7b0381121a4f997) Fixes bug [#445171](https://bugs.kde.org/445171)
{{< /details >}}

{{< details title="xdg-desktop-portal-kde" href="https://commits.kde.org/xdg-desktop-portal-kde" >}}
+ Save/restore file dialog window size. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/8b1e5c4da68c089771999129cfeaaa7b3e5475bb) Fixes bug [#443251](https://bugs.kde.org/443251)
{{< /details >}}

