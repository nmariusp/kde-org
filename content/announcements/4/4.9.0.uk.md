---
aliases:
- ../4.9
custom_about: true
custom_contact: true
date: '2012-12-05'
title: Випуск KDE 4.9 — на згадку про Клер Лоушн
---

<p>
<img src="/stuff/clipart/klogo-official-oxygen-128x128.png" class="float-left m-2" />Команда KDE із задоволенням повідомляє про найсвіжіший випуск оновлено <a href="./plasma">робочих просторів Плазми KDE</a>, <a href="./applications">програм KDE</a> та <a href="./platform">платформи розробки у KDE</a>. У версії 4.9 ви зможете скористатися багатьма 
новими можливостями та покращеннями у стабільності та швидкодії.
</p>
<p>
Цей випуск присвячено пам’яті учасниці розробки KDE <a href="http://dot.kde.org/2012/05/20/remembering-claire-lotion">Клер Лоушн (Claire Lotion)</a>. Життєрадісність та ентузіазм Клер були натхненням для багатьох учасників спільноти, а її робота над форматом, порядком денним та частотою наших зустрічей розробників змінила спосіб, у який ми тепер намагаємося здійснити нашу мету. Ця робота та інші завдання, які брала на себе Клер, значно змінили програмне забезпечення, випущене нами сьогодні. Ми вдячні за її зусилля і завжди пам’ятатимемо про неї.</p>
<div class="text-center">
	<a href="/announcements/4/4.9.0/kde49-desktop.png">
	<img src="/announcements/4/4.9.0/kde49-desktop-thumb.png" class="img-fluid">
	</a> <br/>
</div>
<br/>
<p>
Раніше цього року було організовано команду із забезпечення якості KDE. Метою цієї команди було покращення загального рівня якості та стабільності програмного забезпечення KDE. Особливу увагу було приділено виявленню та виправленню погіршень з часу попередніх випусків. Це завдання є найважливішим, оскільки його виконання забезпечує неухильне покращення якості наших випусків.
</p>
<p>
Цією командою було створено умови для ефективнішого тестування випусків з початкового етапу розробки. Було проведено ряд навчань для нових добровольців та декілька днів інтенсивного тестування. Замість звичайного оглядового тестування зусилля було зосереджено на тих частинах програмного забезпечення, у які було внесено зміни з часу попереднього випуску. Для ряду критичних компонентів було розроблено і використано повні процедури тестування. Командою було виявлено багато важливих вад на ранньому етапі та проведено роботу з розробниками щодо виправлення цих вад. Командою було повідомлено про понад 160 вад у тестових випусках, багато з цих вад було виправлено. Тестувальниками було зроблено значний внесок до удосконалення вже створених повідомлень про вади. Зусилля команди з роботи над звітами про вади надали змогу розробникам зосередитися на виправлені цих вад.
</p>
<p>
У результаті зусиль команди з забезпечення якості KDE випуск 4.9 є найякіснішим з усіх досі випущених.
</p>
<p>
Один зі звітів про вади заслуговує на особливу увагу. Про ваду у Okular було повідомлено у 2007 році, вада отримала понад 1100 голосів, її виправлення було важливим для багатьох користувачів. Проблема полягала у тому, що анотації у файлах PDF не можна було зберегти у самих файлах і надрукувати. За допомогою багатьох коментаторів та учасників обговорень на каналі IRC Okular Фабіо д`Урсо (Fabio D’Urso) було створено код, за допомогою якого нова версія Okular може зберігати і друкувати анотації у PDF. Виправлення вади потребувало певної роботи над бібліотеками KDE та уваги до загальної схеми програми з метою забезпечення належної роботи з документами, які не є документами PDF. Розробка цього коду була першим досвідом Фабіо. Ця людина мала проблему і знайшла спосіб її вирішити.
</p>

<h2><a href="./plasma"><img src="/announcements/4/4.9.0/images/plasma.png" class="app-icon float-left m-3"/>Робочі простори Плазми 4.9 — Основні покращення</a></h2>

<p>
Основними моментами у новій версії робочих просторів Плазми є значні покращення у програмі для керування файлами Dolphin, емуляторі термінала Konsole, просторах дій та засобі керування вікнами KWin. Ознайомтеся з <a href="./plasma">повним текстом оголошення щодо нової версії робочих просторів Плазми</a>.
</p>

<h2><a href="./applications"><img src="/announcements/4/4.9.0/images/applications.png" class="app-icon float-left m-3"/>Нові і покращені програми KDE 4.9</a></h2>

<p>
Сьогодні випущено нові і покращені програми KDE, зокрема Okular, Kopete, KDE PIM, освітні програми та ігри. Ознайомтеся з <a href="./applications">повним текстом оголошення щодо випуску програм KDE</a>.
</p>

<h2><a href="./platform"><img src="/announcements/4/4.9.0/images/platform.png" class="app-icon float-left m-3"/>Платформа KDE 4.9</a></h2>

<p>
У сьогоднішньому випуску платформи для розробки у KDE виправлено вади, покращено якість коду, можливості роботи у мережі та виконано роботу з приготування до випуску Frameworks 5
</p>
<h2>Розкажіть іншим і будьте свідком результатів: мітка «KDE»</h2>
<p>
Команді KDE хотілося б, щоб ви поширили цю новину у мережі. Надсилайте статті на сайти новин, користуйтеся каналами поширення даних, зокрема delicious, digg, reddit, twitter, identi.ca. Вивантажуйте знімки вікон у альбоми Facebook, Flickr, ipernity і Picasa та надсилайте дописи щодо них до відповідних груп. Створюйте відеодемонстрації та вивантажуйте їх на YouTube, Blip.tv і Vimeo. Будь ласка, додавайте до дописів та вивантажених матеріалів мітку «KDE». Це спростить пошук даних і надасть можливість команді з просування KDE проаналізувати новинне покриття випуску програмного забезпечення KDE 4.9.
</p>
<p>
Стежити за розвитком подій у соціальних мережах можна на каналі подачі KDE. На цьому сайті будуть збиратися всі записи з identi.ca, twitter, youtube, flickr, picasaweb, блогів та багатьох інших сайтів соціальних мереж у режимі реального часу. Подачу новин можна знайти на <a href="http://buzz.kde.org">buzz.kde.org</a>.
</p>
<h2>Святкування, присвячені випуску</h2>
<p>
Як завжди, спільнота KDE організовує святкування випуску по всьому світу. Декілька з таких вечірок вже заплановано, інші відбудуться незабаром. Список завжди можна знайти <a href="http://community.kde.org/Promo/Events/Release_Parties/4.9">тут</a>. Долучитися може кожен! Ви зможете поспілкуватися з цікавими людьми, обговорити цікаві питання, попоїсти та вгамувати спрагу. Це чудова можливість дізнатися про те, що відбувається у KDE, долучитися до команди або просто зустрітися з іншими користувачами та учасниками розробки.
</p>
<p>
Ми будемо раді, якщо ви влаштуєте власну вечірку. Такі вечірки чудово проводити самому чи брати у них участь! Ознайомтеся з нашими <a href="http://community.kde.org/Promo/Events/Release_Parties">підказками щодо організації святкувань</a>.
<div align="center">
<table border="0" cellspacing="2" cellpadding="2" class="social">
<tr>
	<td>
		<a class="DiggThisButton DiggCompact" href="http://digg.com/submit?url=http%3A//kde.org/announcements/4.9/&amp;title=KDE%20releases%20version%204.9%20of%20Plasma%20Workspaces,%20Applications%20and%20Platform" rev="news, tech_news"></a>
	</td>
	<td>
		<a href="https://twitter.com/share" class="twitter-share-button" data-url="/announcements/4.9/" data-text="#KDE releases version 4.9 of Plasma Workspaces, Applications and Platform →" data-via="kdecommunity" data-hashtags="linux,opensource">Tweet</a>
		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
	</td>
	<td>
		<script type="text/javascript" src="http://www.reddit.com/static/button/button1.js?url=http%3A//kde.org/announcements/4.9/"></script>
	</td>
	<td>
		<iframe src="http://www.facebook.com/plugins/like.php?app_id=225109044193701&amp;href=http%3A%2F%2Fkde.org%2Fannouncements%2F4.9%2F&amp;send=false&amp;layout=button_count&amp;width=80&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:80px; height:21px;" allowTransparency="true"></iframe>
	</td>
	<td>
		<g:plusone size="medium" href="/announcements/4.9/"></g:plusone>
	</td>
	<td>
	</td>
</tr>
</table>
<table border="0" cellspacing="2" cellpadding="2" class="social">
<tr>
	<td>
		<a href="http://identi.ca/search/notice?q=kde49"><img src="/announcements/buttons/identica.gif" alt="Identi.ca" title="Identi.ca" /></a>
	</td>
	<td>
		<a href="http://www.flickr.com/photos/tags/kde49"><img src="/announcements/buttons/flickr.gif" alt="Flickr" title="Flickr" /></a>
	</td>
	<td>
		<a href="http://www.youtube.com/results?search_query=kde49"><img src="/announcements/buttons/youtube.gif" alt="Youtube" title="Youtube" /></a>
	</td>
	<td>
		<a href="http://delicious.com/tag/kde49"><img src="/announcements/buttons/delicious.gif" alt="del.icio.us" title="del.icio.us" /></a>
	</td>
</tr>
</table>
<span style="font-size: 6pt"><a href="http://microbuttons.wordpress.com">мікрокнопки</a></span>
</div>
</p>
<h2>Про ці оголошення щодо випуску</h2>
<p>
Ці оголошення про випуски було створено Jos Poortvliet, Carl Symons, Valorie Zimmerman, Jure Repinc, David Edmundson, іншими учасниками команди з просування KDE та ширшою спільнотою KDE. У них ви знайдете лише основні моменти серед багатьох змін, які було внесено до програмного забезпечення KDE протягом останніх шести місяців.
</p>

<h4>Підтримайте KDE</h4>

<a href="http://jointhegame.kde.org/"><img src="/announcements/4/4.9.0/images/join-the-game.png" class="img-fluid float-left mr-3"
alt="Join the Game"/> </a>

<p align="justify">Відкрито нову програму KDE e.V. з <a
href="http://jointhegame.kde.org/">фінансової підтримки розробників</a>.
Сплачуючи 25 євро за квартал ви забезпечите зростання спільноти
KDE та створення вільного програмного забезпечення світового
рівня.</p>

<p>&nbsp;</p>
